package ru.example

import com.google.gson.annotations.SerializedName

data class AuthAnswer(
    @SerializedName("INFO")
    val info: String,
    @SerializedName("is_registered")
    val isExist: Boolean,
    @SerializedName("access_token")
    val token: String,
    @SerializedName("refresh_token") val refreshToken: String,
    @SerializedName("is_admin") val isAdmin: Boolean
)

