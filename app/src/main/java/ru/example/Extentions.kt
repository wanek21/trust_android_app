package ru.example

import android.graphics.BitmapFactory
import android.widget.ImageView
import java.util.*

fun ImageView.setPicFromBase64(base64: String){
    log("avater url: $base64")
    val rawBm = Base64.getDecoder().decode(base64)
    setImageBitmap(BitmapFactory.decodeByteArray(rawBm, 0, rawBm.size))
}