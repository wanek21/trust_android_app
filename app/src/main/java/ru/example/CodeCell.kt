package ru.example

import com.google.gson.annotations.SerializedName
import retrofit2.http.Field

data class CodeCell(
    @SerializedName("phone_number")
    val phone: String,
    val code: String
)
