package ru.example

import com.google.gson.annotations.SerializedName

data class ApiCodeAnswer(
    @SerializedName("INFO")
    val info: String
)

data class ApiCodeAnswerDebug(
    @SerializedName("message") val message: String?
)