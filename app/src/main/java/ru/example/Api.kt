package ru.example

import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query
import ru.example.data.CancelComplaintRequest
import ru.example.data.RefreshTokenResponse
import ru.example.domain.AddUserRequest
import ru.example.domain.Answer
import ru.example.domain.ComplaintUserRequest
import ru.example.domain.ComplaintUserResponse
import ru.example.domain.EvaluatorsProfileEntity
import ru.example.domain.EvaluatorsProfilesContainer
import ru.example.domain.GetEvaluatorsRequest
import ru.example.domain.GetUserContactsRequest
import ru.example.domain.GetUsersRequest
import ru.example.domain.PhoneNumber
import ru.example.domain.ProfileEntity
import ru.example.domain.ProfilesContainer
import ru.example.domain.RateProfile
import ru.example.domain.RateUserRequest
import ru.example.domain.SearchRatingResponse
import ru.example.domain.Selection
import ru.example.domain.TokenManager
import ru.example.domain.VotesDetailContainer
import ru.example.ui.fragments.admin.UserToBlock
import ru.example.ui.fragments.admin.UserToBlockItem
import ru.example.ui.fragments.registrationFragment.RegistrationProfileForm

interface Api {

    @GET("vote/details")
    suspend fun getVoteDetailes(
        @Query("votes_owner") id: String
    ): Response<VotesDetailContainer>

    @POST("api/code/")
    suspend fun sendSms(
        @Body phoneNumber: PhoneNumber
    ): Response<ApiCodeAnswer>

    @GET("api/code")
    suspend fun sendSmsDebug(
        @Query("phone_number") number: String
    ): Response<ApiCodeAnswerDebug>

    @POST("api/login/")
    suspend fun sendCodeNew(
        @Body codeVerify: CodeCell
    ): Response<AuthAnswer>

    @POST("api/registration/")
    suspend fun createProfile(
        @Body profile: RegistrationProfileForm,
        @Header("Authorization") token: String? = TokenManager.access_token
    ): Response<ProfileCreationAnswer>

    @Multipart
    @PATCH("api/user/update-avatar/")
    suspend fun addAvatar(
        @Part file: MultipartBody.Part
    ): Response<Answer>

    @POST("api/rating/rate/")
    suspend fun rateUser(
        @Body rateUserRequest: RateUserRequest
    ): Response<RateProfile>

    @POST("api/rating/complaint/")
    suspend fun sendComplaint(
        @Body complaintUserRequest: ComplaintUserRequest
    ): Response<ComplaintUserResponse>

    @POST("api/user/add-new-user/")
    suspend fun addUser(
        @Body profile: AddUserRequest
    ): Response<ProfileCreationAnswer>

    @POST("api/search/evaluators/")
    suspend fun getEvaluatorsUsersList(
        @Body getEvaluatorsRequest: GetEvaluatorsRequest
    ): Response<List<EvaluatorsProfileEntity>>

    @POST("api/search/contacts/") // new
    suspend fun getUserContactList(
        @Body getUserContactsRequest: GetUserContactsRequest
    ): Response<List<ProfileEntity>>

    //@FormUrlEncoded
    @GET("api/rating/my-evaluation/{user_id}")
    suspend fun getSearchRating(
        @Path("user_id") userId: Int
    ): Response<SearchRatingResponse>

    @POST("api/search/")
    suspend fun getUsers(
        @Body request: GetUsersRequest
    ): Response<List<ProfileEntity>>

    @POST("api/search/{fullname}")
    suspend fun findUsersByFullname(
        @Path("fullname") fullname: String,
        @Body selection: Selection
    ): Response<List<ProfileEntity>>

    @GET("api/user/{user_id}")
    suspend fun getProfileById(
        @Path("user_id") userId: Int
    ): Response<ProfileEntity>

    @POST("api/complaints/")
    suspend fun getComplaintsList(
        @Body selection: Selection
    ): Response<List<UserToBlockItem>>

    @GET("api/block-user/{user_id}")
    suspend fun blockUser(
        @Path("user_id") userId: Int
    ): Response<ResponseBody>

    @POST("api/cancel-complaint/")
    suspend fun cancelComplaint(
        @Body cancelComplaintRequest: CancelComplaintRequest
    ): Response<ResponseBody>
}