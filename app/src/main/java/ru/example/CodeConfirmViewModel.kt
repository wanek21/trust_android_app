package ru.example

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import ru.example.domain.PhoneNumber

class CodeConfirmViewModel(
    private val service: ApiService
) : ViewModel() {

    // old
    private val _answerFlow = MutableSharedFlow<Pair<AnswerState, Boolean>>()
    val answerFlow: SharedFlow<Pair<AnswerState, Boolean>> = _answerFlow

    private val _codeConfirmState = MutableStateFlow<AuthAnswer?>(null)
    val codeConfirmState = _codeConfirmState.asStateFlow()

    fun sendCode(code: CodeCell) {
        /*viewModelScope.launch {
            try {
                Log.d("@@@", "sendCode code ${code.code}")
                val answer = service.sendCode(code)
                Log.d("@@@", "senCode answer $answer")
                if (answer.isSuccessful) {
                    answer.body()?.let {
                        log("updating codeConfirmState: $it")
                        _codeConfirmState.update { it }
                        _answerFlow.emit(
                            Pair(
                                if (answer.isSuccessful) AnswerState.SUCCESS else AnswerState.FAIL,
                                answer.code() == 200
                            )
                        )
                    }
                } else _answerFlow.emit(Pair(AnswerState.FAIL, false))
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
                _answerFlow.emit(Pair(AnswerState.FAIL, false))
            }
        }*/
    }

    fun sendCodeForVerification(sendCodeRequest: CodeCell) {
        viewModelScope.launch(Dispatchers.IO) {
            log("sending code")
            val codeVerifyResponse = service.sendCode(sendCodeRequest)
            if(codeVerifyResponse.isSuccessful) {
                val response = codeVerifyResponse.body()
                if(response != null) {
                    log("codeVerifyResponse is success: $response")
                    _codeConfirmState.update { response }
                }
            } else {
                val error = codeVerifyResponse.message()
                log(error)
            }
        }
    }

    fun resendCodeRequest(number: PhoneNumber) {
        viewModelScope.launch {
            try {
                val result = service.sendNumber(number)
                Log.d("@@@", "resendCodeRequest ${result.toString()}")
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}