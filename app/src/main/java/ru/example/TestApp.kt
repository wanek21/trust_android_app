package ru.example

import android.app.Application
import com.google.android.gms.security.ProviderInstaller
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import ru.example.di.module
import javax.net.ssl.SSLContext


class TestApp: Application() {

    override fun onCreate() {
        super.onCreate()
        ProviderInstaller.installIfNeeded(applicationContext);
        /*val sslContext = SSLContext.getInstance("TLSv1.3")
        sslContext.init(null, null, null)
        val engine = sslContext.createSSLEngine()*/
        startKoin {
            androidContext(this@TestApp)
            androidLogger(Level.DEBUG)
            androidLogger(Level.ERROR)
            modules(module)
        }
    }
}