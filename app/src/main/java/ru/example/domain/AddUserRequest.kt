package ru.example.domain

import com.google.gson.annotations.SerializedName
import ru.example.ui.fragments.addUserFragment.AddUserProfileForm

data class AddUserRequest(
    @SerializedName("user_add") val addUserProfileForm: AddUserProfileForm,
    @SerializedName("phone") val phoneNumber: PhoneNumber
)
