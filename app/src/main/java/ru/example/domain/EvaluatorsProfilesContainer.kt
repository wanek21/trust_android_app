package ru.example.domain

import com.google.gson.annotations.SerializedName

data class EvaluatorsProfilesContainer(
    @SerializedName("RESULT")
    val profiles: List<EvaluatorsProfileEntity>,
    @SerializedName("access_token")
    val token: String,
    val ERROR: String
)
