package ru.example.domain

import com.google.gson.annotations.SerializedName

data class GetEvaluatorsRequest(
    val selection: Selection,
    @SerializedName("rating_in") val ratingIn: RatingIn
)

data class RatingIn(
    @SerializedName("user_id") val userId: Int
)

