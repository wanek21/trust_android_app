package ru.example.domain

import android.content.Context
import java.security.KeyStore
import java.security.cert.Certificate

class KeyStoreManager(
    private val context: Context
) {

    private companion object{
        const val KS_NAME = "trust_store.bks"
        const val CERT_NAME = "trust_ca"
    }

    private val keyStore = KeyStore.getInstance("BKS").apply {
        load(context.assets.open(KS_NAME), "4zJY3NJ56JJJ".toCharArray())
    }

    fun getCert(): Certificate =
        keyStore.getCertificate(CERT_NAME)


    fun getKeystoreWithPassword(): Pair<KeyStore, CharArray> {
        return keyStore to "4zJY3NJ56JJJ".toCharArray()
    }



}