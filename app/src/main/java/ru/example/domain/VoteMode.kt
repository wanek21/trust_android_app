package ru.example.domain

enum class VoteMode {

    IDLE, UP, DOWN
}