package ru.example.domain

import android.annotation.SuppressLint
import android.util.Log
import okhttp3.CertificatePinner.Companion.sha1Hash
import okhttp3.OkHttpClient
import ru.example.preferences.Preferences
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.*

class HttpClientFactory(
    private val keyStoreManager: KeyStoreManager
) {


    private val timeout = 5000L


    fun createClient(): OkHttpClient {
        val okBuilder = OkHttpClient.Builder().also {
            it.connectTimeout(timeout, TimeUnit.SECONDS)
            it.readTimeout(timeout, TimeUnit.SECONDS)
        }
        @SuppressLint("CustomX509TrustManager")
        val trustManager = object: X509TrustManager {

            @SuppressLint("TrustAllX509TrustManager")
            override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {
                Log.d("@@@", "ASK CERT")
            }


            @SuppressLint("TrustAllX509TrustManager")
            override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {
//                val shaReceived = chain?.first()?.sha1Hash()
//                val shaStored = (keyStoreManager.getCert() as X509Certificate).sha1Hash()
//                Log.d("@@@", "$shaReceived ||||| $shaStored")
//                if(shaReceived != shaStored){
//                    Log.d("@@@", "MISS CERT")
//                    throw CertificateException()
//                }
            }


            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return emptyArray()
            }

        }
        val keyManager = KeyManagerFactory.getInstance("X509").also {
            val keyVaultSpec = keyStoreManager.getKeystoreWithPassword()
            it.init(keyVaultSpec.first, keyVaultSpec.second )
        }
        val sslContext = SSLContext.getInstance("TLSv1.3").also {
            it.init(keyManager.keyManagers, arrayOf(trustManager), SecureRandom())
        }

        okBuilder.also {
            it.sslSocketFactory(sslContext.socketFactory, trustManager)
            it.hostnameVerifier { _, _ -> true }
        }
        return okBuilder.build()
    }

}