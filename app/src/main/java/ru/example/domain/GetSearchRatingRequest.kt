package ru.example.domain

import com.google.gson.annotations.SerializedName

data class GetSearchRatingRequest(
    @SerializedName("user_id") val userId: Int
)
