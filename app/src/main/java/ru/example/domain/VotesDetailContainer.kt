package ru.example.domain

import com.google.gson.annotations.SerializedName

data class VotesDetailContainer(
    @SerializedName("votes")
    val votes: List<VoteDetailEntity>
)
