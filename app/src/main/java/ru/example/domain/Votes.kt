package ru.example.domain

enum class Votes {

    TRUST_1,
    TRUST_2,
    TRUST_3,
    UN_TRUST_1,
    UN_TRUST_2,
    UN_TRUST_3,

}