package ru.example.domain

import com.google.gson.annotations.SerializedName

data class RateProfile (
    @SerializedName("RESULT")
    val profile: ProfileEntity,
    @SerializedName("access_token")
    val token: String,
    val ERROR: String
)
