package ru.example.domain

import com.google.gson.annotations.SerializedName

data class Answer (
    val STATUS: String,
    @SerializedName("access_token")
    val access_token: String
)
