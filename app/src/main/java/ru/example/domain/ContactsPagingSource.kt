package ru.example.domain

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import ru.example.Api
import ru.example.BuildConfig
import ru.example.log

class ContactsPagingSource(
    private val api: Api,
    private val contacts: List<String>,
) : PagingSource<Int, ProfileEntity>() {


    override fun getRefreshKey(state: PagingState<Int, ProfileEntity>): Int? {
        val anchorPos = state.anchorPosition ?: return null
        val page = state.closestPageToPosition(anchorPos) ?: return null
        return page.prevKey?.plus(1) ?: page.nextKey?.minus(1)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProfileEntity> {
        val pageIndex = params.key ?: 0
        return try {
            val response = when {
                contacts.isNotEmpty() ->
                    api.getUserContactList(
                        getUserContactsRequest = GetUserContactsRequest(
                            selection = Selection(
                                offset = 10 * pageIndex,
                                limit = 10
                            ),
                            contacts = Contacts(contacts)
                        )
                    ).also {
                        Log.d("@@@", "ContactsPagingSource body ${it.body()}")
                        /*Log.d("@@@", "ContactsPagingSource TOKEN ${TokenManager.access_token}")
                        Log.d("@@@", "ContactsPagingSource SIZE ${it.body()?.profiles?.size}")
                        Log.d("@@@", "ContactsPagingSource response success ${it.isSuccessful}")
                        if (it.isSuccessful) {
                            TokenManager.access_token = it.body()?.token
                            Log.d(
                                "@@@",
                                "ContactsPagingSource TokenManager body token ${it.body()?.token}"
                            )
                            Log.d("@@@", "1 SUCCESS " + it.body()?.toString())

                        } else {
                            Log.d("@@@", "1 ERRR " + it.body()?.ERROR.toString())
                        }*/
                    }
                    /*api.getUserContactListOld(contacts, 10 * pageIndex, 10)
                        .also {
                            Log.d(
                                "@@@",
                                "ProfilesPagingSource ${
                                    api.getUserContactListOld(
                                        contacts,
                                        10 * pageIndex,
                                        10
                                    )
                                }"
                            )
                            Log.d("@@@", "ContactsPagingSource body ${it.body()}")
                            Log.d("@@@", "ContactsPagingSource TOKEN ${TokenManager.access_token}")
                            Log.d("@@@", "ContactsPagingSource SIZE ${it.body()?.profiles?.size}")
                            Log.d("@@@", "ContactsPagingSource response success ${it.isSuccessful}")
                            if (it.isSuccessful) {
                                TokenManager.access_token = it.body()?.token
                                Log.d(
                                    "@@@",
                                    "ContactsPagingSource TokenManager body token ${it.body()?.token}"
                                )
                                Log.d("@@@", "1 SUCCESS " + it.body()?.toString())

                            } else {
                                Log.d("@@@", "1 ERRR " + it.body()?.ERROR.toString())
                            }
                        }*/

                else -> {
                    throw IllegalStateException()
                }
            }
            log("response from contacts: ${response.message()}")
            log("response from contacts: ${response.body().toString()}")
            if (response.isSuccessful && response.body() != null) {
                return LoadResult.Page(
                    data = response.body()!!,
                    prevKey = if (pageIndex == 0) null else pageIndex - 1,
                    nextKey = if (response.body()!!.size == params.loadSize) pageIndex + 1 else null
                )
            } else throw IllegalArgumentException(response.code().toString())
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
            LoadResult.Error(e)
        }
    }
}