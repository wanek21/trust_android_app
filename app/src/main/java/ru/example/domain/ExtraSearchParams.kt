package ru.example.domain

data class ExtraSearchParams(
    val firstName: String? = null,
    val lastName: String? = null,
    val patronymic: String? = null,
    val birthdate: String? = null,
    val region: String? = null,
    val city: String? = null,
) {
    val firstNameOrNull: String?
        get() = if (firstName.isNullOrBlank()) null else firstName

    val lastNameOrNull: String?
        get() = if (lastName.isNullOrBlank()) null else lastName

    val patronymicOrNull: String?
        get() = if (patronymic.isNullOrBlank()) null else patronymic

    val birthdateOrNull: String?
        get() = if (birthdate.isNullOrBlank()) null else birthdate

    val regionOrNull: String?
        get() = if (region.isNullOrBlank()) null else region

    val cityOrNull: String?
        get() = if (city.isNullOrBlank()) null else city
}
