package ru.example.domain

import com.google.gson.annotations.SerializedName

data class PhoneNumber(
    @SerializedName("country_code") val code: Int,
    @SerializedName("number") val number: Long
)
