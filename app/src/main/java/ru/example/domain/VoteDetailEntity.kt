package ru.example.domain

import com.google.gson.annotations.SerializedName

data class VoteDetailEntity(
    @SerializedName("vote_mode")
    val voteMode: Int,
    @SerializedName("vote_type")
    val voteType: Int,
    @SerializedName("vote_date")
    val voteDate: String
)
