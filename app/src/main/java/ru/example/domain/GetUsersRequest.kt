package ru.example.domain

import com.google.gson.annotations.SerializedName

data class GetUsersRequest(
    @SerializedName("selection") val selection: Selection,
    @SerializedName("user_search") val params: Params?
)

data class Selection(
    val offset: Int,
    val limit: Int,
)

data class Params(
    @SerializedName("phone_number") val phoneNumber: String?,
    val surname: String?,
    val name: String?,
    val patronymic: String?,
    @SerializedName("birthdate") val birthdate: String?,
    @SerializedName("region") val region: String?,
)
