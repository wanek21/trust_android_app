package ru.example.domain

import com.google.gson.annotations.SerializedName

data class RateUserRequest(
    @SerializedName("user_id") val idUser: Int,
    @SerializedName("feedback") val feedback: Boolean,
    @SerializedName("score") val score: Int
)