package ru.example.domain

object TokenManager {

    var access_token: String? = null
    var refreshToken: String? = null
}