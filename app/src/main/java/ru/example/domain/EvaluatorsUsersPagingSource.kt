package ru.example.domain

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import ru.example.Api
import ru.example.BuildConfig

class EvaluatorsUsersPagingSource(
    private val api: Api,
    private val id_user: Int
) : PagingSource<Int, EvaluatorsProfileEntity>() {


    override fun getRefreshKey(state: PagingState<Int, EvaluatorsProfileEntity>): Int? {
        val anchorPos = state.anchorPosition ?: return null
        val page = state.closestPageToPosition(anchorPos) ?: return null
        return page.prevKey?.plus(1) ?: page.nextKey?.minus(1)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, EvaluatorsProfileEntity> {
        val pageIndex = params.key ?: 0
        return try {
            val response = when {
                id_user != 0 ->
                    api.getEvaluatorsUsersList(
                        getEvaluatorsRequest = GetEvaluatorsRequest(
                            selection = Selection(
                                offset = 10 * pageIndex,
                                limit = 10
                            ),
                            ratingIn = RatingIn(id_user)
                        )
                    ).also {
                        Log.d("@@@", "EvaluatorsUsersPagingSource body ${it.body()}")
                        //Log.d("@@@", "EvaluatorsUsersPagingSource TOKEN ${TokenManager.access_token}")
                        //Log.d("@@@", "EvaluatorsUsersPagingSource SIZE ${it.body()?.profiles?.size}")
                        Log.d("@@@", "EvaluatorsUsersPagingSource response success ${it.isSuccessful}")
                        if (it.isSuccessful) {
                            //TokenManager.access_token = it.body()?.token
                            Log.d("@@@", "1 SUCCESS " + it.body()?.toString())

                        } else {
                            Log.d("@@@", "1 ERRR " + it.errorBody().toString())
                        }
                    }
                    /*api.getEvaluatorsUsersListOld(
                        user_id = id_user,
                        offset = 10 * pageIndex,
                        limit = 10
                    )
                        .also {
                            Log.d(
                                "@@@",
                                "EvaluatorsUsersPagingSource ${
                                    api.getEvaluatorsUsersListOld(
                                        id_user,
                                        10 * pageIndex,
                                        10
                                    )
                                }"
                            )
                            Log.d("@@@", "EvaluatorsUsersPagingSource body ${it.body()}")
                            Log.d("@@@", "EvaluatorsUsersPagingSource TOKEN ${TokenManager.access_token}")
                            Log.d("@@@", "EvaluatorsUsersPagingSource SIZE ${it.body()?.profiles?.size}")
                            Log.d("@@@", "EvaluatorsUsersPagingSource response success ${it.isSuccessful}")
                            if (it.isSuccessful) {
                                TokenManager.access_token = it.body()?.token
                                Log.d(
                                    "@@@",
                                    "EvaluatorsUsersPagingSource TokenManager body token ${it.body()?.token}"
                                )
                                Log.d("@@@", "1 SUCCESS " + it.body()?.toString())

                            } else {
                                Log.d("@@@", "1 ERRR " + it.body()?.ERROR.toString())
                            }
                        }
*/
            else -> {
                throw IllegalStateException()
            }

        }
        if (response.isSuccessful && response.body() != null) {
            val evaluatorsList = response.body()
                return LoadResult.Page(
                    data = response.body()!!,
                    prevKey = if (pageIndex == 0) null else pageIndex - 1,
                    nextKey = if (response.body()!!.size == params.loadSize) pageIndex + 1 else null
                )
            } else throw IllegalArgumentException(response.code().toString())
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
            LoadResult.Error(e)
        }
    }
}