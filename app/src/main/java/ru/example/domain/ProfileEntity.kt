package ru.example.domain

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProfileEntity(
    val id: Int,
    @SerializedName("likes")
    val approves: Int,
    @SerializedName("dislikes")
    val disapproves: Int,
    val country: String,
    @SerializedName("region") val city: String?,
    val isConfirmed: Boolean,
//    @SerializedName("is_confirmed")
//    val isConfirmed: Boolean,
//    @SerializedName("voted_mode")
//    val votedMode: Int, //0 - None, 1 - up, 2 - down
//    @SerializedName("is_hidden")
//    val isHidden : Boolean,
    @SerializedName("phone_number")
    val phoneNumber: String,
    @SerializedName("fullname")
    val fullName: String?,
    //@SerializedName("fullname") val fullName: String,
    @SerializedName("name")
    val name: String?,
    @SerializedName("birthdate")
    val birthDate: String?,
    @SerializedName("file_name")
    val userPic: String?,
    val rating: Float,
    @SerializedName("statistics_id")
    val statisticId: Int?,
    @SerializedName("is_active") val isActive: Boolean
): Parcelable {


}
