package ru.example.domain

import com.google.gson.annotations.SerializedName

data class ProfilesContainer(
    @SerializedName("RESULT")
    val profiles: List<ProfileEntity>,
    @SerializedName("access_token")
    val token: String?,
    val ERROR: String?
)
