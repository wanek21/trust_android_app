package ru.example.domain

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import ru.example.Api
import ru.example.log

class ProfilesPagingSource(
    private val api: Api,
    private val query: String,
    private val region: String,
    private val extraParams: ExtraSearchParams?
) : PagingSource<Int, ProfileEntity>() {


    override fun getRefreshKey(state: PagingState<Int, ProfileEntity>): Int? {
        Log.d("getProfiles", "getRefreshKey state.anchorPosition ${state.anchorPosition}")
        val anchorPosition = state.anchorPosition ?: return null
        val page = state.closestPageToPosition(anchorPosition) ?: return null
        return page.prevKey?.plus(1) ?: page.nextKey?.minus(1)
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProfileEntity> {
        Log.d("@@@", "try load")
        val pageIndex = params.key ?: 1
        val pageSize = params.loadSize
        val limit = if (pageIndex == 0) 10 else pageIndex * 10

        Log.d("getProfiles", "pageIndex $pageIndex pageSize $pageSize")

        val response =
            if (query.isNotEmpty()) {
                log("LOADING WITH QUERY: $query")
                val request = GetUsersRequest(
                    selection = Selection(
                        offset = if (pageIndex == 0) 0 else limit - 10,
                        limit = limit
                    ),
                    params = null
                )
                api.findUsersByFullname(
                    query, // fullname
                    Selection(
                        offset = if (pageIndex == 0) 0 else limit - 10,
                        limit = limit
                    )
                )
                /*api.getProfilesDefault(
                    query = query,
                    limit = limit,
                    offset = if (pageIndex == 0) 0 else limit - 10
                )*/
            } else {
                log("LOADING WITH EXTRA PARAMS: $extraParams")
                val request = GetUsersRequest(
                    selection = Selection(
                        offset = if (pageIndex == 0) 0 else limit - 10,
                        limit = limit
                    ),
                    params = Params(
                        surname = extraParams?.lastNameOrNull,
                        name = extraParams?.firstNameOrNull,
                        patronymic = extraParams?.patronymicOrNull,
                        birthdate = extraParams?.birthdateOrNull,
                        region = extraParams?.regionOrNull,
                        phoneNumber = null,//extraParams?. ?: "",
                    )
                )
                log("here")
                api.getUsers(request)
                /*api.getAdvancedUsers(
                    surname = extraParams?.lastName ?: "",
                    name = extraParams?.firstName ?: "",
                    patronymic = extraParams?.patronymic ?: "",
                    birthdate = extraParams?.birthdate ?: "",
                    city = extraParams?.city ?: "",
                    limit = limit,
                    offset = if (pageIndex == 0) 0 else limit - 10
                )*/
            }
        return if (response.isSuccessful) {
            Log.d("getProfiles", "load ${response}")
            val profiles = checkNotNull(response.body())
            val nextKey = if (profiles.isEmpty()) null else pageIndex + 1
            val prevKey = if (pageIndex > 1) pageIndex - 1 else  null

            Log.d("getProfiles", "nextKey $nextKey prevKey $prevKey")
            LoadResult.Page(
                data = profiles,
                prevKey = prevKey,
                nextKey = nextKey
            )
        } else {
            Log.d("getProfiles", "err $response")
            LoadResult.Error(HttpException(response))
        }





//        return try {
//            val response = when {
//                region.isEmpty() ->
//
////getProfile  setGetProfileListener 2 ProfileEntity(id=1, approves=6, disapproves=2, country=Украина, city=Kalanchak, isConfirmed=false, phoneNumber=9231500264, fullName=Ladlow Travis, name=Travis, birthDate=27.11.71, userPic=/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wgARCAQAAsYDASIAAhEBAxEB/8QAGgAAAwEBAQEAAAAAAAAAAAAAAAECAwQFBv/EABkBAQEBAQEBAAAAAAAAAAAAAAABAgMEBf/aAAwDAQACEAMQAAACQjj0GnAAMTBpg04AAYDABMgaYAKAAAACAFAAAKAIAAgBpgJgAAIABDBJqkAAAAAAMAAcJgAAAAmgAoEAmgaEaBTTO5aJMsgNwYAADAAcDTBpwAKwAAQYKAAAAAAAAgAAAAAACYJhaASAFOWCAgTVIAEAAUAABDBgNAMgAVDESaoBABSAAAAQXDloRlAG4xMGEAMQwAYNOUaYAA0wAAGIYIYJoGhiAABAAAAAAQNpgAIEAAJqkCAAAAAAGDHAmAAoAgCBNAmUgATQNFCaBolYjNQG8jHA0xNgigltKMcJsEUhNuENAMoABMhDLEMpASgAhggEAAABpgAIEAAgVCy5dTvE5QCAAYMGEA0AMQAAgTQJqgAAATQAAmpQCVjNRMIYAxsWkuKQCVKk04BAxMGgaYJgGM8PTDPXrpw8afbJPG9PHGduwDl1AAUY3PSYFbnMzoFWaI59ToWO0HJee84V7Jvh5l9vjrv053z9DMKNTn2i2sjc5tTQDNHnlqdK59ilgjcmpRRlp0GWsCZAIAAEyUAyYGgAAAVIUIKJCkIpwyyXDABoG0wCbPJ9nyfV9Hkfjer50ufrT5U36/nZd67sOHcx2jU8vr5PY7+bgMt0J9DnufN6uDrz3z6e7lvHHm9mTx/Z8j1V4F6OaeYezxrz516zXBy+znefLz+sHhd3L6s3zc/q8DK05enPo5unH098OHj9rwo73tvc+P0dfiTr09XXV5efh14Z7diDj2AAAAHKDM2QNwEQxANA3LGJg0DaB1LihMGnA0wGank+p5HqeryX5HqxrGnNhfPvzei+Oa9JW/N2zNnXj9nB3enzeXvz9Gnp8XXx64ef7fh+znrt4Hu+Vc+lry9Fxx93jetNz4nu+GvvPHS8/E9jxvWz16PD9rxbn1tufa487p4uydOuTz7z25/Qprx/a8v0pV4nteJNen2cSuO7Koudjk6U8/fDbl69QOPUAUAQqdM1FGWIPZAAmCAGJjExtAwBuXFNMYnDaRQmc/nezh1wq81ejzenHn0p2V0cO4Bx6PyPW8frjfrjg9Xm16OIzfTz4NtYyjrwx19A80uPS48OzO+S/V4MdOqfMfXh6nDh0TfF6nD36zrJz749WXn1z7c/s8MTXqYcRrHpLzpl37vKa+p5Tg9Tm55OvnTzvu18p759/DHTz6dQHDuAIAKbY9OElmbyCfSAAACAAEMQW4ZTkGIKqHGhNQNMYgbQMChNABAmh8nWWeP6eptI1inB3mp5XqtghZoCCXISzTUlJ5HpZc3o49+fJrrOXqqvN3SZigFCciBACtAaJgqAQAUAAHCYB18vbhJZm+aB1jAAASqQTBABUhZLKEQxBpWdw2gbQNoG5atAgCAENyFJFCEMQVUMEJGkKRUUCEoEpUWO5aXWbgQwBKk1YlWkuJv0HAellHEaRSTKQAAxMYmyDu4e/IAxfKE+0GmMQMQCaEAIAbkLctQAdQ41E4GmMBKESsAQIE1Zl5+lerlznaenz8krtx04jrvWOJ9N1xLug4+io57ynvneOM7gw6eLHh23z6dOvPj06YsXNh343g+uenPp4Obo49sztnvxy7vK9TxerTq4jlrvXBUduJpHLXVqck9jOM7CXiz9GTij0IOLvx3RDM3ya4OjvNTowzQdkDQJhKp2Zq4ASq3FRTlqxEXeVxQBTlxQOEwBMqU0keV7HN2xL5un6fg4uuss74319nk9Xj+jw93q80+b6WON8+/d5nPfdlrh6/LHV53o51OO0LrxdvLZe/F2xyTcc+vaTXfz8+7yzrXLXKxdnH1/L+iAudbQMQNy4pyzS8GdT5COtcqOvbzvSEMT5rXDu6XuULjurdSc2VR0AixiAzubJTmhoTR51LZLldSS7kXI3Njelpg+ijlOkORdcnLlv4nXM+hjX0fJrydWGXr8Pr+R8/18XZx6fR8m/Dvz416nm9/Bx7d01Xv8OGuOXLpWT24ejeePXvw005s83Lpz1x0pLl1nuxw0XujJdOUd/m+l8/2sT5bAEGmraqEwGCAQCFT9Ty/TmaJE+Y3531vr8nPrz336RvzvJlvz7jQWAFCYZxrlYE7JOlzNQ9MY1SwXp6ODtiN+eY9Tfyu1noWak1rDQWfKmsvL9Hi9HPqXCvZ5tOjLq8nqro5+7zdFweznceTp281vnabcHq5dxwHp857Ple14/TxPTPj0eOzsy6JtVy7wY6BqOKqMHsA080E1YmjoJWAAAIQIAAo9Hzu+ZsgzPnCl6CuRd/T8fo5a6+bfmltyWbQpN8YVlQ5o7OKzCQ1HUM0cXLe2FY11Yw5Xtg5Ou+Wsux8tFcuua5qlsCdlA819HO47tfM1k6ePeTlVRs5Fo+nleZ6fFp1ScL7szBXa8s9OFQdOhx30dUefn6XOcppFJjoocDCAECRQIAAaTDt4uvMokxnxCn6tRWem8zW/CvXty9HHd6ZY5vdjkrN75c7Lyxnpm9ed2UJjERV51LrUXjVVLlapQ9M9M2wqSJ0m3KdYqWFOh5CpEgVQOMylZBrtWGfr8kmHbz1HdhTk5/NvHpRBo9sqj0NPPXO7LDWvW4uvLM5HpOi0NIzWjOedZrMvVec22jjfUHIb0c3Tz6zNkGM+dc5+ra9rgw9GOnTzMZF1ep85G+nsfOG8+7sfPraTnX0z0+ZO72ZPmH9L4C4n0dHzb7XHJWnYefWukvHt7HJWXLz+9l4/d2+bVcle8fPaen5KS/dVvFr5nq8NcXNr7nbPzte1zV53oaeRJ183oTXFr7UW+djh7EmPDn7Uc888ctYx7uvaeHsnzrpnPaYQt8uzLu5OjLMw06qsz2kyOboUvn6axa52qXFUjPPeKynQoeOrLAzmOX18uu+Xh6s/Xzrl2yw9n53v9PpD5v6fwzTt05qbHHEF5c/Zz9FZcHfxH0Xl+p5ejuKivZjzbefbHWTrzw77fC9Pg7ZPO9vxvaPF7+DvMOmlJwaxVt93D7XN4HVz77eb6/ketXJXB9KR4npOTDn3ytx9bzfeOO+Trtw5e04zSK8vtXXf53Ju+nt82/GfrEmXToRLUyUMJjIV7zjLpwejgvFrWim1kmUXIsevK3i03xuQZMxXMdd8fv8Aj4enn6+XAqePpYc76PiuumfajyJrv4lvHo14pS6jmy024zTu59lEVyyeryRqTrnKejXm7W+X9Hv4cnZlz9a+f3Tznq+X7nFzXfknUfU+d38p83sc/Ry+tlhT9Pj618v2PK9zm8fKHse94/Mcn0fDynpPQ414+e+7r6OX0uLpjly8+u/Ti1NKwiOxc+pVTdk49RuZGqOTbOudid4zqsdRcKpDIzrfj257miRmnY682Pdw7iEty3ntCeYavKjSZJWkWO8kojezLsweSy6mp6WGeM9PH28FWcTt9hcO7NvIlusKNSQckVpry6xrrgtTqyyDx59Tk6w1S53NXlNbTmFEM21w2y11x7UwOlZmPL3ci5PbK2XIWzr1C5z1jVYTHQsFGl8sZvZPJUvS8sJraeb
//                api.getAdvancedUsers(surname = extraParams?.lastName, limit = 20, offset = 0)
//
////                 api.getProfilesPageDefault(query, 0, 10)
//                    .also {
//                    Log.d(
//                        "getProfiles",
//                        "getAdvancedUsers ${
//                            api.getAdvancedUsers(
//                                "",
//                                limit = 0,
//                                offset =  10
//                            )
//                        }"
//                    )
////                    Log.d(
////                        "@@@",
////                        "ProfilesPagingSource ${
////                            api.getProfilesPageDefault(
////                                query,
////                                20 * pageIndex,
////                                20,
////                                TokenManager.access_token
////                            )
////                        }"
////                    )
//                    Log.d("getProfiles", "ProfilesPagingSource body ${it.body()}")
//                    Log.d("getProfiles", "ProfilesPagingSource TOKEN ${TokenManager.access_token}")
//                    Log.d("getProfiles", "ProfilesPagingSource SIZE ${it.body()?.profiles?.size}")
//                    Log.d("getProfiles", "ProfilesPagingSource response success ${it.isSuccessful}")
//                    if (it.isSuccessful) {
//                        TokenManager.access_token = it.body()?.token
//                        Log.d(
//                            "getProfiles",
//                            "ProfilesPagingDefault TokenManager body token ${it.body()?.token}"
//                        )
//                        Log.d("getProfiles", "1 SUCCESS " + it.body()?.toString())
//
//                    } else {
//                        Log.d("getProfiles", "1 ERRR " + it.body()?.ERROR.toString())
//                    }
//                }
//
////                query.isNotEmpty() && extraParams == null -> api.getProfilesPageWithQuery(
////                    region,
////                    query,
////                    10 * pageIndex,
////                    10
////                ).also {
////                    if (it.isSuccessful) {
////                        TokenManager.access_token = it.body()?.token
////                        Log.d("@@@", "3 SUCCESS " + it.body()?.toString())
////                    } else {
////                        Log.d("@@@", "3 ERRR " + it.body()?.ERROR.toString())
////                    }
////                }
////
////                extraParams != null -> api.getProfilesPageExtraParams(
////                    region,
////                    extraParams.firstName,
////                    extraParams.lastName,
////                    extraParams.patronomic,
////                    query,
////                    20 * pageIndex,
////                    20
////                ).also {
////                    if (it.isSuccessful) {
////                        TokenManager.access_token = it.body()?.token
////                        Log.d("@@@", "4 SUCCESS " + it.body()?.toString())
////                    } else {
////                        Log.d("@@@", "4 ERRR " + it.body()?.ERROR.toString())
////                    }
////                }*/
//
//                else -> {
//                    throw IllegalStateException()
//                }
//            }
//            if (response.isSuccessful && response.body() != null) {
//                return LoadResult.Page(
//                    data = response.body()!!.profiles,
//                    prevKey = if (pageIndex == 0) null else pageIndex - 1,
//                    nextKey = if (response.body()!!.profiles.size == params.loadSize) pageIndex + 1 else null
//                )
//            } else throw IllegalArgumentException(response.code().toString())
//        } catch (e: Exception) {
//
//            Log.d("getProfiles", " err $e")
//
//            if (BuildConfig.DEBUG) {
//                e.printStackTrace()
//            }
//            LoadResult.Error(e)
//        }
        }
    }