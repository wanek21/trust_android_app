package ru.example.domain

import com.google.gson.annotations.SerializedName

data class SearchRatingResponse(
    val feedback: Boolean
)
