package ru.example.domain

import com.google.gson.annotations.SerializedName

data class GetUserContactsRequest(
    @SerializedName("selection") val selection: Selection,
    @SerializedName("contacts") val contacts: Contacts?
)

data class Contacts(
    @SerializedName("phone_numbers") val numbersList: List<String>
)
