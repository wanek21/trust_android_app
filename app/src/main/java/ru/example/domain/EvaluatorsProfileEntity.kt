package ru.example.domain

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EvaluatorsProfileEntity(
    val id: Int,
    @SerializedName("path_to_avatar") val image_to_bytes: String,
    val phone_number: String,
    val surname: String,
    val name: String,
    val patronymic: String,
    @SerializedName("full_name")
    val fullName: String,
    @SerializedName("birthdate")
    val birthDate: String,
    val country: String,
    val city: String,
    val approved: Boolean,
    @SerializedName("statistics_id")
    val statisticId: Int,
    @SerializedName("likes")
    val approves: Int,
    @SerializedName("dislikes")
    val disapproves: Int,
    val rating: Float,
    val date: String,
    val feedback: Boolean,
    val score: Int
): Parcelable