package ru.example.domain

import com.google.gson.annotations.SerializedName

data class ComplaintUserRequest(
    @SerializedName("user_id") val userId: Int,
    val reason: String
)
