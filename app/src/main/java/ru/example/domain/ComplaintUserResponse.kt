package ru.example.domain

data class ComplaintUserResponse( // объект жалобы
    val complaining_user_id: Int,
    val date: String,
    val id: Int,
    val is_reviewed: Boolean,
    val reason: String,
    val user_id: Int
)
