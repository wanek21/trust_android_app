package ru.example.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import ru.example.ApiService
import ru.example.R
import ru.example.databinding.ActivityMainBinding
import ru.example.domain.TokenManager
import ru.example.log
import ru.example.ui.fragments.mainFragment.MainFragmentViewModel


class MainActivity : AppCompatActivity() {


    private lateinit var binding: ActivityMainBinding
    private val navController by lazy {
        findNavController(R.id.container)
    }

    private val mainViewModel by viewModels<MainFragmentViewModel>()

    var isExtraFilterEnabled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_TestApplication)
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?. setBackgroundDrawable(ContextCompat.getDrawable (this, R.color.black))

        setupToolbar()

        val sharedPreferences = getSharedPreferences("app_prefs", Context.MODE_PRIVATE)
        val accessToken = sharedPreferences.getString(ApiService.TOKEN_KEY, null)
        if(!accessToken.isNullOrEmpty()) {
            log("token: $accessToken")
            TokenManager.access_token = accessToken
            navController.navigate(R.id.action_authFragment_to_mainFragment/*, args = null, navOptions*/)
        }
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.about-> {
                Log.i("AAA", "Click about menu")
                // Обработка нажатия на пункт меню "Настройки"
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (isExtraFilterEnabled) {
            // Отключаем фильтр и обновляем список
            isExtraFilterEnabled = false
            mainViewModel.clearExtraParams()
            Toast.makeText(this, "Фильтр выключен", Toast.LENGTH_SHORT).show()
        } else {
            super.onBackPressed()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.container)
        return if (onBackPressedDispatcher.hasEnabledCallbacks()) {
            onBackPressedDispatcher.onBackPressed()
            true
        } else
            navController.navigateUp() || super.onSupportNavigateUp()
    }

    fun setupToolbar() {
        setupActionBarWithNavController(navController)
    }
}
