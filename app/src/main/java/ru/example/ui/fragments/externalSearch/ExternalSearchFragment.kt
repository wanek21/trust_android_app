package ru.example.ui.fragments.externalSearch

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import ru.example.R
import ru.example.databinding.ExternalSearchLayoutBinding
import ru.example.domain.ExtraSearchParams
import ru.example.ui.MainActivity
import ru.example.ui.fragments.CodeConfirmFragment
import ru.example.ui.fragments.mainFragment.MainFragmentViewModel
import ru.example.ui.fragments.registrationFragment.DatePickerDialogHelper


class ExternalSearchFragment : Fragment() {

    private var _binding: ExternalSearchLayoutBinding? = null
    private val binding get() = checkNotNull(_binding)
    private val viewModel: MainFragmentViewModel by sharedViewModel<MainFragmentViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        goneCustomActionBar()

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.findItem(R.id.menu).isVisible = false
    }

    override fun onResume() {
        super.onResume()
        goneCustomActionBar()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ExternalSearchLayoutBinding.inflate(layoutInflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        //  subscribe()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun goneCustomActionBar() {
        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar
        actionBar?.displayOptions = ActionBar.DISPLAY_SHOW_TITLE
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requireActivity().invalidateOptionsMenu()
    }


    private fun initViews() {

        val datePickerDialogHelper = DatePickerDialogHelper(requireContext(), binding.birthDateEt)

        binding.iconCalendar.setOnClickListener {
            datePickerDialogHelper.showDatePickerDialog()
        }

        binding.searchButton.isEnabled = true

        binding.searchButton.setOnClickListener {
            // viewModel.setNewQuery("Борис")
            val name = binding.nameEt.text.toString()
            val lastName = binding.surnameEt.text.toString()
            val patronymic = binding.patronymicEt.text.toString()
            val birthdate = binding.birthDateEt.text.toString()
            val city = binding.cityEt.text.toString()

            Log.d("getProfiles", "searchClick $lastName")

            viewModel.setExtraParams(
                ExtraSearchParams(
                    firstName = name.trim(),
                    lastName = lastName.trim(),
                    patronymic = patronymic.trim(),
                    birthdate = birthdate.trim(),
                    city = city.trim()
                )
            )
            (activity as MainActivity).isExtraFilterEnabled = true
            findNavController()
                .navigate(
                    R.id.action_extraSearchParamsFragment_to_mainFragment,
                    Bundle().also { it.putString(CodeConfirmFragment.code_num_key, "+79006596370") }
                )

            //     requireActivity().onBackPressed()


            /*            if (checkFields()) {
                try {
//                    viewModel.addUserProfile(
//                        AddUserProfileForm(
//                            binding.phoneEt.text.toString(),
//                            split.first,
//                            split.second,
//                            split.third,
//                            binding.birthDateEt.text.toString(),
//                            binding.countryEt.text.toString(),
//                            "${binding.regionEt.text.toString()} "
//                        ),
//                    )
                } catch (e: Exception) {
                    Snackbar.make(requireView(), "Поля заполнены неверно", Snackbar.LENGTH_SHORT)
                        .show()
                }
            } else {
                with(binding) {
                    if (birthDateEt.text.isNullOrEmpty()) {
                        birthDateLayout.setBackgroundResource(R.drawable.rounded_edittext_error)
                    }
                    surnameEt.setCustomBackgroundIfError(R.drawable.rounded_edittext_error)
                    nameEt.setCustomBackgroundIfError(R.drawable.rounded_edittext_error)
                    patronymicEt.setCustomBackgroundIfError(R.drawable.rounded_edittext_error)

                    searchButton.isEnabled = false
                }
            }*/

        }


//    private fun userAlreadyExist() {
//        with(binding) {
//            birthDateLayout.setBackgroundResource(R.drawable.rounded_edittext_error)
//            countryLayout.setBackgroundResource(R.drawable.rounded_edittext_error)
//            regionEt.setBackgroundResource(R.drawable.rounded_edittext_error)
//            nameEt.setBackgroundResource(R.drawable.rounded_edittext_error)
//
//            createProfile.isEnabled = false
//            disclaimerTv.text = getString(R.string.such_a_person_already_exists)
//            disclaimerTv.setTextColor(resources.getColor(R.color.cherry_red))
//        }
//    }


//    private fun splitField(text: Editable?): Triple<String, String, String?>? {
//        return text?.run {
//            val inputSplit = text.toString().split(" ")
//            if (inputSplit.size >= 3) {
//                Triple(inputSplit.first(), inputSplit[1], inputSplit.last())
//            } else if (inputSplit.size == 2) {
//                Triple(inputSplit.first(), inputSplit[1], null)
//            } else {
//                null
//            }
//        }
//    }

    }

    private fun EditText.setCustomBackgroundIfError(drawableResource: Int) {
        if (text.isNullOrEmpty()) {
            setBackgroundResource(drawableResource)
        }
    }

    private fun checkFields(): Boolean {
        return binding.run {
            !surnameEt.text.isNullOrEmpty() && !nameEt.text.isNullOrEmpty()
                    && !patronymicEt.text.isNullOrEmpty() && !birthDateEt.text.isNullOrEmpty()
                    && !cityEt.text.isNullOrEmpty()
        }
    }


//    private fun subscribe() {
//        lifecycleScope.launchWhenStarted {
//            viewModel.answerFlow.collectLatest {
//                if (it == AnswerState.SUCCESS && viewModel.userAlreadyAdded) {
//                    Toast.makeText(
//                        requireContext(),
//                        "User already added",
//                        Toast.LENGTH_SHORT
//                    )
//                        .show()
//
//                    userAlreadyExist()
//                }
//                if (it == AnswerState.SUCCESS && !viewModel.userAlreadyAdded) {
//                    Toast.makeText(
//                        requireContext(),
//                        "User added successful",
//                        Toast.LENGTH_SHORT
//                    )
//                        .show()
//                } else {
//                    Toast.makeText(requireContext(), "Server error", Toast.LENGTH_SHORT)
//                        .show()
//                }
//            }
//        }
//    }

}