package ru.example.ui.fragments

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.example.AnswerState
import ru.example.AuthFragmentViewModel
import ru.example.PhoneTextWatcher
import ru.example.R
import ru.example.databinding.AuthLayoutBinding
import ru.example.domain.PhoneNumber


class AuthFragment : Fragment() {

    companion object {
        const val code_num_key = "cnc"
    }

    private lateinit var binding: AuthLayoutBinding
    private val viewModel by viewModel<AuthFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = AuthLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()

        // Скрыть ActionBar
        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar
        actionBar?.hide()

      subscribe()
//        findNavController().navigateUp()
//                    findNavController().navigate(
//                        R.id.action_authFragment_to_codeConfirmFragment,
//                        Bundle().also {
//                         //   it.putString(code_num_key, binding.phoneInput.text.toString())
//                               it.putString(code_num_key, "+79006596370")
//                        }
//                    )
    }


    private fun subscribe() {
        lifecycleScope.launch {
            viewModel.answerFlow.onEach { it ->
                if (it == AnswerState.SUCCESS) {

                    findNavController().navigateUp()
                    findNavController().navigate(
                        R.id.action_authFragment_to_codeConfirmFragment,
                        Bundle().also {
                            it.putString(code_num_key, "${binding.countryCode.text}${binding.phoneInput.text}")
                            // it.putString(code_num_key, "+79006596370")
                            it.putString("code", viewModel.codeState.value)
                            it.putString("country_code", binding.countryCode.text.toString())
                            it.putString("phone_number", binding.phoneInput.text.toString())
                        }
                    )
                }
                //   Toast.makeText(requireContext(), it.name, Toast.LENGTH_SHORT).show()
            }.collect()
        }
    }

    private fun initViews() {
        //  setSpannableText()
        setupSpinner()

        binding.phoneInput.addTextChangedListener(PhoneTextWatcher(binding))
        binding.sendButton.setOnClickListener {
            val number = "${binding.countryCode.text}${binding.phoneInput.text}"
            //    val mockNumber = "+79006596370"
            val phoneNumber = PhoneNumber(
                binding.countryCode.text.toString().substring(1).toInt(),
                binding.phoneInput.text.toString().toLong()
            )
            if (number.isNotEmpty()) {
                viewModel.senCode(phoneNumber)
            } else Toast.makeText(
                requireContext(),
                "Input Your Phone",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun setupSpinner() {
        val spinner = binding.countrySpinner
        val countries = resources.getStringArray(R.array.country_codes)
        val countryCodes = resources.getStringArray(R.array.country_codes_codes)
        val adapter =
            ArrayAdapter(requireContext(), R.layout.custom_spinner_item, countries)
        adapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item)

        spinner.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                val selectedCountry = parent.getItemAtPosition(position).toString()
                val countryCode = when (selectedCountry) {
                    "Россия" -> "+7"
                    "Беларусь" -> "+375"
                    "Армения" -> "+374"
                    "Казахстан" -> "+7"
                    "Узбекистан" -> "+998"
                    "Таджикистан" -> "+992"
                    "Кыргызстан" -> "+996"
                    else -> ""
                }
                // Устанавливаем выбранный код страны
                val countryIndex = countries.indexOf(selectedCountry)
                if (countryIndex != -1) {
                    binding.countryCode.text = countryCode
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun setSpannableText() {
        val text = "+7 (XXX) XXX-XX-XX"
        val spannableString = SpannableString(text)

        spannableString.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.grey3)),
            0, // начальная позиция
            2, // конечная позиция (не включая)
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        spannableString.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.grey2)),
            2, // начальная позиция
            text.length, // конечная позиция (включая)
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        binding.phoneInput.hint = spannableString
    }
}