package ru.example.ui.fragments.admin

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.example.R

import ru.example.ui.fragments.admin.placeholder.PlaceholderContent.PlaceholderItem
import ru.example.databinding.FragmentAdminListBinding
import ru.example.log

/**
 * [RecyclerView.Adapter] that can display a [PlaceholderItem].
 * TODO: Replace the implementation with code for your data type.
 */
class UsersToBlockViewAdapter(
    private val values: List<UserToBlockItem>
) : RecyclerView.Adapter<UsersToBlockViewAdapter.ViewHolder>() {

    var onItemClick: ((Int, Int) -> Unit)? = null

    var items: MutableList<UserToBlockItem> = values.toMutableList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            FragmentAdminListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.idView.text = item.user_id.toString()
        holder.contentView.text = "Жалоба"
        holder.date.text = item.date

        holder.itemView.rootView.setOnClickListener {
            log("ivvoke ${onItemClick == null}")
            onItemClick?.invoke(item.user_id, item.complaining_user_id)
        }
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(binding: FragmentAdminListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val idView: TextView = binding.itemNumber
        val contentView: TextView = binding.content
        val date: TextView = binding.date

        override fun toString(): String {
            return super.toString() + " '" + contentView.text + "'"
        }
    }

}