package ru.example.ui.fragments.mainFragment

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import ru.example.databinding.ProfileItemLayoutBinding
import ru.example.domain.ProfileEntity

class MainProfilesAdapter: PagingDataAdapter<ProfileEntity, ProfileViewHolder>(ProfilesDiffCallBack()) {

    private var onProfileClicked: ((ProfileEntity?) -> (Unit))? = null
    private var getProfileListener: GetProfile? = null

    interface GetProfile {
        fun getProfile(profile: ProfileEntity?)
    }
    fun setGetProfileListener(listener: GetProfile) {
        this.getProfileListener = listener
    }

    override fun onBindViewHolder(holder: ProfileViewHolder, position: Int) {
        holder.onBind(getItem(holder.layoutPosition))
        holder.itemView.setOnClickListener {
            onProfileClicked?.invoke(getItem(holder.layoutPosition))
        }
      //  val profile = holder.getUserProfile(getItem(holder.layoutPosition))
       // getProfileListener?.getProfile(profile)
      //  Log.d("AAA", "getProfile onBimdViewHolder profile ${profile}")
      //  Log.d("AAA", "getProfile onBimdViewHolder ${getProfileListener?.getProfile(profile)}")
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileViewHolder {
       return ProfileViewHolder(
           ProfileItemLayoutBinding.inflate(
               LayoutInflater.from(parent.context),
               parent,
               false
           ),
           context = parent.context
       )
    }

    fun bindAction(action: (ProfileEntity?) -> Unit){
        this.onProfileClicked = action
    }

    class ProfilesDiffCallBack: DiffUtil.ItemCallback<ProfileEntity>(){

        override fun areItemsTheSame(oldItem: ProfileEntity, newItem: ProfileEntity): Boolean =
            oldItem == newItem


        override fun areContentsTheSame(oldItem: ProfileEntity, newItem: ProfileEntity): Boolean =
            oldItem == newItem


    }
}