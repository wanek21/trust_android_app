package ru.example.ui.fragments.contactsFragment

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import ru.example.data.Contact
import ru.example.databinding.ContactItemLayoutBinding
import ru.example.domain.ProfileEntity

class ContactsAdapter(private val contactsList: List<Contact>) : PagingDataAdapter<ProfileEntity, ContactsViewHolder>(ProfilesDiffCallBack()) {

    private var onProfileClicked: ((ProfileEntity?) -> (Unit))? = null
    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        holder.onBind(getItem(holder.layoutPosition))
        holder.itemView.setOnClickListener {
            onProfileClicked?.invoke(getItem(holder.layoutPosition))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsViewHolder {
        Log.d("AAA", "ContactsAdapter ${contactsList}")
        return ContactsViewHolder(
            contactsList,
            ContactItemLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    fun bindAction(action: (ProfileEntity?) -> Unit){
        this.onProfileClicked = action
    }

    class ProfilesDiffCallBack: DiffUtil.ItemCallback<ProfileEntity>(){

        override fun areItemsTheSame(oldItem: ProfileEntity, newItem: ProfileEntity): Boolean =
            oldItem.id == newItem.id


        override fun areContentsTheSame(oldItem: ProfileEntity, newItem: ProfileEntity): Boolean =
            oldItem == newItem


    }

}

