package ru.example.ui.fragments.contactsFragment

import android.content.ContentResolver
import android.provider.ContactsContract
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.stateIn
import ru.example.ApiService
import ru.example.data.Contact
import ru.example.domain.ProfileEntity
import ru.example.log

class ContactsViewmodel(
    private val apiService: ApiService,
) : ViewModel() {

    private val _contacts = MutableStateFlow<List<Contact>>(emptyList())
    val contacts: Flow<List<Contact>> get() = _contacts

    private val _numbersList = mutableListOf<String>()
    private val numberList: List<String> = _numbersList

    private val _infoMessage = MutableStateFlow<String?>(null)

    fun readContacts(contentResolver: ContentResolver) {
        val projection = setOf(
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
        ).toTypedArray()

        val cursor = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            projection,
            null,
            null,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
        )

        val contactsMap = mutableMapOf<String, Contact>()

        cursor?.use {
            try {
                while (it.moveToNext()) {
                    val contactId = it.getString(0)
                    val contactName = it.getString(2)
                    val phoneNumber = it.getString(1)

                    var formattedPhoneNumber = phoneNumber
                        .replace("[^\\d]".toRegex(), "")

                    if (formattedPhoneNumber[0] == '7') {
                        formattedPhoneNumber = formattedPhoneNumber.replaceFirst("7", "+7")
                    }

                    if (!contactsMap.containsKey(formattedPhoneNumber)) {
                        contactsMap[formattedPhoneNumber] = Contact(
                            contactId,
                            contactName,
                            formattedPhoneNumber
                        )

                        _numbersList.add(formattedPhoneNumber)
                        log("added formattedPhoneNumber: $formattedPhoneNumber")
                    }
                }
            } catch (_: Exception) {

            }
        }

        val uniqueContacts = contactsMap.values.toList()
        _contacts.value = uniqueContacts

        Log.i("AAA", "ViewModel uniqueContacts $uniqueContacts")
        Log.i("AAA", "ViewModel numberList $numberList")
    }


    private val _userContactsFlow =
        _contacts.flatMapLatest { contacts ->
            apiService.getContacts(
                numberList
            )
        }.cachedIn(viewModelScope)

    val userContactsFlow: StateFlow<PagingData<ProfileEntity>?> = _userContactsFlow
        .stateIn(viewModelScope, SharingStarted.Eagerly, null)
}
