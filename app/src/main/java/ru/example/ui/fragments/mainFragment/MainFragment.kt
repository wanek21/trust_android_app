package ru.example.ui.fragments.mainFragment

import android.Manifest
import android.R.menu
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import ru.example.R
import ru.example.data.armeniaRegionsList
import ru.example.data.belarusRegionsList
import ru.example.data.kazakhstanRegionsList
import ru.example.data.kyrgyzstanRegionsList
import ru.example.data.russiaRegionList
import ru.example.data.tadjikistanRegionList
import ru.example.data.uzbekistanRegionsList
import ru.example.databinding.FragmentContactsDialogBinding
import ru.example.databinding.MainFragmentLayoutBinding
import ru.example.domain.ExtraSearchParams
import ru.example.domain.ProfileEntity
import ru.example.log
import ru.example.preferences.Preferences
import ru.example.ui.MainActivity
import ru.example.ui.fragments.CodeConfirmFragment
import ru.example.ui.util.RegionDialog


class MainFragment : Fragment(), MainProfilesAdapter.GetProfile {

    companion object {
        const val PROFILE_KEY = "pd"
        const val code_num_key = "cnc"
        const val USER_PROFILE = "user_prorile"
        const val REQUEST_READ_CONTACTS = 79
        const val MOCK_PHONE = "+79006596370"
    }

    private lateinit var binding: MainFragmentLayoutBinding
    private val viewModel: MainFragmentViewModel by sharedViewModel<MainFragmentViewModel>() //by viewModel<MainFragmentViewModel>()
    private val adapter: MainProfilesAdapter by lazy { MainProfilesAdapter() }
    private var menuItem: MenuItem? = null
    private var currentUserProfile: ProfileEntity? = null
    private val phone by lazy {
        arguments?.getString(code_num_key)
            ?: requireContext().getSharedPreferences("app_prefs", Context.MODE_PRIVATE).getString(Preferences.PHONE, null)
            ?: MOCK_PHONE
    }
    private var userProfile: ProfileEntity? = null

    private val isAdmin by lazy {
        requireContext().getSharedPreferences("app_prefs", Context.MODE_PRIVATE).getBoolean(Preferences.IS_ADMIN, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d("getProfiles", "profiles onAttach")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        showCustomActionBar()
        Log.d("getProfiles", "profiles onCreate")
        setListeners()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentLayoutBinding.inflate(inflater, container, false)
        findBlockedUsers()
        return binding.root
        Log.d("getProfiles", "profiles onCreateView")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        phone.let { viewModel.setThisUserNumber(it) }
        Log.d("getProfiles", "profiles onViewCreated")
        binding.noResponse.visibility = View.GONE
        binding.etSearch.doOnTextChanged { text, start, before, count ->
            viewModel.setSearch(text.toString())
        }
    }

    private fun findBlockedUsers() {
        val yourBlockedUsers = requireContext().getSharedPreferences("app_prefs", Context.MODE_PRIVATE).getStringSet(Preferences.COMPLAINTS_IDS, setOf())
        log("blocked users: $yourBlockedUsers")
        val userIds = yourBlockedUsers?.map { it.toInt() }
        if(userIds != null) {
            lifecycleScope.launch {
                viewModel
                    .checkBlockedUsers(userIds)
                    .collect { fullname ->
                        if (fullname != null) {
                            Toast.makeText(requireContext(), "Ваша жалоба удовлетворена", Toast.LENGTH_SHORT).show()
                        }
                    }
            }
        }
    }


    override fun onResume() {
        super.onResume()
        showCustomActionBar()
        /*lifecycleScope.launch {
            viewModel.profilesFlow.collect()
        }*/
        Log.d("getProfiles", "profiles onResumed")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        goneCustomActionBar()
        //viewModel.viewModelScope.cancel()
        Log.d("getProfiles", "profiles onDestroyView")
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.findItem(R.id.admin)
        item.setVisible(isAdmin)
        super.onPrepareOptionsMenu(menu)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menuItem = menu.findItem(R.id.menu)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)

        val subMenu = menuItem?.subMenu
        subMenu?.let {
            for (i in 0 until it.size()) {
                val subMenuItem = it.getItem(i)
                subMenuItem.setOnMenuItemClickListener { item: MenuItem ->
                    when (item.itemId) {
                        R.id.menu_user_profile -> {

                            Log.d(
                                "AAA",
                                "getProfile  setGetProfileListener 2 $currentUserProfile"
                            )
                            findNavController().navigate(
                                R.id.action_mainFragment_to_userProfileFragment,
                                Bundle().also {
                                    it.putString(
                                        CodeConfirmFragment.code_num_key,
                                        phone
                                    )
                                })

                            true
                        }


                        R.id.about -> {
                            Toast.makeText(requireContext(), "В разработке", Toast.LENGTH_SHORT)
                                .show()
                            true
                        }

                        R.id.rules -> {
                            Toast.makeText(requireContext(), "В разработке", Toast.LENGTH_SHORT)
                                .show()
                            true
                        }

                        R.id.contacts -> {
                            Toast.makeText(requireContext(), "В разработке", Toast.LENGTH_SHORT)
                                .show()
                            true
                        }
                        R.id.admin -> {
                            findNavController().navigate(
                                R.id.action_mainFragment_to_adminFragment,
                                Bundle().also {
                                    it.putString(
                                        CodeConfirmFragment.code_num_key,
                                        phone
                                    )
                                })
                            true
                        }

                        else -> false
                    }
                }
            }
        }

    }

    override fun getProfile(profile: ProfileEntity?) {
        Log.d("AAA", "getProfile  $profile")
    }

    private fun getProfile() {
        Log.d("getProfiles", "getProfiles()")
        lifecycleScope.launchWhenStarted {
            viewModel.numberList.collectLatest { users ->
                users?.let {
                    Log.d("getProfiles", "user profile $users")
                    if(users.isNotEmpty())
                        userProfile = users[0]
                }
            }
        }
    }

    private fun setRegionDialog() {
        val userRegion = userProfile?.city
        val userCountry = userProfile?.country ?: String()
        binding.regionChooseTitle.text = userRegion ?: String()

        // Назначаем обработчик нажатия на кнопку
        binding.region.setOnClickListener {
            val regionDialog =
                RegionDialog(
                    requireContext(),
                    getRegionsList(userCountry.toString())
                ) { selectedRegion ->
                    Log.d("AAA", "Selected region: $selectedRegion")
                    binding.regionChooseTitle.text = selectedRegion
                    viewModel.setExtraParams(
                        ExtraSearchParams(
                            region = selectedRegion
                        )
                    )
                    (activity as MainActivity).isExtraFilterEnabled = true
                }
            regionDialog.show()
        }
    }


    private fun getRegionsList(country: String): List<String> {
        return when (country) {
            "Беларусь" -> belarusRegionsList
            "Армения" -> armeniaRegionsList
            "Казахстан" -> kazakhstanRegionsList
            "Узбекистан" -> uzbekistanRegionsList
            "Таджикистан" -> tadjikistanRegionList
            "Кыргызстан" -> kyrgyzstanRegionsList
            else -> russiaRegionList
        }

    }


    private fun initViews() {
        Log.d("getProfiles", "init views")
        getProfile()

        setRegionDialog()

        adapter.setGetProfileListener(object : MainProfilesAdapter.GetProfile {
            override fun getProfile(profile: ProfileEntity?) {
                if (profile != null) {
                    currentUserProfile = profile
                }
                Log.d("AAA", "getProfile  setGetProfileListener 1 $profile")
            }
        })

        binding.searchButton.setOnClickListener {
            val searchQuery = binding.etSearch.text.toString()
            viewModel.setSearch(
                searchQuery.ifEmpty { String() }
            )

        }

        binding.allPeople.setOnClickListener {
            viewModel.setExtraParams(
                ExtraSearchParams()
            )
        }

        binding.myRegion.setOnClickListener {
            Log.d("getProfiles", "user city ${userProfile?.city}")
            viewModel.setExtraParams(
                ExtraSearchParams(
                    region = userProfile?.city
                )
            )
        }

        binding.profilesRw.adapter = adapter.also {
            it.bindAction { data ->
                data?.let { profile ->
                    findNavController().navigate(
                        R.id.action_mainFragment_to_externalProfileDetailes,
                        Bundle().also { bundle ->
                            bundle.putParcelable(PROFILE_KEY, profile)
                            Log.d("AAA", "profile $profile")
                        })

                }
            }
        }

        binding.extraSearchButton.setOnClickListener {
            findNavController().navigate(
                R.id.action_mainFragment_to_externalSearchFragment,
            )
        }


        binding.addManBtn.setOnClickListener {
            goneCustomActionBar()
            findNavController().navigate(
                R.id.action_mainFragment_to_addUserFragment,
                Bundle().also { it.putString("cnc", phone) }
            )
            menuItem?.isVisible = false
        }

        binding.myContactsBtn.setOnClickListener {
            readContacts()
        }
    }


    private fun setListeners() {
        Log.d("getProfiles", " subscribe")
        lifecycleScope.launchWhenStarted {
            viewModel.profilesFlow.collectLatest { pageData ->
                Log.d("getProfiles", " subscribe profilesFlow collect $pageData")
                log("profilesFlow collect ${pageData}")
                changeNoResponseVisibility()
                pageData.let {
                    adapter.submitData(it)
                    adapter.refresh()
                }
            }
        }
        lifecycleScope.launch {
            adapter.loadStateFlow.collect {
                if (it.append is LoadState.NotLoading && it.append.endOfPaginationReached) {
                    if(adapter.itemCount < 1)
                        changeNoResponseVisibility()
                    else changeNoResponseVisibility()
                        //Toast.makeText(requireContext(), "Поиск не дал результатов", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun showCustomActionBar() {
        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar
        actionBar?.show()

        actionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        actionBar?.setCustomView(R.layout.main_fragment_title)

        val customImageView =
            actionBar?.customView?.findViewById<ImageView>(R.id.custom_actionbar_image)
        customImageView?.setImageResource(R.drawable.main_title)
        requireActivity().invalidateOptionsMenu()
    }

    private fun goneCustomActionBar() {
        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar
        actionBar?.displayOptions = ActionBar.DISPLAY_SHOW_TITLE
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }




    private fun readContacts() {

        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_CONTACTS
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            showCustomPermissionDialog()

        } else {
            goneCustomActionBar()
            findNavController().navigate(R.id.action_mainFragment_to_contactsFragment)
        }
    }

    private fun changeNoResponseVisibility() {
        adapter.refresh()
        Log.d("getProfiles", "changeNoResponseVisibility adapter.itemCount ${adapter.itemCount}")
        binding.noResponse.visibility = if (adapter.itemCount == 0) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun showCustomPermissionDialog() {
        val dialogBinding = FragmentContactsDialogBinding.inflate(layoutInflater)
        val dialogBuilder =
            AlertDialog.Builder(requireContext()).setView(dialogBinding.root)
        val alertDialog = dialogBuilder.create()

        dialogBinding.ok.setOnClickListener {

            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.READ_CONTACTS),
                REQUEST_READ_CONTACTS
            )
            alertDialog.dismiss()
        }

        dialogBinding.cancel.setOnClickListener {
            alertDialog.dismiss()
        }

        alertDialog.show()
    }
}

