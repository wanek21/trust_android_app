package ru.example.ui.fragments.voteDetails

import android.content.Context
import android.os.Binder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import ru.example.databinding.VotesListLayoutBinding
import ru.example.ui.fragments.ExternalProfileDetailes

class VotesDetailFragment : Fragment() {


    private val viewModel: VoteDetailsViewModel by inject()
    private val adapter: VotesAdapter by lazy { VotesAdapter() }
    private lateinit var id: String
    private lateinit var binding: VotesListLayoutBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        id = requireArguments().getString(ExternalProfileDetailes.P_ID_KEY) ?: throw IllegalStateException()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = VotesListLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribe()
        initViews()
    }

    private fun subscribe() {
        lifecycleScope.launch {
            viewModel.votesFlow.onEach {
                if (it != null) {
                    adapter.setNewList(it)
                } else {

                }
            }
        }
    }

        private fun initViews() {
            binding.votesRw.adapter = adapter
        }

    }