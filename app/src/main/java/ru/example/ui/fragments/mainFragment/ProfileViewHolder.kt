package ru.example.ui.fragments.mainFragment

import android.content.Context
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import ru.example.ApiService
import ru.example.databinding.ProfileItemLayoutBinding
import ru.example.domain.ProfileEntity
import ru.example.log

class ProfileViewHolder(
    private val binding: ProfileItemLayoutBinding,
    private val context: Context
) : RecyclerView.ViewHolder(binding.root) {

    fun onBind(value: ProfileEntity?) {
        value?.let { profile ->
            val token = context.getSharedPreferences("app_prefs", Context.MODE_PRIVATE).getString(
                ApiService.TOKEN_KEY, ""
            )
            val glideUrl = GlideUrl("https://185.178.46.6/api/user/avatar/${profile.userPic}") { mapOf(Pair("Authorization", token)) }
            binding.apply {
                approves.text = " ${profile.approves}"
                disapproves.text = " ${profile.disapproves}"
                profileName.text = profile.fullName
                Glide.with(binding.root).load(glideUrl).into(userPic)
                birthdate.text = " " + profile.birthDate
                location.text = " " + profile.city
                Log.d("Profiles", "${profile.phoneNumber}")
            }
        }

    }

}