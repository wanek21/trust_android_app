package ru.example.ui.fragments.addUserFragment

import com.google.gson.annotations.SerializedName
import ru.example.ui.fragments.userProfileFragment.ImageData

data class AddUserProfileForm(
    @SerializedName("surname")
    val surname: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("patronymic")
    val patronymic: String?,
    @SerializedName("birthdate")
    val birthDate: String,
    @SerializedName("country")
    val country: String,
    @SerializedName("region")
    val townOrVillage: String
)