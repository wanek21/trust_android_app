package ru.example.ui.fragments.addUserFragment

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.viewbinding.BuildConfig
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import ru.example.*
import ru.example.domain.PhoneNumber
import ru.example.ui.fragments.userProfileFragment.ImageData

class AddUserViewModel(
    private val service: ApiService
): ViewModel() {

    private val _answerFlow = MutableSharedFlow<AnswerState>()
    val answerFlow: SharedFlow<AnswerState> = _answerFlow
    var userAlreadyAdded: Boolean = false

    val error = MutableStateFlow<String?>(null)

    fun addUserProfile(
        phoneNumber: PhoneNumber,
        profile: AddUserProfileForm
    ) {
        Log.d("@@@", "addUserProfile heerrrr: $profile")
        viewModelScope.launch {
            try {
                val answer = service.addUserProfile(
                    phoneNumber,
                    profile
                )
                Log.d("@@@", "addUserProfile answer $answer")
                if (answer.code() == 200){
                    userAlreadyAdded = false
                    _answerFlow.emit(AnswerState.SUCCESS)
                }
                if (answer.code() == 201){
                    userAlreadyAdded = true
                    _answerFlow.emit(AnswerState.SUCCESS)
                }
                if(answer.code() == 409) {
                    error.update {
                        "Пользователь с таким номером уже существует"
                    }
                }
                else {
                    userAlreadyAdded = false
                     _answerFlow.emit(AnswerState.FAIL)
                }
            } catch (e: Exception){
                if(BuildConfig.DEBUG){
                    e.printStackTrace()
                }
                _answerFlow.emit(AnswerState.FAIL)
            }
        }
    }
}