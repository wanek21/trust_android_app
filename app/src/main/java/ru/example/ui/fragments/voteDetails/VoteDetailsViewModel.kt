package ru.example.ui.fragments.voteDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import ru.example.ApiService
import ru.example.domain.VoteDetailEntity

class VoteDetailsViewModel(
    private val apiService: ApiService
): ViewModel() {

    private val idFlow = MutableSharedFlow<String>()

    val votesFlow: MutableSharedFlow<List<VoteDetailEntity>?> = MutableSharedFlow()

    init {
        viewModelScope.launch {
            idFlow.onEach {
                getVotes(it)
            }.collect()
        }
    }

    private fun getVotes(id: String) {
        viewModelScope.launch {
            try {
                val result = apiService.getVoteDetailes(id)
                if(result.isSuccessful && result.body() != null){
                    votesFlow.emit(result.body()!!.votes)
                } else votesFlow.emit(null)
            } catch (e: Exception){
                votesFlow.emit(null)
                e.printStackTrace()
            }
        }
    }


    fun setNewId(id: String){
        viewModelScope.launch {
            idFlow.emit(id)
        }
    }


}