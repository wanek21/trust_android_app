package ru.example.ui.fragments.userProfileFragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.example.AnswerState
import ru.example.ApiService
import ru.example.BuildConfig
import ru.example.R
import ru.example.databinding.UserProfileLayoutBinding
import ru.example.domain.ProfileEntity
import ru.example.log
import ru.example.ui.fragments.CodeConfirmFragment
import java.io.ByteArrayOutputStream
import java.io.IOException

class UserProfileFragment : Fragment() {

    companion object {
        const val REQUEST_IMAGE_PICK = 1001
        const val REQUEST_READ_EXTERNAL_STORAGE = 1002
        const val USER_PROFILE = "user_prorile"
        const val code_num_key = "cnc"
    }

    private lateinit var binding: UserProfileLayoutBinding
    private val viewModel: UserProfileViewModel by viewModel()
    private var selectedImageUri: Uri? = null
    private var sImage: Uri? = null
    private var phone: String? = null
    private var currentUserImage: String? = null
    private lateinit var profile: ProfileEntity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        phone = requireArguments().getString(CodeConfirmFragment.code_num_key)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        goneCustomActionBar()

    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.findItem(R.id.menu).isVisible = false
    }

    override fun onResume() {
        super.onResume()
        goneCustomActionBar()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = UserProfileLayoutBinding.inflate(layoutInflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //  currentUserImage = profile.userPic
        initViews()
        subscribe()
        //   setImageBase64(profile.userPic)

        //Glide.with(view).load(profile.userPic).into(binding.userPic)

        phone?.let { viewModel.setUserNumber(it) }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_PICK && resultCode == Activity.RESULT_OK) {
            val selectedImageUri: Uri? = data?.data

            if (selectedImageUri != null) {
                setImageUri(selectedImageUri)
            }
            try {
                sImage = selectedImageUri
                binding.textResult.visibility = View.GONE
                binding.saveButton.visibility = View.VISIBLE
                binding.saveButton.isEnabled = true
                /*selectedImageUri?.let {
                    val bitmap: Bitmap =
                        MediaStore.Images.Media.getBitmap(
                            requireActivity().contentResolver,
                            selectedImageUri
                        )
                    val stream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                    val bytes: ByteArray = stream.toByteArray()
                    sImage = Base64.encodeToString(bytes, Base64.DEFAULT)

                    if (currentUserImage != sImage) {
                        binding.textResult.visibility = View.GONE
                        binding.saveButton.visibility = View.VISIBLE
                        binding.saveButton.isEnabled = true
                    }
                }*/
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun setImageUri(uri: Uri) {
        binding.userPic.setImageURI(uri)
        selectedImageUri = uri
    }

    private fun setImageBase64(base64Image: String) {
        val bytes = Base64.decode(base64Image, Base64.DEFAULT)
        val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
        binding.userPic.setImageBitmap(bitmap)
    }


    private fun goneCustomActionBar() {
        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar
        actionBar?.displayOptions = ActionBar.DISPLAY_SHOW_TITLE
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requireActivity().invalidateOptionsMenu()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, REQUEST_IMAGE_PICK)
            } else {
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun getProfile() {
        lifecycleScope.launchWhenStarted {
            viewModel.numberList.collectLatest { userProfile ->
                userProfile?.let {
                    Log.d("AAA", "user profile $userProfile")
                    if(userProfile.isNotEmpty()) {
                        profile = userProfile[0]
                        binding.trust.text = getString(R.string.trust, profile.approves)
                        binding.unTrust.text = getString(R.string.trust, profile.disapproves)
                        binding.userName.text = profile.fullName
                        binding.region.text = "${profile.country}, ${profile.city}"
                        binding.bDate.text = profile.birthDate
                        currentUserImage = profile.userPic
                        //setImageBase64(profile.userPic)
                        val token = requireContext().getSharedPreferences("app_prefs", Context.MODE_PRIVATE).getString(
                            ApiService.TOKEN_KEY, ""
                        )
                        val glideUrl = GlideUrl(profile.userPic) { mapOf(Pair("Authorization", token)) }
                        Glide.with(requireContext()).load(glideUrl).into(binding.userPic)
                    }
                }
            }
        }
    }


    private fun initViews() {

        getProfile()

        binding.userPic.setOnClickListener {
            val permission = Manifest.permission.READ_EXTERNAL_STORAGE

            if (ContextCompat.checkSelfPermission(requireContext(), permission)
                != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(permission),
                    REQUEST_READ_EXTERNAL_STORAGE
                )
            } else {
                val intent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, REQUEST_IMAGE_PICK)
            }
        }

        binding.saveButton.setOnClickListener {
            sImage?.let {
                viewModel.addAvatar(it)
                Log.d("AAA", "image ${it}")
            }
        }

        binding.confirmButton.setOnClickListener {
            Toast.makeText(requireContext(), "В разработке", Toast.LENGTH_SHORT)
                .show()
        }

        binding.ratingTitle.setOnClickListener {
            findNavController().navigate(
                R.id.action_userProfileFragment_to_evaluatorsUsersFragment,
                Bundle().also { bundle ->
                    bundle.putParcelable(USER_PROFILE, profile)
                })
        }
        binding.shareButton.setOnClickListener {
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.setType("text/plain")
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Trust not trust")
                var shareMessage = "\nПосмотри как за меня проголосовали\n\n"
                shareMessage =
                    shareMessage + "https://play.google.com/store/apps/details?id=" +
                            BuildConfig.APPLICATION_ID + "\n\n"
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "choose one"))
            } catch (e: Exception) {
                e.toString()
            }
        }
    }


    private fun subscribe() {
        lifecycleScope.launchWhenStarted {
            viewModel.answerFlow.collectLatest {
                if (it == AnswerState.SUCCESS) {
                    //currentUserImage = sImage
                    binding.saveButton.visibility = View.GONE
                    binding.textResult.visibility = View.VISIBLE
                } else {
                    Toast.makeText(requireContext(), "server error", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

}