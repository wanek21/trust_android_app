package ru.example.ui.fragments.voteDetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ru.example.databinding.VoteDetailLayoutBinding
import ru.example.domain.VoteDetailEntity

class VotesAdapter: RecyclerView.Adapter<VoteViewHolder>() {

    private var votesList: List<VoteDetailEntity> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VoteViewHolder {
        return VoteViewHolder(
            VoteDetailLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false)
        )
    }

    override fun getItemCount(): Int = votesList.size

    override fun onBindViewHolder(holder: VoteViewHolder, position: Int) {
        TODO("Not yet implemented")
    }

    private fun getItem(position: Int) = votesList[position]

    fun setNewList(newList: List<VoteDetailEntity>){
        val result = DiffUtil.calculateDiff(VoteDetailDiffUtil(votesList, newList))
        votesList = newList
        result.dispatchUpdatesTo(this)
    }
}