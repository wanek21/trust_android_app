package ru.example.ui.fragments.userProfileFragment

import android.net.Uri
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import ru.example.AnswerState
import ru.example.ApiService
import ru.example.BuildConfig
import ru.example.domain.ProfileEntity

class UserProfileViewModel(
    private val service: ApiService
) : ViewModel() {

    private val _answerFlow = MutableSharedFlow<AnswerState>()
    val answerFlow: SharedFlow<AnswerState> = _answerFlow

    private val _numbersList = MutableSharedFlow<List<ProfileEntity>?>()
    val numberList: SharedFlow<List<ProfileEntity>?> = _numbersList


    fun setUserNumber(phone: String) {
        viewModelScope.launch {
            try {
                _numbersList.emit(
                    service.getUserProfileByPhone(
                        phone
                    ).body()
                )
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
                _answerFlow.emit(AnswerState.FAIL)
            }
        }
    }

    fun addAvatar(avatarUri: Uri) {
        viewModelScope.launch {
            try {
                val answer = service.addAvatar(avatarUri)
                Log.d("@@@", "addAvatar answer $answer")
                Log.d("@@@", "addAvatar STATE ${answer.isSuccessful}")

                if (answer.code() == 200) {
                    _answerFlow.emit(AnswerState.SUCCESS)
                } else {
                    _answerFlow.emit(AnswerState.FAIL)
                }
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
                _answerFlow.emit(AnswerState.FAIL)
            }

        }
    }
}

