package ru.example.ui.fragments.registrationFragment

import com.google.gson.annotations.SerializedName

data class RegistrationProfileForm(
    //@SerializedName("phone_number")
    //val number: String,
    @SerializedName("surname")
    val surname: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("patronymic")
    val patronymic: String?,
    @SerializedName("birthdate")
    val birthDate: String,
    @SerializedName("country")
    val country: String,
    @SerializedName("region")
    val townOrVillage: String
)