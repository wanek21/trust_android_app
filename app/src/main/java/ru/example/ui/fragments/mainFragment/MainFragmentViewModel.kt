package ru.example.ui.fragments.mainFragment

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import androidx.paging.filter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import ru.example.Api
import ru.example.ApiService
import ru.example.BuildConfig
import ru.example.data.UsersRepository
import ru.example.domain.ComplaintUserResponse
import ru.example.domain.ExtraSearchParams
import ru.example.domain.ProfileEntity
import ru.example.log
import ru.example.preferences.Preferences
import ru.example.ui.fragments.admin.UserToBlockItem

class MainFragmentViewModel(
    private val apiService: ApiService,
    private val preferences: Preferences,
    private val api: Api,
    private val usersRepository: UsersRepository
) : ViewModel() {


    private val regionNameFlow = MutableStateFlow(preferences.region ?: "")
    private val _searchFlow = MutableStateFlow("")
    val searchFlow = _searchFlow

    private val extraSearchParamsFlow = MutableStateFlow<ExtraSearchParams?>(null)

    private val _numbersList = MutableSharedFlow<List<ProfileEntity>?>()
    val numberList: SharedFlow<List<ProfileEntity>?> = _numbersList

    private val _complaintsList = MutableStateFlow<List<UserToBlockItem>>(emptyList())
    val complaintsList: StateFlow<List<UserToBlockItem>> = _complaintsList


    @OptIn(ExperimentalCoroutinesApi::class)
    private val _profilesFlow =
        _searchFlow.flatMapLatest { query ->
            regionNameFlow.flatMapLatest { region ->
                extraSearchParamsFlow.flatMapLatest { extraParams ->
                    log("_profilesFlow: $query $region $extraParams")
                    apiService.getProfiles(
                        query,
                        region,
                        extraParams
                    )
                }
            }
        }.cachedIn(viewModelScope)


    val profilesFlow: StateFlow<PagingData<ProfileEntity>> = _profilesFlow
        .combine(searchFlow) { profiles, query ->
            log("combine profilesFlow profiles $profiles query $query")
            profiles.filter { profile ->
                profile.fullName?.contains(query, ignoreCase = true) ?: false
            }
        }.stateIn(viewModelScope, SharingStarted.Eagerly, PagingData.empty())

    init {
        log("init main fragment view model")
    }

    fun updateComplaintsList() {
        usersRepository
            .getUsersForBlock()
            .onEach { complaintsList ->
                _complaintsList.update { complaintsList }
            }.launchIn(viewModelScope)
    }

    fun getUserById(id: Int): Flow<Result<ProfileEntity>> {
        log("[getUserById]")
        return usersRepository.getUserById(id)
    }

    fun checkBlockedUsers(usersList: List<Int>): Flow<String?> {
        return usersRepository.checkBlockedUsers(usersList)
    }

    fun setExtraParams(extraSearchParams: ExtraSearchParams) {
        log("updating extraParams $extraSearchParams")
        extraSearchParamsFlow.update { extraSearchParams }
    }

    fun clearExtraParams() {
        log("clearing extraParams")
        extraSearchParamsFlow.update { null }
    }

    fun updateProfileList() {
        viewModelScope.launch {
            log("updateProfileList")
            _searchFlow.update {
                it.plus("4")
            }
        }
    }

    fun setSearch(query: String) {
        viewModelScope.launch {
            Log.d("getProfiles", "query $query")
            _searchFlow.update { query }
        }
    }

    fun setThisUserNumber(phone: String) {
        viewModelScope.launch {
            try {
                _numbersList.emit(
                    apiService.getUserProfileByPhone(
                        phone
                    ).body()
                )
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun setNewRegion(regionName: String) {
        viewModelScope.launch {
            Log.d("@@@", "new region $regionName")
            regionNameFlow.emit(regionName)
        }
    }

    fun loaOnStart() {
        viewModelScope.launch {
            regionNameFlow.emit(preferences.region ?: "")
        }
    }


}
