package ru.example.ui.fragments.registrationFragment

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.viewbinding.BuildConfig
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import ru.example.*

class RegistrationViewModel(
    private val service: ApiService
): ViewModel() {

    private val _answerFlow = MutableSharedFlow<AnswerState>()
    val answerFlow: SharedFlow<AnswerState> = _answerFlow

    fun sendProfile(profile: RegistrationProfileForm) {
        Log.d("@@@", "send profile $profile")
        viewModelScope.launch {
            try {
                val answer = service.createProfile(profile)
                Log.d("@@@", "sendProfile answer $answer")
                if(answer.code() == 200){
                    _answerFlow.emit(AnswerState.SUCCESS)
                } else {
                     _answerFlow.emit(AnswerState.FAIL)
                }
            } catch (e: Exception){
                if(BuildConfig.DEBUG){
                    e.printStackTrace()
                }
                _answerFlow.emit(AnswerState.FAIL)
            }
        }
    }
}