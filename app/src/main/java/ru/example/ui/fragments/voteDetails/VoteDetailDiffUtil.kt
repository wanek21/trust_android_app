package ru.example.ui.fragments.voteDetails

import androidx.recyclerview.widget.DiffUtil
import ru.example.domain.VoteDetailEntity

class VoteDetailDiffUtil(
   private val oldList: List<VoteDetailEntity>,
   private val newList: List<VoteDetailEntity>
): DiffUtil.Callback()  {

    override fun getOldListSize(): Int  = oldList.size

    override fun getNewListSize(): Int  = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }

}