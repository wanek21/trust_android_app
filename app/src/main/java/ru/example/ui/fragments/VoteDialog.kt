package ru.example.ui.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.button.MaterialButton
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import ru.example.AnswerState
import ru.example.R
import ru.example.databinding.VoteDialogBinding
import ru.example.domain.VoteMode
import ru.example.domain.Votes
import ru.example.log

class VoteDialog : DialogFragment() {

    private companion object {
        const val MAX_SCORE = 3
        const val MIDDLE_SCORE = 2
        const val MIN_SCORE = 1
    }

    private lateinit var mode: VoteMode
    private lateinit var id: String
    private lateinit var binding: VoteDialogBinding
    private var curVote: Votes? = null
    private val viewModel: ExternalProfileDetailesViewModel by sharedViewModel()
    private var feedback: Boolean? = null
    private var score: Int? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        requireArguments().let {
            mode = VoteMode.values()[it.getInt(ExternalProfileDetailes.V_TYPE)]
            id = it.getString(ExternalProfileDetailes.P_ID_KEY) ?: throw IllegalArgumentException()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = VoteDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        subscribe()
    }


    private fun initViews() {
        binding.ok.setOnClickListener {
            Log.d("voteMode", "Click OK id $id")
            feedback?.let { feedback ->
                score?.let { score ->
                    viewModel.rateUser(id.toInt(), feedback, score)
                    viewModel.changeVoteState(if (feedback) VoteMode.UP else VoteMode.DOWN)
                    /*if (feedback) {
                        log("dialog: execute upLikes()")
                        viewModel.upLikes()
                    } else viewModel.upDislikes()*/
                    Log.d("voteMode", "Click OK feedback $feedback score $score")
                }
            }





            /*            curVote?.let {
                            if (mode == VoteMode.UP) {
                                viewModel.voteUp(id, it)
                            } else {
                                viewModel.voteDown(id, it)
                            }
                        }*/
            dismiss()
        }



        binding.cancel.setOnClickListener {
            dialog?.cancel()
            feedback = null
            score = null
        }
        when (mode) {
            VoteMode.IDLE -> throw IllegalArgumentException()
            VoteMode.UP -> {
                prepareUpVote()
            }

            VoteMode.DOWN -> {
                prepareDownVote()
            }
        }
    }

    private fun nonSelected(button: MaterialButton) {
        button.backgroundTintList =
            ColorStateList.valueOf(getColor(requireContext(), R.color.grey1))
        button.setTextColor(
            ColorStateList.valueOf(
                getColor(
                    requireContext(),
                    R.color.white
                )
            )
        )
        button.strokeColor =
            ColorStateList.valueOf(getColor(requireContext(), R.color.white))
    }

    private fun selectedVoiteUp(button: MaterialButton) {
        button.backgroundTintList =
            ColorStateList.valueOf(getColor(requireContext(), R.color.green))
        button.setTextColor(
            ColorStateList.valueOf(
                getColor(
                    requireContext(),
                    R.color.black
                )
            )
        )
        button.strokeColor =
            ColorStateList.valueOf(getColor(requireContext(), R.color.green))
    }

    private fun selectedVoiteDown(button: MaterialButton) {
        button.backgroundTintList =
            ColorStateList.valueOf(getColor(requireContext(), R.color.cherry_red))
        button.setTextColor(
            ColorStateList.valueOf(
                getColor(
                    requireContext(),
                    R.color.white
                )
            )
        )
        button.strokeColor =
            ColorStateList.valueOf(getColor(requireContext(), R.color.cherry_red))
    }

    private fun prepareDownVote() {
        feedback = false
        binding.title.text = resources.getString(R.string.can_not_trust)
        binding.title.setTextColor(
            ColorStateList.valueOf(
                getColor(
                    requireContext(),
                    R.color.cherry_red
                )
            )
        )

        val fVar = binding.fVar
        val sVar = binding.sVar
        val tVar = binding.tVar

        binding.fVar.text = getString(R.string.u_trust_for_sure)
        binding.sVar.text = getString(R.string.u_looks_untrustful)
        binding.tVar.text = getString(R.string.u_trust_unknow)
        binding.fVar.setOnClickListener {
            selectedVoiteDown(fVar)
            nonSelected(sVar)
            nonSelected(tVar)
            score = MAX_SCORE
        }
        binding.sVar.setOnClickListener {
            selectedVoiteDown(sVar)
            nonSelected(fVar)
            nonSelected(tVar)
            score = MIDDLE_SCORE
        }
        binding.tVar.setOnClickListener {
            selectedVoiteDown(tVar)
            nonSelected(sVar)
            nonSelected(fVar)
            score = MIN_SCORE
        }
    }

    private fun prepareUpVote() {
        feedback = true
        binding.title.text = resources.getString(R.string.can_trust)
        binding.title.setTextColor(
            ColorStateList.valueOf(
                getColor(
                    requireContext(),
                    R.color.green
                )
            )
        )

        val fVar = binding.fVar
        val sVar = binding.sVar
        val tVar = binding.tVar

        binding.fVar.text = getString(R.string.trust_for_sure)
        binding.sVar.text = getString(R.string.looks_trustful)
        binding.tVar.text = getString(R.string.trust_unknow)
        binding.fVar.setOnClickListener {
            selectedVoiteUp(fVar)
            nonSelected(sVar)
            nonSelected(tVar)
            score = MAX_SCORE
        }
        binding.sVar.setOnClickListener {
            selectedVoiteUp(sVar)
            nonSelected(fVar)
            nonSelected(tVar)
            score = MIDDLE_SCORE
        }
        binding.tVar.setOnClickListener {
            selectedVoiteUp(tVar)
            nonSelected(sVar)
            nonSelected(fVar)
            score = MIN_SCORE
        }

    }

    @SuppressLint("ShowToast")
    private fun subscribe() {
        lifecycleScope.launchWhenStarted {
            viewModel.answerFlow.collectLatest {
                if (it == AnswerState.SUCCESS) {
                    Log.d("@@@", "rateuser subscribe SUCCESS")
                } else {
                    Toast.makeText(requireContext(), "server error", Toast.LENGTH_SHORT)
                        .show()
                }
            }


        }
    }


}