package ru.example.ui.fragments.contactsFragment

import android.content.Context
import android.util.Log
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import ru.example.ApiService
import ru.example.data.Contact
import ru.example.databinding.ContactItemLayoutBinding
import ru.example.domain.ProfileEntity

class ContactsViewHolder(
    private val contactsList: List<Contact>,
    private val binding: ContactItemLayoutBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun onBind(value: ProfileEntity?) {
        Log.d("AAA", "setContactName ${contactsList}")

        value?.let { profile ->
            binding.apply {
                approves.text = " ${profile.approves}"
                disapproves.text = " ${profile.disapproves}"
                profileName.text = profile.fullName
                val token = binding.root.context.getSharedPreferences("app_prefs", Context.MODE_PRIVATE).getString(
                    ApiService.TOKEN_KEY, ""
                )
                val glideUrl = GlideUrl(profile.userPic) { mapOf(Pair("Authorization", token)) }
                Glide.with(binding.root).load(glideUrl).into(userPic)
                contectPhone.text = " " + profile.phoneNumber
                location.text = profile.country
                Log.d("Profiles", "${profile.phoneNumber}")
                setContactName(profile.phoneNumber)
            }
        }

    }

    private fun setContactName(contactPhone: String) {
        if (contactsList.find { contactPhone == it.number } != null) {
            binding.contatcName.text = contactsList.first { contactPhone == it.number }.name
        }

    }

}