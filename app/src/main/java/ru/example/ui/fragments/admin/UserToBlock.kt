package ru.example.ui.fragments.admin

class UserToBlock : ArrayList<UserToBlockItem>()

data class UserToBlockItem(
    val complaining_user_id: Int,
    val date: String,
    val id: Int,
    val is_reviewed: Boolean,
    val reason: String,
    val user_id: Int
)

fun createMockUserToBlockItems(): List<UserToBlockItem> {
    return listOf(
        UserToBlockItem(
            complaining_user_id = 123,
            date = "2024-03-30",
            id = 1,
            is_reviewed = false,
            reason = "Reason 1",
            user_id = 456
        ),
        UserToBlockItem(
            complaining_user_id = 456,
            date = "2024-03-31",
            id = 2,
            is_reviewed = true,
            reason = "Reason 2",
            user_id = 789
        ),
        UserToBlockItem(
            complaining_user_id = 789,
            date = "2024-04-01",
            id = 3,
            is_reviewed = false,
            reason = "Reason 3",
            user_id = 101112
        ),
        UserToBlockItem(
            complaining_user_id = 101112,
            date = "2024-04-02",
            id = 4,
            is_reviewed = true,
            reason = "Reason 4",
            user_id = 131415
        ),
        UserToBlockItem(
            complaining_user_id = 131415,
            date = "2024-04-03",
            id = 5,
            is_reviewed = false,
            reason = "Reason 5",
            user_id = 161718
        ),
        UserToBlockItem(
            complaining_user_id = 161718,
            date = "2024-04-04",
            id = 6,
            is_reviewed = true,
            reason = "Reason 6",
            user_id = 192021
        ),
        UserToBlockItem(
            complaining_user_id = 192021,
            date = "2024-04-05",
            id = 7,
            is_reviewed = false,
            reason = "Reason 7",
            user_id = 222324
        )
    )
}