package ru.example.ui.fragments.evaluatorsUsersFragment

import android.graphics.Color
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import ru.example.R
import ru.example.databinding.EvaluatorsUsersItemBinding
import ru.example.domain.EvaluatorsProfileEntity
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class EvaluatorsUsersViewHolder(
    private val binding: EvaluatorsUsersItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    //TODO: получить строки из ресурсов
    private companion object{
        val TRUST = 1

        val TRUST_3 = "Данному человеку точно можно верить"
        val TRUST_2 = "Данный человек производит впечатление, что ему можно верить"
        val TRUST_1 = "Доверяю, но дел с ним никаких не имел(a)"

        val NOT_TRUST_3 = "Данному человеку точно нельзя верить"
        val NOT_RUST_2 = "Данный человек производит впечатление, что ему нельзя верить"
        val NOT_TRUST_1 = "Не доверяю, но дел с ним никаких не имел(a)"
    }

    fun onBind(value: EvaluatorsProfileEntity?) {
        value?.let { profile ->
            binding.apply {
                approves.text = " ${profile.approves}"
                disapproves.text = " ${profile.disapproves}"
                feedback.text = if(profile.feedback) "Можно" else "Нельзя"
                feedback.setTextColor(
                    if(profile.feedback)
                    Color.GREEN
                else
                    Color.RED
                )
                val dateTime = LocalDateTime.parse(profile.date)

                // Format the LocalDateTime to YYYY-MM-DD
                val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
                val formattedDate = dateTime.format(formatter)
                voteDate.text = formattedDate
                setScore(profile.feedback, profile.score)
                Log.d("EvaluatorsUsersViewHolder", "${profile}")
            }
        }
        Log.d("AAA", "EvaluatorsUsersViewHolder onBind profile $value")
    }

    private fun setScore(feedback: Boolean, score: Int){
            binding.score.text = when (score) {
                1 -> if(feedback) TRUST_1 else NOT_TRUST_1
                2 -> if(feedback) TRUST_2 else NOT_RUST_2
                3 -> if(feedback) TRUST_3 else NOT_TRUST_3

                else -> ""
        }
    }
}