package ru.example.ui.fragments.evaluatorsUsersFragment

import android.content.ContentResolver
import android.provider.ContactsContract
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import ru.example.ApiService
import ru.example.data.Contact
import ru.example.domain.EvaluatorsProfileEntity
import ru.example.domain.ProfileEntity

class EvaluatorsUsersViewmodel(
    private val apiService: ApiService,
) : ViewModel() {

    private val userIdFlow = MutableStateFlow(0)

    private val _evaluatorsUsersFlow:  Flow<PagingData<EvaluatorsProfileEntity>> =
        userIdFlow.flatMapLatest { userId ->
            apiService.getEvaluatorsUsers(
                userId
            )
        }



    val evaluatorsUsersFlow: StateFlow<PagingData<EvaluatorsProfileEntity>?> = _evaluatorsUsersFlow
        .stateIn(viewModelScope, SharingStarted.Eagerly, null)

    fun setUserId(userId: Int){
        viewModelScope.launch {
            userIdFlow.emit(userId)
        }
    }

}
