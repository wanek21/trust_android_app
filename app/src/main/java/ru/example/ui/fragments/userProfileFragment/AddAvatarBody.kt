package ru.example.ui.fragments.userProfileFragment

import com.google.gson.annotations.SerializedName

data class AddAvatarBody (
    @SerializedName("image_to_bytes")
    val image_to_bytes: String,
)