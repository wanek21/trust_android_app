package ru.example.ui.fragments.registrationFragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.InputType.TYPE_NULL
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.example.AnswerState
import ru.example.R
import ru.example.data.armeniaRegionsList
import ru.example.data.belarusRegionsList
import ru.example.data.kazakhstanRegionsList
import ru.example.data.kyrgyzstanRegionsList
import ru.example.data.russiaRegionList
import ru.example.data.tadjikistanRegionList
import ru.example.data.uzbekistanRegionsList
import ru.example.databinding.DialogCountryBinding
import ru.example.databinding.RegistrationLayoutBinding
import ru.example.ui.fragments.AuthFragment
import ru.example.ui.util.RegionDialog


class RegistrationFragment : Fragment() {

    private var _binding: RegistrationLayoutBinding? = null
    private val binding get() = checkNotNull(_binding)
    private lateinit var phone: String
    private val viewModel: RegistrationViewModel by viewModel()

    companion object {
        const val code_num_key = "cnc"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        phone = requireArguments().getString(AuthFragment.code_num_key)
            ?: throw IllegalArgumentException("Phone is not provided")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        goneCustomActionBar()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = RegistrationLayoutBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.findItem(R.id.menu).isVisible = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        subscribe()
    }

    override fun onResume() {
        super.onResume()
        goneCustomActionBar()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun goneCustomActionBar() {
        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar
        actionBar?.displayOptions = ActionBar.DISPLAY_SHOW_TITLE
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requireActivity().invalidateOptionsMenu()
    }

    private fun getRegionsList(country: String): List<String> {
        return when (country) {
            "Беларусь" -> belarusRegionsList
            "Армения" -> armeniaRegionsList
            "Казахстан" -> kazakhstanRegionsList
            "Узбекистан" -> uzbekistanRegionsList
            "Таджикистан" -> tadjikistanRegionList
            "Кыргызстан" -> kyrgyzstanRegionsList
            else -> russiaRegionList
        }
    }

    private fun setRegionDialog() {
        val selectedCountry = binding.countryEt.text.toString()
        val regionDialog =
            RegionDialog(
                requireContext(),
                getRegionsList(selectedCountry)
            ) { selectedRegion ->
                Log.d("AAA", "AddUserFragment selected region: $selectedRegion")
                binding.regionEt.setText(selectedRegion)
            }
        regionDialog.show()
    }

    private fun setCountryDialog(context: Context) {
        val bindingDialogCountryBinding = DialogCountryBinding.inflate(LayoutInflater.from(context))
        val view = bindingDialogCountryBinding.root

        val regionList = listOf(
            bindingDialogCountryBinding.russia,
            bindingDialogCountryBinding.belarus,
            bindingDialogCountryBinding.armenia,
            bindingDialogCountryBinding.kazakhstan,
            bindingDialogCountryBinding.kyrgyzstan,
            bindingDialogCountryBinding.tadjikistan,
            bindingDialogCountryBinding.uzbekistan
        )
        val dialog = Dialog(context)
        dialog.setContentView(view)

        val positiveButton = bindingDialogCountryBinding.positiveButton
        val negativeButton = bindingDialogCountryBinding.negativeButton
        var selectedCountry = ""

        for ((index, selectedItem) in regionList.withIndex()) {
            selectedItem.setOnClickListener {
                selectedItem.setTextColor(Color.GREEN)
                selectedCountry = selectedItem.text.toString()

                for ((i, item) in regionList.withIndex()) {
                    if (index != i) {
                        item.setTextColor(Color.WHITE)
                    }
                }
            }
        }

        positiveButton.setOnClickListener {
            binding.regionEt.text.clear()
            binding.countryEt.setText(selectedCountry)
            dialog.dismiss()
        }

        negativeButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun initViews() {

        val datePickerDialogHelper = DatePickerDialogHelper(requireContext(), binding.birthDateEt)
        binding.countryEt.setText("Россия")

        binding.regionEt.inputType = TYPE_NULL
        binding.birthDateEt.inputType = TYPE_NULL
        binding.countryEt.inputType = TYPE_NULL

        binding.nameEt.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                datePickerDialogHelper.showDatePickerDialog()
                true
            } else {
                false
            }
        }

        binding.iconRegion.setOnClickListener {
            setRegionDialog()

        }
        binding.regionEt.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                setRegionDialog()
            }
            false
        }

        binding.birthDateEt.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                datePickerDialogHelper.showDatePickerDialog()
            }
            false
        }

        binding.countryEt.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                setCountryDialog(requireContext())
            }
            false
        }

        binding.iconCalendar.setOnClickListener {
            datePickerDialogHelper.showDatePickerDialog()
        }

        binding.iconCounty.setOnClickListener {
            setCountryDialog(requireContext())
        }


        val textWatchers = listOf(
            //binding.nameEt,
            binding.birthDateEt,
            binding.countryEt,
            binding.regionEt
        )

        var isFormatting = false

        textWatchers.forEach { editText ->
            editText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    if (isFormatting) return

                    val isValid = validateField(editText.id, s.toString())
                    editText.setCustomBackgroundIfEmpty(isValid = isValid)
                    binding.createProfile.isEnabled = isValid && checkFields()

                    if (editText != binding.birthDateEt) {
                        isFormatting = true
                        val input = s.toString()
                        val words = input.split(" ")
                        val capitalizedWords = words.map { it.capitalize() }
                        val formattedName = capitalizedWords.joinToString(" ")
                        editText.setText(formattedName)
                        editText.setSelection(formattedName.length)
                        isFormatting = false
                    }

                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })
        }


        val dateTextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable?) {
                if (!s.isNullOrEmpty() && s.length % 3 == 0 && s[s.length - 1] != '.') {
                    binding.birthDateEt.setText(
                        s.toString().substring(0, s.length - 1) + "." + s[s.length - 1]
                    )
                    binding.birthDateEt.setSelection(binding.birthDateEt.text.length)
                }
            }
        }

        binding.birthDateEt.addTextChangedListener(dateTextWatcher)




        binding.createProfile.setOnClickListener {
            if (checkFields()) {
                try {
                    val split = splitField(binding.nameEt.text) ?: throw IllegalStateException()
                    viewModel.sendProfile(
                        RegistrationProfileForm(
                            //phone.trim(),
                            split.first,
                            split.second,
                            split.third,
                            binding.birthDateEt.text.toString().trim(),
                            binding.countryEt.text.toString().trim(),
                            "${binding.regionEt.text.trim()} "
                        )
                    )
                } catch (e: Exception) {
                    Snackbar.make(requireView(), "ERR", Snackbar.LENGTH_SHORT).show()
                }
            } else {
                with(binding) {
                    if (birthDateEt.text.isNullOrEmpty()) {
                        birthDateLayout.setBackgroundResource(R.drawable.rounded_edittext_error)
                    }
                    if (countryEt.text.isNullOrEmpty()) {
                        countryLayout.setBackgroundResource(R.drawable.rounded_edittext_error)
                    }

                    regionEt.setCustomBackgroundIfEmpty(R.drawable.rounded_edittext_error)
                    nameEt.setCustomBackgroundIfEmpty(R.drawable.rounded_edittext_error)

                    createProfile.isEnabled = false
                    disclaimerTv.text = getString(R.string.please_fill_in_the_required_fields)
                    disclaimerTv.setTextColor(resources.getColor(R.color.cherry_red))
                }
            }
        }
    }

    private fun validateField(fieldId: Int, text: String): Boolean {
        return when (fieldId) {
            R.id.name_et -> {
                val words = text.split("\\s+".toRegex())
                words.all { it.matches(Regex("^[a-zA-Zа-яА-Я]+$")) }
                        && !text.contains("Путин", ignoreCase = true)
                        && !text.contains("Putin", ignoreCase = true)
            }

            R.id.birth_date_et -> text.matches(Regex("^\\d{2}\\.\\d{2}\\.\\d{2}$"))
            R.id.country_et -> text.matches(Regex("^[a-zA-Zа-яА-Я\\s\\-]+$"))
            R.id.region_et -> text.isNotEmpty()
            else -> true
        }
    }

    private fun checkFields(): Boolean {
        return binding.run {
            val nameValid = validateField(R.id.name_et, nameEt.text.toString().trim())
            val birthDateValid =
                validateField(R.id.birth_date_et, birthDateEt.text.toString().trim())
            val countryValid = validateField(R.id.country_et, countryEt.text.toString().trim())
            val regionValid = validateField(R.id.region_et, regionEt.text.toString().trim())

            nameValid && birthDateValid && countryValid && regionValid
        }
    }

    private fun EditText.setCustomBackgroundIfEmpty(
        drawableResource: Int = R.drawable.rounded_edittext,
        isValid: Boolean = false
    ) {
        if (text.isNullOrEmpty() || !isValid) {
            setBackgroundResource(drawableResource)
        } else {
            setBackgroundResource(R.drawable.rounded_edittext)
        }
    }


    private fun splitField(text: Editable?): Triple<String, String, String?>? {
        return text?.run {
            val inputSplit = text.toString().split(Regex("\\s+"))
            if (inputSplit.size >= 3) {
                Triple(inputSplit.first(), inputSplit[1], inputSplit.last())
            } else if (inputSplit.size == 2) {
                Triple(inputSplit.first(), inputSplit[1], null)
            } else {
                null
            }
        }
    }


    private fun subscribe() {
        lifecycleScope.launchWhenStarted {
            viewModel.answerFlow.collectLatest {

                val navController = findNavController()

                if (it == AnswerState.SUCCESS) {
                    Toast.makeText(
                        requireContext(),
                        "Успешная регистрация",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    navController.navigate(
                        R.id.action_registrationFragment_to_mainFragment,
                        Bundle().also {
                            it.putString(code_num_key, phone)
                        })
                } else {
                    Toast.makeText(requireContext(), "Ошибка при регистрации", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }


}