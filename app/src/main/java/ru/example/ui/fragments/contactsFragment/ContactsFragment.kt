package ru.example.ui.fragments.contactsFragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.flow.collectLatest
import ru.example.R
import ru.example.databinding.FragmentContactsBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.example.data.Contact
import ru.example.ui.fragments.mainFragment.MainFragment


class ContactsFragment : Fragment() {

    private lateinit var binding: FragmentContactsBinding
    private val viewModel: ContactsViewmodel by viewModel()
    private var contactList: List<Contact> = emptyList()
    private lateinit var adapter: ContactsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel.readContacts(requireActivity().contentResolver)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentContactsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.findItem(R.id.menu).isVisible = false
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribe()
    }

    private fun bindItemClick() {
        binding.contacysRw.adapter = adapter.also {
            it.bindAction { data ->
                data?.let { profile ->
                    findNavController().navigate(
                        R.id.action_contactsFragment_to_externalProfileDetailes,
                        Bundle().also { bundle ->
                            bundle.putParcelable(MainFragment.PROFILE_KEY, profile)
                        })
                }
            }
        }
    }


    private fun subscribe() {
        lifecycleScope.launchWhenStarted {
            viewModel.userContactsFlow.collectLatest { pageData ->
                pageData?.let {
                    viewModel.contacts.collect { contacts ->
                        if(contacts.isEmpty()) {
                            Toast.makeText(requireContext(), "Среди ваших контактов нет зарегистрированных пользователей", Toast.LENGTH_LONG).show()
                        }
                        adapter = ContactsAdapter(contacts)
                        bindItemClick()

                        adapter.submitData(pageData)
                        adapter.refresh()
                    }
                }
            }
        }
    }
}

