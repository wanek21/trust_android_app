package ru.example.ui.fragments

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import ru.example.BuildConfig
import ru.example.R
import ru.example.databinding.ExternalProfileLayoutBinding
import ru.example.domain.ProfileEntity
import ru.example.domain.VoteMode
import ru.example.log
import ru.example.ui.fragments.mainFragment.MainFragment
import ru.example.ui.fragments.mainFragment.MainFragmentViewModel


class ExternalProfileDetailes : Fragment() {

    companion object {
        const val P_ID_KEY = "pid"
        const val V_TYPE = "voteType"
        const val USER_PROFILE = "user_prorile"
        const val code_num_key = "cnc"
    }

    private lateinit var profile: ProfileEntity

    //    private lateinit var userProfile: ProfileEntity
    private lateinit var binding: ExternalProfileLayoutBinding
    private val viewModel: ExternalProfileDetailesViewModel by sharedViewModel()
    private val mainViewModel: MainFragmentViewModel by sharedViewModel()
    private var phone: String? = null

    private val isAdmin by lazy {
        arguments?.getBoolean("isAdmin", false) ?: false
    }
    private val complainingUserId by lazy {
        arguments?.getInt("complaining_user_id", 0) ?: 0
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        phone = requireArguments().getString(code_num_key)
        profile = requireArguments().getParcelable<ProfileEntity>(MainFragment.PROFILE_KEY)
            ?: throw IllegalArgumentException()
        viewModel.setCurrentLikesAndDislikes(profile.approves, profile.disapproves)
//        userProfile = requireArguments().getParcelable<ProfileEntity>(USER_PROFILE)
//            ?: throw IllegalArgumentException()
        //   Log.d("AAA", "external user profile $userProfile")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ExternalProfileLayoutBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.updateVoteState(profile.id)
        initViews()
        setListeners()
        setAdminButtons()

        Log.d("voteMode", "unTrustVote vote state ${viewModel.getVoteState()}")
    }

    private fun setAdminButtons() {
        if(isAdmin) {
            binding.toBlock.isVisible = true
            binding.toCancel.isVisible = true
            binding.toBlock.setOnClickListener {
                viewModel.blockUser(profile.id)
            }
            binding.toCancel.setOnClickListener {
                viewModel.cancelComplaint(
                    userId = profile.id,
                    complainingUserId = complainingUserId
                )
            }
            binding.toBlock.setOnClickListener {
                viewModel.blockUser(profile.id)
            }
        } else {
            binding.toBlock.isVisible = false
            binding.toCancel.isVisible = false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.ext_profile_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
        menu.findItem(R.id.menu).isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.report_menu_item -> {
                //showToast("в разработке")
                showComplaintDialog()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    private fun showComplaintDialog() {
        val dialog = MaterialAlertDialogBuilder(requireContext()).also {
            it.setMessage("Хотите отправить жалобу?")
            it.setNegativeButton("Нет") { dialog, _ -> }
            it.setPositiveButton("Отправить") { dialog, _ ->
                viewModel.sendComplaint(profile.id)
            }
        }
        dialog.show()
    }


    private fun setListeners() {
        Log.d("voteMode", "subscribe")
        lifecycleScope.launch {
            viewModel.howUserRatedStatus.collect { voteStatus ->
                reVoteMode(voteStatus.code)
            }
        }
        lifecycleScope.launch {
            viewModel.infoMessage.collect {
                if (it != null) {
                    showToast(it)
                    viewModel.clearInfoMessages()
                }
            }
        }

    }

    private fun initViews() {

        binding.bDate.text = profile.birthDate
        binding.name.text = profile.fullName
        binding.region.text = profile.country
        binding.trust.text = getString(R.string.votes, profile.approves)
        binding.unTrust.text = getString(R.string.votes, profile.disapproves)
        binding.confirmStatus.setText(if (profile.isConfirmed) R.string.confirmed_profile else R.string.un_confirmed_profile)
        Glide.with(requireContext()).load("https://185.178.46.6/api/user/avatar/${profile.userPic}").into(binding.userPic)

        Log.d("voteMode", "init vote mode ${viewModel.getVoteState()}")

        lifecycleScope.launchWhenResumed {
            viewModel.likesCount.collect {
                log("collected likes $it")
                binding.trust.text = getString(R.string.votes, it)
                mainViewModel.updateProfileList()
            }
        }
        lifecycleScope.launchWhenResumed {
            viewModel.dislikesCount.collect {
                log("collected dislikes $it")
                binding.unTrust.text = getString(R.string.votes, it)
                mainViewModel.updateProfileList()
            }
        }

        binding.shareButton.setOnClickListener {
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.setType("text/plain")
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Trust not trust")
                var shareMessage = "\nПосмотри как за меня проголосовали\n\n"
                shareMessage =
                    shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n"
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "choose one"))
            } catch (e: Exception) {
                e.toString()
            }
        }


        binding.trustVote.setOnClickListener {
            findNavController().navigate(
                R.id.action_externalProfileDetailes_to_voteDialog,
                Bundle().also {
                    it.putString(P_ID_KEY, profile.id.toString())
                    it.putInt(V_TYPE, VoteMode.UP.ordinal)
                })
            Log.d("voteMode", "unTrustVote vote state ${viewModel.getVoteState()}")
        }

        binding.changeVote.setOnClickListener {
            findNavController().navigate(
                R.id.action_externalProfileDetailes_to_voteDialog,
                Bundle().also {
                    it.putString(P_ID_KEY, profile.id.toString())
                    it.putInt(
                        V_TYPE,
                        if (viewModel.getVoteState() == VoteStatus.LIKE) VoteMode.DOWN.ordinal else VoteMode.UP.ordinal
                    )
                })
        }

        binding.unTrustVote.setOnClickListener {
            findNavController().navigate(
                R.id.action_externalProfileDetailes_to_voteDialog,
                Bundle().also {
                    it.putString(P_ID_KEY, profile.id.toString())
                    it.putInt(V_TYPE, VoteMode.DOWN.ordinal)
                })
        }


        binding.ratingCardBox.setOnClickListener {
            findNavController().navigate(
                R.id.action_externalProfileDetailes_to_evaluatorsUsersFragment,
                Bundle().also { bundle ->
                    bundle.putParcelable(USER_PROFILE, profile)
                })
        }


        when (viewModel.getVoteState()) {
            VoteStatus.NONE -> {
                Log.d("voteMode", "when IDLE mode")
                reVoteMode(-1)
            }

            VoteStatus.LIKE -> {
                reVoteMode(0)
            }

            VoteStatus.DISLIKE -> {
                reVoteMode(1)
            }
        }
    }

    // вызывать, если мой голос изменился
    private fun reVoteMode(feedback: Int) {

        val redColor: Int? = context?.let {
            ContextCompat.getColor(it, R.color.cherry_red)
        }

        Log.d("AAA", "reVoteMode() feedback $feedback")
        binding.trustVote.visibility = if (feedback == -1) View.VISIBLE else View.INVISIBLE
        binding.unTrustVote.visibility = if (feedback == -1) View.VISIBLE else View.INVISIBLE
        binding.changeVote.visibility = if (feedback != -1) View.VISIBLE else View.INVISIBLE

        with(binding.changeVote) {
            setText(if (feedback == 1) R.string.can_not_trust else R.string.can_trust)
            setBackgroundColor(if (feedback == 1) checkNotNull(redColor) else Color.GREEN)
            setTextColor(if (feedback == 1) Color.WHITE else Color.BLACK)
        }
    }
}