package ru.example.ui.fragments.evaluatorsUsersFragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.flow.collectLatest
import ru.example.R
import ru.example.databinding.FragmentContactsBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.example.data.Contact
import ru.example.databinding.FragmentEvaluatorsUsersBinding
import ru.example.domain.ProfileEntity
import ru.example.ui.fragments.mainFragment.MainFragment
import ru.example.ui.fragments.mainFragment.MainProfilesAdapter
import ru.example.ui.fragments.userProfileFragment.UserProfileFragment
import ru.example.ui.fragments.userProfileFragment.UserProfileFragment.Companion.USER_PROFILE


class EvaluatorsUsersFragment : Fragment() {

    companion object {
        const val PROFILE_KEY = "pd"
    }

    private lateinit var binding: FragmentEvaluatorsUsersBinding
    private val viewModel: EvaluatorsUsersViewmodel by viewModel()
    private val adapter: EvaluatorsUsersAdapter by lazy { EvaluatorsUsersAdapter() }

    private lateinit var profile: ProfileEntity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        profile = requireArguments().getParcelable<ProfileEntity>(USER_PROFILE)
            ?: throw IllegalArgumentException()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel.setUserId(profile.id)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEvaluatorsUsersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.findItem(R.id.menu).isVisible = false
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        subscribe()
    }

    private fun initViews() {

        binding.evaluatorsUsersRw.adapter = adapter.also {
            it.bindAction { data ->
                data?.let { evaluators_profile ->

                    val userProfile = ProfileEntity(
                        evaluators_profile.id,
                        evaluators_profile.approves,
                        evaluators_profile.disapproves,
                        evaluators_profile.country,
                        evaluators_profile.city,
                        evaluators_profile.approved,
                        evaluators_profile.phone_number,
                        evaluators_profile.fullName,
                        evaluators_profile.name,
                        evaluators_profile.birthDate,
                        evaluators_profile.image_to_bytes,
                        evaluators_profile.rating,
                        evaluators_profile.statisticId,
                        true
                        )

                    findNavController().navigate(
                        R.id.action_evaluatorsUsersFragment_to_externalProfileDetailes,
                        Bundle().also { bundle ->
                            bundle.putParcelable(PROFILE_KEY, userProfile)
                           Log.d("AAA", "profile $profile")
                       })
                }
            }
        }
    }
    private fun subscribe() {
        lifecycleScope.launchWhenStarted {
            viewModel.evaluatorsUsersFlow.collectLatest { pageData ->
                pageData?.let {
                    adapter.submitData(it)
                    adapter.refresh()
                }
            }
        }
    }

}