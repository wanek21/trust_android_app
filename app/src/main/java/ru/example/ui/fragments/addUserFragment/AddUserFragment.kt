package ru.example.ui.fragments.addUserFragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.InputType.TYPE_NULL
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel
import ru.example.AnswerState
import ru.example.R
import ru.example.data.armeniaRegionsList
import ru.example.data.belarusRegionsList
import ru.example.data.kazakhstanRegionsList
import ru.example.data.kyrgyzstanRegionsList
import ru.example.data.russiaRegionList
import ru.example.data.tadjikistanRegionList
import ru.example.data.uzbekistanRegionsList
import ru.example.databinding.AddUserLayoutBinding
import ru.example.databinding.DialogCountryBinding
import ru.example.domain.PhoneNumber
import ru.example.log
import ru.example.ui.fragments.AuthFragment
import ru.example.ui.fragments.registrationFragment.DatePickerDialogHelper
import ru.example.ui.util.RegionDialog
import java.io.ByteArrayOutputStream
import java.io.IOException

class AddUserFragment : Fragment() {

    private var _binding: AddUserLayoutBinding? = null
    private val binding get() = checkNotNull(_binding)
    private lateinit var phone: String
    private val viewModel: AddUserViewModel by viewModel()
    private var selectedImageUri: Uri? = null
    private var sImage: String? = null

    companion object {
        const val REQUEST_IMAGE_PICK = 1001
        const val REQUEST_READ_EXTERNAL_STORAGE = 1002
        val image =
            "/9j/4QDmRXhpZgAASUkqAAgAAAAFABIBAwABAAAAAQAAADEBAgAcAAAASgAAADIBAgAUAAAAZgAAABMCAwABAAAAAQAAAGmHBAABAAAAegAAAAAAAABBQ0QgU3lzdGVtcyBEaWdpdGFsIEltYWdpbmcAMjAwNToxMjowMSAwMDoyNDo0OAAFAACQBwAEAAAAMDIyMJCSAgAEAAAANjg3AAKgBAABAAAAgAAAAAOgBAABAAAAgAAAAAWgBAABAAAAvAAAAAAAAAACAAEAAgAEAAAAUjk4AAIABwAEAAAAMDEwMAAAAAAKAAAA/8AAEQgAgACAAwEhAAIRAQMRAf/bAIQAAwICAgIBAwICAgMDAwMEBwQEBAQECQYGBQcKCQsLCgkKCgwNEQ4MDBAMCgoPFA8QERITExMLDhUWFRIWERITEgEEBQUGBQYNBwcNGxIPEhsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsb/8QAmQAAAwEBAQEBAQAAAAAAAAAABAUGAwIHCAABEAABAwMCAggFBAIABwAAAAABAgMEAAURBiESMQcTIkFRcYGRFDJCYaEIFTNScsEjJDRiY9HwAQACAwEBAQEAAAAAAAAAAAAEBQIDBgEHAAgRAAICAgEDAgQGAQUAAAAAAAECAAMEETEFEiFBUQYTYXEUIpGh0fAyQlKBseH/2gAMAwEAAhEDEQA/APiNIR3Jwa6SkY5UNNFNAk454zXaGz4CoGTEIQ3jZQrVLWaqJlgE1RHPhRLcfblVZMsAm6WB4YrrqiVYxmobloWfuoyK5VH765uS7ZmqPvyrJUfCdxUg0+1B1s4O4FCPMdnkKsBkCIElNdgeFEmBATVDeedbpQRsAaqJloE2Q0SrnRTbByN6qJlgEJQz4CiERycE1UTLVWbpjnurRMUkciaqLQhU3O/hO7euTEONwc1Dvlormao6hnsmh1sA/TUg04U1MHGQRmg3ovYyBzq9TKGWJE5HdWrfHxcqMMAEIQlWOVFNtrIFUtLQIYzGcJ2TRiIrnemqGYCXKphDcdf9aMZhuKHZRQ7MISlZJhjNseUr5CaNbtLuBlOKGewCM6sdjNf2ogfLXJtvY+Wqfmbh34aYO2/sfLQL1vAB7NWq8HsogLtuc4iEpJrL9lkuJBKm0/5KxRItAi9qCeJHtN+BNGMMEnambGJlEOZZWBgNgn70azFWrm3jyFDMQISqkxlHgk4GVDP2pkzZ5SyOBpavvignsA5jCulm4jONp6QtAyyr2pvC029txNqpdZeAI5x8JifIjhjTriUgJaUKOTp55DeC0ceVLnvHvNHVjBRPxsbgQT1YHpQcizK4d0n2qK3Dctegai2RaCMnHvS2TbQjmBnzo5LNxZbRqJ5UQDPaFJpcQEnCuf3plU0RX1SSjRHyRxNUyZhrGCUCmzMJnVQxhHjLH0pprFaUPqQPMUFYRGFKajeO4lAGHEHHICmsOUOMJKlkeAFLbF3HVDaMprWUOKymOs+GasLPZ5twdQzFgqcUvZKUpJJP+6Q5JC8maykKK+4+J6vp79NnSjfYSJKNPqitL3CpTiWdvHB3/FP3/wBIvSQI3El21rV/UTMH8pqFeHk2DuC+PrM5kfFPTcezsDE/YeJGam/Tz0l6bguSp2mZLjDeeJ2MQ8kDx7OTj0ry642h+PxcSVDG1RIelu2wajrD6li9QTvobf8A3JqbHWFEZNIZsdwAjCj9t6aUsJDIEUvw3dzwK8sZpa/b3lJ3aUPMUyRxEltZMnottU4QkTG1D/KnUTTbkhwJRJT5A5o6y4L6RPRiNZwY/gaDfWni+NQCadRujZ95QK56Dmk9vUFX0j2vpD68tH1v6KwMFU9s+tUkHosa6xI+LR9uJVI7+rfSNKsBKR58z1Lo1/T9P1dqVESC60GW8KfeIyhpPifv4Dvr7B0B0U6P6O7YhFmgJdmhOHJzyQXVeOP6j7D81f0qv8a34hx4HH8zDfFXWmA/BU+P938fzLLO+fc1/CsDbO+K1wAnmJMxcXhWc15Z0ndCmkdfxHJPwrduueOxLYQE8R/8iRsrz50JlY631lTDen9Rt6fkC6s/ce49p8ea76O1aM1Y9aLyw62+1uCkdlxJ5KSe8GoWbb7O0SVB059KxyNcjlG5E90xshculbV4I3E8k2ZsEhpY9c0imz7SnI6t0DzppSLWlthRR5nnMe1yePZY96orPbpCHwFKUE95znFaC6wETO4mPYHEtrPa1Oqwq5I27iKr7fpy5LSFMOpcH2NZfKvVT+YTaVVlF8mUVu0vdysZiqPqarbJpO5qntMJhrU44oJSAOZJ2FZrJya28Az6y+usHZn1h0awrvofVcjo/e0fINvTFYnMX2OB1DzikkPNPEnIcQpI4eEEFKxyKTn0ouJSgqUoBIGST3V6BhqmPQiKfQfr6/vPzxnXNlZD3NySTMY86JNidfClsyGlcltOBaT6jav448kbFY96Z94HMVt7QdTu2QRw+OaDkOZB3AGPGvi4lWp5N06aFZ1r0RyFxmkG525Cn4i/qVgZU35ED3Ar4VusacHlANLx5VmMtUXI7veet/CWT8zEapv9J/Y/+7k5Ntt0fBCI6hUzNsV0XIW3kFaACpIOSkHlkeh9qKotrWanIrd+I8t+kYiSklSjj71TW7TMAAAn80PkZTR1j4aVSqtenoCAkoQk48TVpZrZCZKVBtIP2NZLNyXIMLt/KviWltSwnhCUg+u9XGjktHXtvzwpAfSrJ7sb/wCqzBsHeAZj+oEitj9J72xc1qaCjJBUR4UPcurudmkW2eWpEWW0pl5lwcSHEKGFJI7wQSK3GNksACGnkViDfEV6Qs1o0HoY6dsLaI9vRKkSWI6cJQwHXFOFtAHJCVKISO4YHdTCTcwoZC0+9Ozn952x8wM1EmStjsFl0tru836zvyG3L6sPy2FylrYL2SVOoQSQhagUhXDgEITt3lq9ett3Acfeu2dQ7z3EzvySYluN3K2VAqGCN96+VrxGtV3v16ttnlITLgvOMuDqu0yrJ4ThScEeBGQcHwNZ7PyC4D+g5mt6ATQ7KDomT0TT85On0JvvwqpySUuLjJKW3ADgLAO4Khvjuzip256Os69RoupbcTLaR1YcQsjiRnPCRyIzvuOe9DUZunPy+Pr7T0erdyL38jX6yPgOnYFZ9qfQHM4wT7VobxNZWPEpLetIx2iKqLe6nAwv81l8sEgyNo8alRbX2yoArNWOm5rbOoYri3SkJcTk55VjcglXBmTz6yVIntEUrDYIcTjzrRbygcKd9jT6hz28zy6xRviY9aetPNSSOed6ycVxowEkd2c0yVhqCkQCQ2odnJ8cZqejSNTt68lxrpEgrsy8qhyo76uvRsOw62QBjOcKSTywQOdWgpo9x+0kBO7lIitsKUVEBIycmvE7tJbdvDksJTxniSF/UElWeHPhnfFJ8uzegJpuj1EsSZN3GbhCsLGTUrcpuQR1gzR2FVxN5SoCzyeFqyGnHHIbB86bxtX27IJlp9DW8txGPpG9WdSw5jiNre1pQB15UR3CnkPXUEY6tLivSk9/T3bmFC2u3/Exuz0kIYV/w4yz60UOmCQyOJmNwnwO9Jn6Etp/MYLbjpYfMNR+pHVrCkthaeAYGxOacMfqR1I9b+FEQl08lcRx7VU3w5XWAUsIiZ+j4dh8CLrh+oXXzDQXBYXIVxZW0ghJCQMkjPM+AHM03tfSz0kG5vtS74xIiLeU6yoMEOtNqwQhRyAcHONs4wDnGalZ0ummv/I7P1/vMDbo2M1ugmgP3+3tqbWjXEbTF5uFxtanEybooOTeBzAdcBUesIP1dognvASO4VhM6dJS7gpD8l5CUjYJVkk+eKCGBk5Tlmfz/RKx0THU71JLUXTRfJaVRY0iQgK24yscvaoO6dIV5bUsOSXFnGw4tq0GH0epAO/yYfXj04q/lWT7/SHd1sK4piUk8uI5xSCXrO4pWVPXXJV4DlWjpwKk4WD2ZWh4Op5/CwpQys+9UEFLKkgFZ38af271F+MAT5j2I20lQw4PankRwtpwlxOKUW+ZqcYBeI0aW443/KlJ7966+CW6R/zSR65oAsF9IewJgt0tUuLbFSIC2JMhKhwsuOcAcGRkA5ABxk5NaS7dKlWBcR2eqOFjbqXihaD3YUn/AO2qQtUgEjzFz1uSyb8Ec+swDt2aS2ly5Fa0JCSoJO5A3Przoxq8X1tI7RIByCds11q6nHkSpTcvje5xK1HenWlcTBO2+OdT0y9XV1RKi5v9I2q2mipeILfdbwBFMi4PHdanEEeZNBSLgop4nJDqvNJpiqD0ih7D6mKX7ukKx1Kz9yKWy7mFIwmOT60alf1i6y3fpNoUDJHa3p5CipTzV+aja0Z49Wo6iMt4GSR601jtNAZ6w+hpZYTNJQi6haJEJmW3GemNoddzwNqcAUvHPA5n0oy1xJaJkgzJiH2VOcUYJRwrQnA7KjyO+cbd+9Aue0HY+0KUFnHafA5/SGyNMWu5z48p9s/ERVhbLwPbRg5wD4HcEd4p/FsUNWCvBz4mlt2S4XXtOGtK2Zl9eY3j2C1pwcoTRf7RbMEEJUP8aTPl2kyosYM9boAVksIPpSyVAtqUbRW80RTdY3rI7iCdDilJHUNj0qelwYnESqOg+lPqHbXMgyq3IiWZFgb8TSPakctu3ozhpA9KcVFzFmRXUPOojhyCADnbzptHkDAPENvvRziA0tGMeU2rk6CAcHB5HwreBMmpv0ht8IVEISth3jAUDjtII7wDgg/fHdQTKPO43qc7Uj/n7RqfgJL7Tj6G1rYWlxtRA4kKHIg8xTdickAYc/NAWKTzG9ZQbK+sPj3PhH8hpgzdlAfOaXWVbkGO4Y3enAMJKq3/AH55KOWfWgmxlJ8yrUydv7xHIA+dLpF4cUT/AO6urx1EiRE8u6LVkf7pJMuSt+yD602prErZtCIZt1OTlP5qem3NJz2R703qr1EmTduQFr1ZKdQpD8VTbjSynIyErGdlDPiPaj4Opbsb+40/Hb+FKeJpxBPEMYHCocs7k5HdTpqF8zMpk26Ugff+/wB8RjHujjN0dlMpUlyRguAuHhJAwDjlnHfTFF+ndxTQr1D1h1eQ6+FhDN9ufECFDNMI1+u4PzJ50I1SGHV5d44jJjUF6JASpA8xTBm93pQ/mQPSg3pqjOvIyHhiLveiN5SeXhXZud3POfj7YoY1Vj0hga0jyZmZ11V8078UO9PuAH/WH2qYRPacY2AcxdIuc7BPxKjSqVeJY2Lx8aMrrWLrbrB6xTKu72+VA0ol3Y75CaOrr3xFFtpPMhWLggJG+fWjmbqEkEGm7IYqVwYU3d9h2hmjWbsCOY96HZNy4NDWbruOFX5o9m7Odzn5oZqpetpEOYvK0pyXM+tGN6gKfr/NCtUDDa8ntm6NSuf32rs6mIHz1UaRChnGYuaoXyDv5oR7UhOxdPlmprQJS+cT6xdJ1GnH8nP70rlX8HiHETRaU+8AfILRZIvZI7IFK5F0Wv6vOi0T2grPJBmQsnY58qYtSXFHIUkjzozczlF5hbbp4AeIAnlvRDclYwlK8+tQYAxqtqmEtzFBY4jijGbgrIHWAUOw1L+6GNXE7dvPrW6bl/3VUQJ93TQXEE/Odq6+NBx2/Peudok+6cmUjG6+fKslzG8bnkK+1PtiBvzWuHOaAenNnISc+VWAb4nCwEBelAg5UOfjQb01kJOFgk0So1BLclFHM//Z"

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        phone = requireArguments().getString(AuthFragment.code_num_key)
            ?: throw IllegalArgumentException("Phone is not provided")

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        goneCustomActionBar()

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.findItem(R.id.menu).isVisible = false
    }

    override fun onResume() {
        super.onResume()
        goneCustomActionBar()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = AddUserLayoutBinding.inflate(layoutInflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        subscribe()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun goneCustomActionBar() {
        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar
        actionBar?.displayOptions = ActionBar.DISPLAY_SHOW_TITLE
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        requireActivity().invalidateOptionsMenu()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_PICK && resultCode == Activity.RESULT_OK) {
            val selectedImageUri: Uri? = data?.data

            if (selectedImageUri != null) {
                setImageUri(selectedImageUri)
            }
            try {
                selectedImageUri?.let {
                    val bitmap: Bitmap =
                        MediaStore.Images.Media.getBitmap(
                            requireActivity().contentResolver,
                            selectedImageUri
                        )
                    val stream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
                    val bytes: ByteArray = stream.toByteArray()
                    sImage = Base64.encodeToString(bytes, Base64.DEFAULT)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, REQUEST_IMAGE_PICK)
            } else {
            }
        }
    }

    private fun setImageUri(uri: Uri) {
        binding.userPic.setImageURI(uri)
        selectedImageUri = uri
    }

    private fun setRegionDialog(context: Context) {
        val bindingDialogCountryBinding = DialogCountryBinding.inflate(LayoutInflater.from(context))
        val view = bindingDialogCountryBinding.root
        binding.regionEt.setText(String())

        val regionList = listOf(
            bindingDialogCountryBinding.russia,
            bindingDialogCountryBinding.belarus,
            bindingDialogCountryBinding.armenia,
            bindingDialogCountryBinding.kazakhstan,
            bindingDialogCountryBinding.kyrgyzstan,
            bindingDialogCountryBinding.tadjikistan,
            bindingDialogCountryBinding.uzbekistan
        )

        val dialog = Dialog(context)
        dialog.setContentView(view)

        val positiveButton = bindingDialogCountryBinding.positiveButton
        val negativeButton = bindingDialogCountryBinding.negativeButton
        var selectedCountry = ""

        for ((index, selectedItem) in regionList.withIndex()) {
            selectedItem.setOnClickListener {
                selectedItem.setTextColor(Color.GREEN)
                selectedCountry = selectedItem.text.toString()

                for ((i, item) in regionList.withIndex()) {
                    if (index != i) {
                        item.setTextColor(Color.WHITE)
                    }
                }
            }
        }

        positiveButton.setOnClickListener {
            binding.regionEt.text.clear()
            binding.countryEt.setText(selectedCountry)
            dialog.dismiss()
        }

        negativeButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun setupSpinner() {
        val spinner = binding.countrySpinner
        val countries = resources.getStringArray(R.array.country_codes)
        val adapter =
            ArrayAdapter(requireContext(), R.layout.custom_spinner_item, countries)
        adapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item)

        spinner.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                val selectedCountry = parent.getItemAtPosition(position).toString()
                val countryCode = when (selectedCountry) {
                    "Россия" -> "+7"
                    "Беларусь" -> "+375"
                    "Армения" -> "+374"
                    "Казахстан" -> "+7"
                    "Узбекистан" -> "+998"
                    "Таджикистан" -> "+992"
                    "Кыргызстан" -> "+996"
                    else -> ""
                }
                // Устанавливаем выбранный код страны
                val countryIndex = countries.indexOf(selectedCountry)
                if (countryIndex != -1) {
                    binding.countryCode.text = countryCode
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun initTextWatchers() {

        val dateTextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            }

            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable?) {
                if (!s.isNullOrEmpty() && s.length % 3 == 0 && s[s.length - 1] != '.') {
                    binding.birthDateEt.setText(
                        s.toString().substring(0, s.length - 1) + "." + s[s.length - 1]
                    )
                    binding.birthDateEt.setSelection(binding.birthDateEt.text.length)
                }
            }
        }


        /*val phoneTextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                if (!s.toString().startsWith("+")) {
                    binding.phoneEt.setText("+$s")
                    binding.phoneEt.setSelection(binding.phoneEt.length())
                }
            }
        }*/

        binding.birthDateEt.addTextChangedListener(dateTextWatcher)
        //binding.phoneEt.addTextChangedListener(phoneTextWatcher)

        val textWatchers = listOf(
            binding.nameEt,
            binding.birthDateEt,
//            binding.countryEt,
//            binding.regionEt
        )

        textWatchers.forEach { editText ->
            editText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    if (editText != binding.birthDateEt) {
                        val input = s.toString()
                        val words = input.split(" ")
                        val capitalizedWords = words.map { it.capitalize() }
                        val formattedName = capitalizedWords.joinToString(" ")
                        editText.removeTextChangedListener(this)
                        editText.setText(formattedName)
                        editText.setSelection(formattedName.length)
                        editText.addTextChangedListener(this)
                    }
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })
        }
    }


    private fun getRegionsList(country: String): List<String> {
        return when (country) {
            "Беларусь" -> belarusRegionsList
            "Армения" -> armeniaRegionsList
            "Казахстан" -> kazakhstanRegionsList
            "Узбекистан" -> uzbekistanRegionsList
            "Таджикистан" -> tadjikistanRegionList
            "Кыргызстан" -> kyrgyzstanRegionsList
            else -> russiaRegionList
        }
    }


    private fun setRegionDialog() {
        val selectedCountry = binding.countryEt.text.toString()
        val regionDialog =
            RegionDialog(
                requireContext(),
                getRegionsList(selectedCountry)
            ) { selectedRegion ->
                Log.d("AAA", "AddUserFragment selected region: $selectedRegion")
                binding.regionEt.setText(selectedRegion)
            }
        regionDialog.show()
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun initViews() {
        setupSpinner()

        //binding.phoneInput.addTextChangedListener(PhoneTextWatcher(binding))
        val datePickerDialogHelper = DatePickerDialogHelper(requireContext(), binding.birthDateEt)

        binding.regionEt.inputType = TYPE_NULL
        binding.countryEt.inputType = TYPE_NULL
        binding.birthDateEt.inputType = TYPE_NULL

        binding.countryEt.setText("Россия")
        initTextWatchers()

        binding.userPic.setOnClickListener {
            val permission = Manifest.permission.READ_EXTERNAL_STORAGE

            if (ContextCompat.checkSelfPermission(requireContext(), permission)
                != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(permission),
                    REQUEST_READ_EXTERNAL_STORAGE
                )
            } else {
                val intent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, REQUEST_IMAGE_PICK)
            }
        }
        binding.iconCalendar.setOnClickListener {
            datePickerDialogHelper.showDatePickerDialog()
        }

        binding.iconCounty.setOnClickListener {
            setRegionDialog(requireContext())
        }

        binding.iconRegion.setOnClickListener {
            setRegionDialog()
        }

        binding.regionEt.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                setRegionDialog()
            }
            false
        }

        binding.countryEt.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                setRegionDialog(requireContext())
            }
            false
        }

        binding.birthDateEt.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                datePickerDialogHelper.showDatePickerDialog()
            }
            false
        }

        binding.nameEt.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                datePickerDialogHelper.showDatePickerDialog()
                true
            } else {
                false
            }
        }

        binding.createProfile.isEnabled = true

        binding.createProfile.setOnClickListener {
            if (checkFields()) {
                try {
                    val split = splitField(binding.nameEt.text) ?: throw IllegalStateException()

                    with(binding) {
                        birthDateEt.setBackgroundResource(R.drawable.rounded_edittext)
                        birthDateLayout.setBackgroundResource(R.drawable.rounded_edittext)
                        countryEt.setBackgroundResource(R.drawable.rounded_edittext)
                        regionEt.setBackgroundResource(R.drawable.rounded_edittext)
                        nameEt.setBackgroundResource(R.drawable.rounded_edittext)
                    }

                    viewModel.addUserProfile(
                        phoneNumber = PhoneNumber(
                            binding.countryCode.text.toString().substring(1).toInt(),
                            binding.phoneEt.text.toString().toLong()
                        ),
                        profile = AddUserProfileForm(
                            split.first,
                            split.second,
                            split.third,
                            binding.birthDateEt.text.toString(),
                            binding.countryEt.text.toString(),
                            "${binding.regionEt.text.toString()} ",
                        ),
                    )
                } catch (e: Exception) {
                    log("ex: ${e.toString()}")
                    Snackbar.make(requireView(), "Поля заполнены неверно", Snackbar.LENGTH_SHORT)
                        .show()
                }
            } else {
                with(binding) {
                    if (birthDateEt.text.isNullOrEmpty()) {
                        birthDateLayout.setBackgroundResource(R.drawable.rounded_edittext_error)
                    }
                    if (countryEt.text.isNullOrEmpty()) {
                        countryLayout.setBackgroundResource(R.drawable.rounded_edittext_error)
                    }

                    phoneEt.setCustomBackgroundIfError(R.drawable.rounded_edittext_error)
                    regionEt.setCustomBackgroundIfError(R.drawable.rounded_edittext_error)
                    nameEt.setCustomBackgroundIfError(R.drawable.rounded_edittext_error)

                    //createProfile.isEnabled = false
                    disclaimerTv.text = getString(R.string.please_fill_in_the_required_fields)
                    disclaimerTv.setTextColor(resources.getColor(R.color.cherry_red))
                }
            }
        }
    }

    private fun userAlreadyExist() {
        with(binding) {
            phoneEt.setBackgroundResource(R.drawable.rounded_edittext_error)

            //createProfile.isEnabled = false
            disclaimerTv.text = getString(R.string.such_a_person_already_exists)
            disclaimerTv.setTextColor(resources.getColor(R.color.cherry_red))
        }
    }

    private fun EditText.setCustomBackgroundIfError(drawableResource: Int) {
        if (text.isNullOrEmpty()) {
            setBackgroundResource(drawableResource)
        }
    }

    private fun splitField(text: Editable?): Triple<String, String, String?>? {
        return text?.run {
            val inputSplit = text.toString().split(" ")
            if (inputSplit.size >= 3) {
                Triple(inputSplit.first(), inputSplit[1], inputSplit.last())
            } else if (inputSplit.size == 2) {
                Triple(inputSplit.first(), inputSplit[1], null)
            } else {
                null
            }
        }
    }


    private fun checkFields(): Boolean {
        return binding.run {
            !nameEt.text.isNullOrEmpty() && !birthDateEt.text.isNullOrEmpty()
                    && !countryEt.text.isNullOrEmpty() && !regionEt.text.isNullOrEmpty()
        }
    }


    private fun subscribe() {
        lifecycleScope.launchWhenStarted {
            viewModel.answerFlow.collectLatest {
                if (it == AnswerState.SUCCESS && viewModel.userAlreadyAdded) {
                    Toast.makeText(
                        requireContext(),
                        "User already added",
                        Toast.LENGTH_SHORT
                    )
                        .show()

                    userAlreadyExist()
                }
                if (it == AnswerState.SUCCESS && !viewModel.userAlreadyAdded) {
                    binding.phoneEt.setBackgroundResource(R.drawable.rounded_edittext)
                    binding.disclaimerTv.text = ""
                    Toast.makeText(
                        requireContext(),
                        "Пользователь добавлен",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                } else {
                    //Toast.makeText(requireContext(), "Server error", Toast.LENGTH_SHORT).show()
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.error.collect {
                if(!it.isNullOrEmpty()) {
                    userAlreadyExist()
                    Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                    viewModel.error.emit(null)
                }
            }
        }
    }


}