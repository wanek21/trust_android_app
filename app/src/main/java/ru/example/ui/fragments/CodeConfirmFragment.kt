package ru.example.ui.fragments

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.flow.collectLatest
import org.koin.android.ext.android.inject
import ru.example.AnswerState
import ru.example.CodeCell
import ru.example.CodeConfirmViewModel
import ru.example.R
import ru.example.databinding.CodeConfirmLayoutBinding
import ru.example.ui.MainActivity
import kotlin.time.DurationUnit
import ru.example.domain.PhoneNumber
import ru.example.log
import kotlin.time.toDuration

class CodeConfirmFragment : Fragment() {

    companion object {
        const val code_num_key = "cnc"
    }

    private lateinit var binding: CodeConfirmLayoutBinding
    private var phone: String? = null
    private var countryCode: String? = null
    private var phoneNumber: String? = null
    private val viewModel: CodeConfirmViewModel by inject()

    private val navController by lazy {
        findNavController()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        phone = requireArguments().getString(code_num_key)
        countryCode = requireArguments().getString("country_code")
        phoneNumber = requireArguments().getString("phone_number")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.findItem(R.id.menu).isVisible = false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = CodeConfirmLayoutBinding.inflate(inflater, container, false)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val actionBar = (requireActivity() as AppCompatActivity).supportActionBar
        actionBar?.show()

        //subscribe()
        setCodeVerificationListener()

        binding.textView4.text = getString(R.string.code_conf_bottom_info, phone)

        val editTexts = arrayOf(
            binding.editTextPhone1,
            binding.editTextPhone2,
            binding.editTextPhone3,
            binding.editTextPhone4
        )

        fun isAllEditTextsFilled(): Boolean {
            return editTexts.all { it.text.isNotBlank() }
        }

        editTexts.forEachIndexed { index, editText ->
            val nextIndex = (index + 1) % editTexts.size

            editText.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    Log.d("@@@", "onTextChange chars $s")
                    binding.sendButton.isEnabled = !s.isNullOrEmpty()
                    Log.d("@@@", "onTextChange btn  ${binding.sendButton.isEnabled}")
                }

                override fun afterTextChanged(s: Editable?) {
                    if (!s.isNullOrEmpty()) {
                        if (index == editTexts.size - 1) {
                            // Последний EditText, проверяем код
                            checkVerificationCode()
                        } else {
                            // Переходим к следующему EditText
                            editTexts[nextIndex].requestFocus()
                        }
                    }
                }
            })
        }

        val counter = object : CountDownTimer(120000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val duration = millisUntilFinished.toDuration(DurationUnit.MILLISECONDS)
                val timeString =
                    duration.toComponents { minutes, seconds, _ ->
                        String.format("%02d:%02d", minutes, seconds)
                    }
                binding.counter.text = timeString
            }

            override fun onFinish() {
                binding.counterHeader.visibility = View.GONE
                binding.counter.visibility = View.GONE
                binding.sendButton2.visibility = View.VISIBLE
                binding.sendButton2.isEnabled = true
            }
        }
        counter.start()

        binding.sendButton2.setOnClickListener {
            counter.start()
            binding.counterHeader.visibility = View.VISIBLE
            binding.counter.visibility = View.VISIBLE
            binding.sendButton2.visibility = View.GONE
            binding.sendButton2.isEnabled = false
            phone?.let {
                viewModel.resendCodeRequest(
                    PhoneNumber(countryCode!!.toInt(), phoneNumber!!.toLong())
                )
            }
        }

        binding.sendButton.setOnClickListener {
            if (isAllEditTextsFilled()) {
                checkVerificationCode()
            }
        }
    }

    private fun checkVerificationCode(code: String) { // for debug
        phone?.let {
            viewModel.sendCodeForVerification(
                CodeCell(
                    it,
                    code
                )
            )
            Log.d(
                "AAA",
                "code: ${binding.editTextPhone1.text} ${binding.editTextPhone2.text} " +
                        "${binding.editTextPhone3.text} ${binding.editTextPhone4.text}"
            )
        }
    }

    private fun checkVerificationCode() {
        phone?.let {
            viewModel.sendCodeForVerification(
                CodeCell(
                    it,
                    "${binding.editTextPhone1.text}${binding.editTextPhone2.text}" +
                            "${binding.editTextPhone3.text}${binding.editTextPhone4.text}"

                )
            )
            Log.d(
                "AAA",
                "code: ${binding.editTextPhone1.text} ${binding.editTextPhone2.text} " +
                        "${binding.editTextPhone3.text} ${binding.editTextPhone4.text}"
            )
        }
    }

    private fun setCodeVerificationListener() {
        lifecycleScope.launchWhenStarted {
            viewModel.codeConfirmState.collect { authAnswer ->
                log("codeConfirmState collected: $authAnswer")
                if(authAnswer != null) {
                    if(authAnswer.isExist) {
                        log("navigate to main")
                        navController.navigate(
                            R.id.action_codeConfirmFragment_to_mainFragment,
                            Bundle().also {
                                it.putString(code_num_key, phone)
                            })
                    } else {
                        log("navigate to registration")
                        navController.navigate(
                            R.id.action_codeConfirmFragment_to_registrationFragment,
                            Bundle().also { it.putString(code_num_key, phone) }
                        )
                    }
                }
            }
        }
    }

    private fun subscribe() {
        lifecycleScope.launchWhenStarted {
            viewModel.answerFlow.collectLatest {
                Log.d("@@@", "codeConfirm $it")
                if (it.first == AnswerState.SUCCESS) {
                    val navController = findNavController()
                    if (it.second) {
                        log("navigate to main")
                        //  navController.popBackStack(R.id.mainFragment, true)
                        navController.navigate(
                            R.id.action_codeConfirmFragment_to_mainFragment,
                            Bundle().also {
                                it.putString(code_num_key, phone)
                            })


                        /*                        val navController = findNavController()
                                                val graph = navController.navInflater.inflate(R.navigation.app_nav)

                                                graph.setStartDestination(
                                                    R.id.mainFragment
                                                )
                                                navController.graph = graph*/


                        (requireActivity() as MainActivity).setupToolbar()
                    } else {
                        log("navigate to registration")
                        navController.navigate(
                            R.id.action_codeConfirmFragment_to_registrationFragment,
                            Bundle().also { it.putString(code_num_key, phone) }
                        )
                    }
                }
            }
        }
    }

}