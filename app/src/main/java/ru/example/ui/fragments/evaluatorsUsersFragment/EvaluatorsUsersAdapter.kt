package ru.example.ui.fragments.evaluatorsUsersFragment

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import ru.example.databinding.EvaluatorsUsersItemBinding
import ru.example.domain.EvaluatorsProfileEntity
import ru.example.domain.ProfileEntity

class EvaluatorsUsersAdapter : PagingDataAdapter<EvaluatorsProfileEntity, EvaluatorsUsersViewHolder>(ProfilesDiffCallBack()) {

    private var onProfileClicked: ((EvaluatorsProfileEntity?) -> (Unit))? = null
    override fun onBindViewHolder(holder: EvaluatorsUsersViewHolder, position: Int) {
        holder.onBind(getItem(holder.layoutPosition))
        holder.itemView.setOnClickListener {
            onProfileClicked?.invoke(getItem(holder.layoutPosition))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EvaluatorsUsersViewHolder {
        return EvaluatorsUsersViewHolder(
            EvaluatorsUsersItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    fun bindAction(action: (EvaluatorsProfileEntity?) -> Unit){
        this.onProfileClicked = action
    }

    class ProfilesDiffCallBack: DiffUtil.ItemCallback<EvaluatorsProfileEntity>(){

        override fun areItemsTheSame(oldItem: EvaluatorsProfileEntity, newItem: EvaluatorsProfileEntity): Boolean =
            oldItem.id == newItem.id


        override fun areContentsTheSame(oldItem: EvaluatorsProfileEntity, newItem: EvaluatorsProfileEntity): Boolean =
            oldItem == newItem


    }

}
