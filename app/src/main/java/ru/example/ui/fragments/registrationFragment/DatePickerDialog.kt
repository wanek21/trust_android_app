package ru.example.ui.fragments.registrationFragment

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.NumberPicker
import ru.example.databinding.AddUserLayoutBinding
import ru.example.databinding.DialogDatePickerBinding
import ru.example.databinding.RegistrationLayoutBinding
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class DatePickerDialogHelper(private val context: Context, private val editText: EditText) {


    fun showDatePickerDialog() {
        val bindingDatePicker = DialogDatePickerBinding.inflate(LayoutInflater.from(context))
        val view = bindingDatePicker.root
        val dayPicker = bindingDatePicker.dayPicker
        val monthPicker = bindingDatePicker.monthPicker
        val yearPicker = bindingDatePicker.yearPicker

        // Настройка NumberPicker
        setSettings(yearPicker, dayPicker, monthPicker)

        // Создание Dialog
        val dialog = Dialog(context)
        dialog.setContentView(view)

        val positiveButton = bindingDatePicker.positiveButton
        val negativeButton = bindingDatePicker.negativeButton

        positiveButton.setOnClickListener {
            val selectedDay = dayPicker.value
            val selectedMonth = monthPicker.value
            val selectedYear = yearPicker.value
            val selectedDate = SimpleDateFormat("dd.MM.yy", Locale.getDefault())
                .format(
                    Calendar.getInstance()
                        .apply { set(selectedYear, selectedMonth - 1, selectedDay) }.time
                )
             editText.setText(selectedDate)

         //   binding.birthDateEt.setText(selectedDate)
            dialog.dismiss()
        }

        negativeButton.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun setSettings(
        yearPicker: NumberPicker,
        dayPicker: NumberPicker,
        monthPicker: NumberPicker
    ) {

        val currentYear = Calendar.getInstance().get(Calendar.YEAR)
        val currentMonth = Calendar.getInstance().get(Calendar.MONTH)
        val currentDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)

        yearPicker.minValue = currentYear - 100
        yearPicker.maxValue = currentYear
        yearPicker.value = currentYear

        dayPicker.minValue = 1
        dayPicker.maxValue = 31

        monthPicker.minValue = 1
        monthPicker.maxValue = 12

        dayPicker.value = currentDay
        monthPicker.value = currentMonth + 1

        monthPicker.displayedValues = arrayOf(
            "янв.",
            "фев.",
            "мар.",
            "апр.",
            "май.",
            "июн.",
            "июл.",
            "авг.",
            "сен.",
            "окт.",
            "ноя.",
            "дек."
        )
    }
}

