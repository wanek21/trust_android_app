package ru.example.ui.fragments.admin

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import ru.example.R
import ru.example.log
import ru.example.ui.fragments.admin.placeholder.PlaceholderContent
import ru.example.ui.fragments.mainFragment.MainFragment
import ru.example.ui.fragments.mainFragment.MainFragmentViewModel

/**
 * A fragment representing a list of Items.
 */
class AdminFragment : Fragment() {

    private var columnCount = 1

    private val viewModel: MainFragmentViewModel by sharedViewModel<MainFragmentViewModel>()

    private val usersAdapter by lazy {
        UsersToBlockViewAdapter(usersList)
    }
    private var usersList = mutableListOf<UserToBlockItem>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }

        setListeners()
    }

    override fun onResume() {
        super.onResume()
        viewModel.updateComplaintsList()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_admin, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                usersAdapter.onItemClick = { userId, complainingUserId ->
                    lifecycleScope.launch {
                        viewModel
                            .getUserById(userId)
                            .collect { result ->
                                if (result.isSuccess) {
                                    val user = result.getOrNull()
                                    if (user != null) {
                                        log("Navigation to user: $user")
                                        findNavController().navigate(
                                            R.id.action_adminFragment_to_externalProfileDetailes,
                                            Bundle().also { bundle ->
                                                bundle.putParcelable(MainFragment.PROFILE_KEY, user)
                                                bundle.putBoolean("isAdmin", true)
                                                bundle.putInt("complaining_user_id", complainingUserId)
                                            })
                                    }
                                } else log("getUserById error: $result()")
                            }
                    }
                }
                adapter = usersAdapter
            }
        }
        return view
    }

    // Call this function when you need to update adapter
    private fun notifyAdapter(list: MutableList<UserToBlockItem>) {
        usersAdapter.items = list
    }

    private fun setListeners() {
        lifecycleScope.launch {
            viewModel.complaintsList.collect { complaintsList ->
                log("setListeners: $complaintsList")
                notifyAdapter(complaintsList.toMutableList())
            }
        }
    }

    companion object {
        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"
    }
}