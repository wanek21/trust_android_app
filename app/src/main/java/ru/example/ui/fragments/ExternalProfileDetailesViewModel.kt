package ru.example.ui.fragments

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.viewbinding.BuildConfig
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import ru.example.AnswerState
import ru.example.ApiService
import ru.example.data.UsersRepository
import ru.example.domain.ComplaintUserRequest
import ru.example.domain.VoteMode
import ru.example.log

enum class VoteStatus(val code: Int) {
    LIKE(1),
    DISLIKE(0),
    NONE(-1)
}

class ExternalProfileDetailesViewModel(
    private val apiService: ApiService,
    private val usersRepository: UsersRepository
) : ViewModel() {

    private var voteState = VoteMode.IDLE

    val voteFlow = MutableSharedFlow<Int>(1)

    private val _answerFlow = MutableSharedFlow<AnswerState>()
    val answerFlow: SharedFlow<AnswerState> = _answerFlow

    /*private val _profilesList = MutableStateFlow<List<EvaluatorsProfileEntity>?>(emptyList())
    val profilesList: StateFlow<List<EvaluatorsProfileEntity>?> = _profilesList

    private val _numbersList = MutableStateFlow<List<ProfileEntity>?>(emptyList())
    val numberList: StateFlow<List<ProfileEntity>?> = _numbersList*/

    // в него записывается значение сразу после получения с апи
    private val _userRating = MutableStateFlow<Int>(1) // feedback ввиде -1 0 и 1
    val userRating: StateFlow<Int> = _userRating

    private val _likesCount = MutableStateFlow(0)
    val likesCount: StateFlow<Int> = _likesCount

    private val _dislikesCount = MutableStateFlow(0)
    val dislikesCount: StateFlow<Int> = _dislikesCount

    private val _userProfile = MutableStateFlow(0)
    val userProfile: StateFlow<Int>  = _userProfile

    private val _howUserRatedStatus = MutableStateFlow(VoteStatus.NONE)
    val howUserRatedStatus = _howUserRatedStatus.asStateFlow()

    private val _infoMessage = MutableStateFlow<String?>(null)
    val infoMessage: StateFlow<String?> = _infoMessage

    fun upLikes() {
        val currentLikes = _likesCount.value
        _likesCount.update {
            currentLikes + 1
        }
    }

    fun upDislikes() {
        val currentDislikes = _dislikesCount.value
        _dislikesCount.update {
            currentDislikes + 1
        }
    }

    fun sendComplaint(userId: Int) {
        viewModelScope.launch {
            apiService.sendComplaint(
                ComplaintUserRequest(userId, "Какая-то жалоба")
            ).collect { result ->
                result.onSuccess {
                    _infoMessage.update {
                        "Ваша жалоба отправлена. Спасибо, что помогаете нам"
                    }
                }.onFailure { ex ->
                    _infoMessage.update {
                        ex.message
                    }
                }
            }
        }
    }

    fun blockUser(userId: Int) {
        viewModelScope.launch {
            usersRepository.blockUser(userId).collect { result ->
                _infoMessage.update {
                    if (result.isSuccess) "Пользователь заблокирован" else "Произошла ошибка"
                }
            }
        }
    }

    fun cancelComplaint(userId: Int, complainingUserId: Int) {
        viewModelScope.launch {
            usersRepository.cancelComplaint(userId, complainingUserId).collect { result ->
                _infoMessage.update {
                    if (result.isSuccess) "Жалоба отменена" else "Произошла ошибка"
                }
            }
        }
    }

    fun clearInfoMessages() { _infoMessage.update { null } }

    fun setCurrentLikesAndDislikes(likes: Int, dislikes: Int) {
        _likesCount.value = likes
        _dislikesCount.value = dislikes
    }

    fun rateUser(id_user: Int, feedback: Boolean, score: Int) {
        viewModelScope.launch {
            try {
                val answer = apiService.rateUser(id_user, feedback, score)
                //  Log.d("voteMode", "VM rateUser answer $answer")
                // Log.d("voteMode", "VM rateUser STATE ${answer.isSuccessful}")
                if(answer.isSuccessful) {
                    if(feedback) upLikes()
                    else upDislikes()
                }
                if (answer.code() == 200 || answer.code() == 308) {
                    _answerFlow.emit(AnswerState.SUCCESS)

                    //updateVoteState(id_user)
                } else {
                    _answerFlow.emit(AnswerState.FAIL)
                }
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
                _answerFlow.emit(AnswerState.FAIL)
            }

        }
    }

    fun updateVoteState(userId: Int) { // new
        viewModelScope.launch(Dispatchers.IO) {
            val response = apiService.getSearchRating(userId)
            if(response.code() == 404) { // юзер еще не голосовал
                _howUserRatedStatus.update { VoteStatus.NONE }
            } else {
                val feedback = response.body()?.feedback
                if(feedback == true) {
                    _howUserRatedStatus.update { VoteStatus.LIKE }
                } else if(feedback == false) {
                    _howUserRatedStatus.update { VoteStatus.DISLIKE }
                }
            }
        }
    }

    fun getCurrentUserVotes(user_id: Int) { // old
        viewModelScope.launch {
            try {
                /*val response = apiService.getSearchRating(user_id)
                log("response message: ${response.message()}")
                log("response body: ${response.body()}")
                response.body()?.feedback?.let {
                    log("getSearchRating: $it")
                    _userRating.emit(it)
                }*/

                /*Log.d(
                    "voteMode",
                    "VM getCurrentUserVotes ${
                        apiService.getSearchRating(user_id).body()
                    }"
                )*/


            } catch (e: Exception) {
                log("ex: ${e.stackTraceToString()}")
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
                _answerFlow.emit(AnswerState.FAIL)
            }

        }
    }

    /*   fun getSearchRating(user_id: Int) {
           viewModelScope.launch {
               try {
                   _profilesList.emit(
                       userProfile?.get(0)?.let {
                           apiService.getEvaluatorsUsersVote(
                               105
                               // it.id
                           ).body()?.profiles

                       }
                   )

                   Log.d(
                       "AAA",
                       "viewmodel getCurrentUserVotes ${
                           userProfile?.get(0)?.let {
                               apiService.getEvaluatorsUsersVote(
                                   105
                                   //   it.id
                               ).body()?.profiles

                           }
                       }"
                   )
               } catch (e: Exception) {
                   if (BuildConfig.DEBUG) {
                       e.printStackTrace()
                   }
                   _answerFlow.emit(AnswerState.FAIL)
               }
           }
       }*/

    /*fun setUserNumber(phone: String) {
        viewModelScope.launch {
            try {
                _numbersList.emit(
                    apiService.getUserProfileByPhone(
                        phone
                    ).body()
                )
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
                _answerFlow.emit(AnswerState.FAIL)
            }
        }
    }*/


    fun changeVoteState(state: VoteMode) {
        voteState = state
        viewModelScope.launch {
            _howUserRatedStatus.update { voteStatus ->
                when(voteState) {
                    VoteMode.UP -> VoteStatus.LIKE
                    VoteMode.DOWN -> VoteStatus.DISLIKE
                    VoteMode.IDLE -> VoteStatus.NONE
                }
            }
            /*voteFlow.emit(
                when (voteState) {
                    VoteMode.UP -> 1
                    VoteMode.DOWN -> 0
                    VoteMode.IDLE -> -1
                }
            )*/
        }
        Log.d("voteMode", "ViewModel changeVoteState $voteState")
        //   Log.d("voteMode", "ViewModel changeVoteState ${getVoteState()}")
    }

    fun getVoteState(): VoteStatus = howUserRatedStatus.value


    /*    fun voteUp(id: String, type: Votes) {
    //        viewModelScope.launch {
    //            try {
    //                val response = apiService.voteUp(id, type.ordinal)
    //                if (response.isSuccessful) {
    //                    voteState = VoteMode.UP
    //                    voteFlow.emit(true)
    //                }
    //            } catch (e: Exception){
    //                e.printStackTrace()
    //                voteFlow.emit(false)
    //            }
    //        }
        }

        fun voteDown(id: String, type: Votes) {
    //        viewModelScope.launch {
    //            try {
    //                val response = apiService.voteDown(id, type.ordinal)
    //                if (response.isSuccessful) {
    //                    voteState = VoteMode.DOWN
    //                    voteFlow.emit(true)
    //                }
    //            } catch (e: Exception){
    //                e.printStackTrace()
    //                voteFlow.emit(false)
    //            }
    //        }
        }*/

}