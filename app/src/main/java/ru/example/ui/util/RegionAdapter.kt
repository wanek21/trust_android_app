package ru.example.ui.util

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import ru.example.R
import ru.example.databinding.CountryRegionItemBinding
import ru.example.domain.ProfileEntity

class RegionAdapter(private val regionsList: List<String>) :
    RecyclerView.Adapter<RegionAdapter.RegionViewHolder>() {

    private var onRegionItemClicked: ((String?) -> (Unit))? = null
    private var selectedPosition = RecyclerView.NO_POSITION

    class RegionViewHolder(binding: CountryRegionItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val regionTextView = binding.regionTextView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegionViewHolder {
        val binding =
            CountryRegionItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RegionViewHolder(binding)
    }

    override fun onBindViewHolder(
        holder: RegionViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        val region = regionsList[position]
        holder.regionTextView.text = region

        // Устанавливаем цвет текста в зависимости от выбранного элемента
        holder.regionTextView.setTextColor(
            if (selectedPosition == position) ContextCompat.getColor(
                holder.itemView.context,
                R.color.green
            )
            else ContextCompat.getColor(holder.itemView.context, R.color.white)
        )

        holder.itemView.setOnClickListener {
            val previousSelectedPosition = selectedPosition
            selectedPosition = position

            notifyItemChanged(previousSelectedPosition)
            notifyItemChanged(selectedPosition)

            onRegionItemClicked?.invoke(region)
        }
    }

    override fun getItemCount(): Int {
        return regionsList.size
    }

    fun bindAction(action: (String?) -> Unit) {
        this.onRegionItemClicked = action
    }
}