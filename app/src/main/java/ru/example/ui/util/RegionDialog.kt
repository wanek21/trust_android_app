package ru.example.ui.util

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import ru.example.databinding.DialogCountryRegionBinding

class RegionDialog(
    context: Context,
    private val regionsList: List<String>,
    private val onRegionSelected: (String?) -> Unit
) : Dialog(context) {

    private var binding: DialogCountryRegionBinding? = null
    private val adapter by lazy {
        RegionAdapter(regionsList)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DialogCountryRegionBinding.inflate(layoutInflater)
        binding?.root?.let { setContentView(it) }

        bindRegionItemClick()

        binding?.negativeButton?.setOnClickListener {
            dismiss()
        }

        binding?.positiveButton?.setOnClickListener {
            dismiss()
        }
    }

    private fun bindRegionItemClick() {
        binding?.regionsRecyclerView?.adapter = adapter.also {
            it.bindAction { item ->
                Log.d("AAA", "item $item")
                onRegionSelected(item)
            }
        }
    }
}