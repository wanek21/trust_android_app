package ru.example.data

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import ru.example.ApiService
import ru.example.domain.ComplaintUserResponse
import ru.example.domain.ProfileEntity
import ru.example.log
import ru.example.preferences.Preferences
import ru.example.ui.fragments.admin.UserToBlockItem
import ru.example.ui.fragments.admin.placeholder.PlaceholderContent

class UsersRepository(
    private val apiService: ApiService,
    private val sharedPreferences: SharedPreferences
) {

    fun getUsersForBlock(): Flow<List<UserToBlockItem>> = flow {
        val response = apiService.getUsersForBlock()
        log("getUsersForBlock response: $response")
        emit(response.body() ?: emptyList())
    }.flowOn(Dispatchers.IO)

    fun getUserById(id: Int): Flow<Result<ProfileEntity>> = flow {
        val response = apiService.getUserById(id)
        log("getUserById response: $response")
        if(response.isSuccessful) {
            val user = response.body()
            if(user != null) {
                emit(Result.success(user))
            } else emit(Result.failure(Exception("User not found")))
        } else emit(Result.failure(Exception("User not found")))
    }.flowOn(Dispatchers.IO)

    fun blockUser(id: Int): Flow<Result<Boolean>> = flow {
        val response = apiService.blockUser(id)
        log("blockUser response: $response")
        if(response.isSuccessful) {
            emit(Result.success(true))
        } else emit(Result.failure(Exception("Error during block user")))
    }.flowOn(Dispatchers.IO)

    fun cancelComplaint(userId: Int, complainingUserId: Int): Flow<Result<Boolean>> = flow {
        val response = apiService.cancelComplaint(userId, complainingUserId)
        log("cancelComplaint response: $response")
        if(response.isSuccessful) {
            emit(Result.success(true))
        } else emit(Result.failure(Exception("User not found")))
    }.flowOn(Dispatchers.IO)

    fun checkBlockedUsers(usersList: List<Int>) = flow {
        usersList.forEach { userId ->
            val user = getUserById(userId).first().getOrNull()
            if(user != null && !user.isActive) {
                val yourBlockedUsers = sharedPreferences.getStringSet(Preferences.COMPLAINTS_IDS, setOf())
                log("removing id from this set: $yourBlockedUsers")
                val newList = usersList.toMutableList()
                newList.remove(userId)
                sharedPreferences.edit {
                    putStringSet(Preferences.COMPLAINTS_IDS, newList.map { it.toString() }.toSet())
                }
                emit(user.fullName)
                return@forEach
            }
        }
        emit(null)
    }.flowOn(Dispatchers.IO)
}