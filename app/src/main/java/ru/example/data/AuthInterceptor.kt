package ru.example.data

import android.content.SharedPreferences
import android.provider.Settings.Global.putString
import androidx.core.content.edit
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.example.ApiService
import ru.example.ApiService.Companion.TOKEN_KEY
import ru.example.di.UnsafeOkHttpClient
import ru.example.domain.TokenManager
import ru.example.log

class AuthInterceptor(
    private val sharedPreferences: SharedPreferences
): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = sharedPreferences.getString(TOKEN_KEY, null)
        val request = chain.request()
        val response = if (!token.isNullOrEmpty()) {
            val newRequest = request
                .newBuilder()
                .header("Authorization", token)
                .build()
            return chain.proceed(newRequest)
        } else chain.proceed(request)

        if (/*response.code == 401 || */response.code == 403) {
            log("response code: 403")
            val updatedToken = getUpdatedToken()
            log("get updated token: $updatedToken")
        }

        return response
    }

    private fun getUpdatedToken(): String? {
        log("get updated token")
        return runBlocking(Dispatchers.IO) {
            val refreshTokenRetrofit = Retrofit.Builder()
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .baseUrl("https://185.178.46.6")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            val refreshTokenApi = refreshTokenRetrofit.create(RefreshTokenApi::class.java)
            val refreshToken = sharedPreferences.getString(ApiService.REFRESH_TOKEN, null)
            log("[refreshTokenApi] refreshToken: $refreshToken")
            if(!refreshToken.isNullOrEmpty()) {
                val authTokenResponse = refreshTokenApi.refreshAccessToken(
                    "Bearer $refreshToken"
                ).execute()

                log("authTokenResponse: ${authTokenResponse.raw().message}")

                log("error response: " + authTokenResponse.message())

                if(authTokenResponse.isSuccessful) {
                    val newToken = authTokenResponse.body()?.accessToken
                    if(!newToken.isNullOrEmpty()) {
                        log("Successfully saved new token: \n$newToken")
                        sharedPreferences.edit {
                            putString(TOKEN_KEY, "Bearer $newToken")
                        }
                        newToken
                    } else null
                } else null
            } else null
        }
    }
}
