package ru.example.data

import android.content.SharedPreferences
import androidx.core.content.edit
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.example.ApiService
import ru.example.ApiService.Companion.REFRESH_TOKEN
import ru.example.ApiService.Companion.TOKEN_KEY
import ru.example.di.UnsafeOkHttpClient
import ru.example.log

class TokenAuthenticator(
    private val sharedPreferences: SharedPreferences
) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request {
        // This is a synchronous call
        log("authenticate")
        val updatedToken = getUpdatedToken()
        log("get new token after refresh: $updatedToken")
        if(updatedToken != null) {
            return response.request.newBuilder()
                .header(ApiService.AUTH_HEADER, "Bearer $updatedToken")
                .build()
        } else {
            return response.request.newBuilder()
                //.header(ApiService.AUTH_HEADER, updatedToken)
                .build()
        }
    }

    private fun getUpdatedToken(): String? {
        log("get updated token")
        return runBlocking(Dispatchers.IO) {
            val refreshTokenRetrofit = Retrofit.Builder()
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .baseUrl("https://185.178.46.6")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            val refreshTokenApi = refreshTokenRetrofit.create(RefreshTokenApi::class.java)
            val refreshToken = sharedPreferences.getString(ApiService.REFRESH_TOKEN, null)
            log("[refreshTokenApi] refreshToken: $refreshToken")
            if(!refreshToken.isNullOrEmpty()) {
                val authTokenResponse = refreshTokenApi.refreshAccessToken(
                    "Bearer $refreshToken"
                ).execute()

                log("authTokenResponse: ${authTokenResponse.raw().message}")

                log("error response: " + authTokenResponse.message())

                if(authTokenResponse.isSuccessful) {
                    val newToken = authTokenResponse.body()?.accessToken
                    if(!newToken.isNullOrEmpty()) {
                        log("Successfully saved new token: \n$newToken")
                        sharedPreferences.edit {
                            putString(TOKEN_KEY, "Bearer $newToken")
                        }
                        newToken
                    } else null
                } else null
            } else null
        }
    }
}