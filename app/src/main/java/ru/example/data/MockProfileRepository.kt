package ru.example.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import ru.example.domain.ExtraSearchParams
import ru.example.domain.ProfileEntity

class MockProfileRepository : ProfileRepository {
    override fun getProfiles(query: String, region: String, extraParams: ExtraSearchParams?): Flow<PagingData<ProfileEntity>> {
        return Pager(PagingConfig(pageSize = 20)) {
            MockProfilesPagingSource()
        }.flow
    }
}
