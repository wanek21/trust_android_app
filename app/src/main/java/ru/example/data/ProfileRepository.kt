package ru.example.data

import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import ru.example.domain.ExtraSearchParams
import ru.example.domain.ProfileEntity

interface ProfileRepository {
    fun getProfiles(query: String, region: String, extraParams: ExtraSearchParams?): Flow<PagingData<ProfileEntity>>
}
