package ru.example.data

data class Contact(val id: String, val name: String, val number: String)
