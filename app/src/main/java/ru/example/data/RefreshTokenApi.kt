package ru.example.data

import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.POST
import ru.example.domain.TokenManager

interface RefreshTokenApi {
    @POST("api/refresh-token/")
    fun refreshAccessToken(
        @Header("Authorization") refreshToken: String? = TokenManager.refreshToken
    ): Call<RefreshTokenResponse>
}