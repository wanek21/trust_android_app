package ru.example.data

data class CancelComplaintRequest(
    val user_id: Int,
    val complaining_user_id: Int
)
