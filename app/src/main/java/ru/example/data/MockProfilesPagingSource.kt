package ru.example.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import ru.example.domain.ProfileEntity

class MockProfilesPagingSource : PagingSource<Int, ProfileEntity>() {
    private val profiles = mutableListOf<ProfileEntity>()
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProfileEntity> {
        try {
            val startIndex = params.key ?: 0
            val profiles = generateMockProfiles(startIndex, params.loadSize)
            addMockUser()

            return LoadResult.Page(
                data = profiles,
                prevKey = if (startIndex > 0) startIndex - params.loadSize else null,
                nextKey = if (profiles.isNotEmpty()) startIndex + params.loadSize else null
            )
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, ProfileEntity>): Int? {
        return state.anchorPosition
    }

    private fun generateMockProfiles(startIndex: Int, pageSize: Int): List<ProfileEntity> {

        addLongTitle()

        for (i in startIndex+1 until (startIndex+1 + pageSize)) {
            val profile = ProfileEntity(
                id = i,
                approves = i+1,
                disapproves = i+1,
                country = "Country $i",
                city = "City $i",
                isConfirmed = true,
                phoneNumber = "+1234567890",
                fullName = "Surname Name $i",
                name = "Name $i",
                birthDate = "01/01/2000",
                userPic = image,
                rating = 4.5f,
                statisticId = i,
                isActive = true
            )
            profiles.add(profile)
        }


        return profiles
    }

    private fun addLongTitle(){
        val profile = ProfileEntity(
            id = 0,
            approves = 66,
            disapproves = 11,
            country = "Очень длинное название для страны",
            city = "Очень длинное название для города",
            isConfirmed = true,
            phoneNumber = "+1234567890",
            fullName = "Очень длинная фамилия пользователя",
            name = "Очень длинное имя пользователя",
            birthDate = "01/01/2000",
            userPic = image,
            rating = 4.5f,
            statisticId = 0,
            isActive = true
        )
        profiles.add(profile)
    }

   private fun addMockUser(){
        val profile = ProfileEntity(
            id = 0,
            approves = 0,
            disapproves = 0,
            country = "Country",
            city = "City",
            isConfirmed = true,
            phoneNumber = "+79006596370",
            fullName = "Full Name",
            name = "Name ",
            birthDate = "01/01/2000",
            userPic = image,
            rating = 4.5f,
            statisticId = 60,
            isActive = true
        )
        profiles.add(profile)
    }

   private companion object{
       val image = "/9j/4QDmRXhpZgAASUkqAAgAAAAFABIBAwABAAAAAQAAADEBAgAcAAAASgAAADIBAgAUAAAAZgAAABMCAwABAAAAAQAAAGmHBAABAAAAegAAAAAAAABBQ0QgU3lzdGVtcyBEaWdpdGFsIEltYWdpbmcAMjAwNToxMjowMSAwMDoyNDo0OAAFAACQBwAEAAAAMDIyMJCSAgAEAAAANjg3AAKgBAABAAAAgAAAAAOgBAABAAAAgAAAAAWgBAABAAAAvAAAAAAAAAACAAEAAgAEAAAAUjk4AAIABwAEAAAAMDEwMAAAAAAKAAAA/8AAEQgAgACAAwEhAAIRAQMRAf/bAIQAAwICAgIBAwICAgMDAwMEBwQEBAQECQYGBQcKCQsLCgkKCgwNEQ4MDBAMCgoPFA8QERITExMLDhUWFRIWERITEgEEBQUGBQYNBwcNGxIPEhsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsbGxsb/8QAmQAAAwEBAQEBAQAAAAAAAAAABAUGAwIHCAABEAABAwMCAggFBAIABwAAAAABAgMEAAURBiESMQcTIkFRcYGRFDJCYaEIFTNScsEjJDRiY9HwAQACAwEBAQEAAAAAAAAAAAAEBQIDBgEHAAgRAAICAgEDAgQGAQUAAAAAAAECAAMEETEFEiFBUQYTYXEUIpGh0fAyQlKBseH/2gAMAwEAAhEDEQA/APiNIR3Jwa6SkY5UNNFNAk454zXaGz4CoGTEIQ3jZQrVLWaqJlgE1RHPhRLcfblVZMsAm6WB4YrrqiVYxmobloWfuoyK5VH765uS7ZmqPvyrJUfCdxUg0+1B1s4O4FCPMdnkKsBkCIElNdgeFEmBATVDeedbpQRsAaqJloE2Q0SrnRTbByN6qJlgEJQz4CiERycE1UTLVWbpjnurRMUkciaqLQhU3O/hO7euTEONwc1Dvlormao6hnsmh1sA/TUg04U1MHGQRmg3ovYyBzq9TKGWJE5HdWrfHxcqMMAEIQlWOVFNtrIFUtLQIYzGcJ2TRiIrnemqGYCXKphDcdf9aMZhuKHZRQ7MISlZJhjNseUr5CaNbtLuBlOKGewCM6sdjNf2ogfLXJtvY+Wqfmbh34aYO2/sfLQL1vAB7NWq8HsogLtuc4iEpJrL9lkuJBKm0/5KxRItAi9qCeJHtN+BNGMMEnambGJlEOZZWBgNgn70azFWrm3jyFDMQISqkxlHgk4GVDP2pkzZ5SyOBpavvignsA5jCulm4jONp6QtAyyr2pvC029txNqpdZeAI5x8JifIjhjTriUgJaUKOTp55DeC0ceVLnvHvNHVjBRPxsbgQT1YHpQcizK4d0n2qK3Dctegai2RaCMnHvS2TbQjmBnzo5LNxZbRqJ5UQDPaFJpcQEnCuf3plU0RX1SSjRHyRxNUyZhrGCUCmzMJnVQxhHjLH0pprFaUPqQPMUFYRGFKajeO4lAGHEHHICmsOUOMJKlkeAFLbF3HVDaMprWUOKymOs+GasLPZ5twdQzFgqcUvZKUpJJP+6Q5JC8maykKK+4+J6vp79NnSjfYSJKNPqitL3CpTiWdvHB3/FP3/wBIvSQI3El21rV/UTMH8pqFeHk2DuC+PrM5kfFPTcezsDE/YeJGam/Tz0l6bguSp2mZLjDeeJ2MQ8kDx7OTj0ry642h+PxcSVDG1RIelu2wajrD6li9QTvobf8A3JqbHWFEZNIZsdwAjCj9t6aUsJDIEUvw3dzwK8sZpa/b3lJ3aUPMUyRxEltZMnottU4QkTG1D/KnUTTbkhwJRJT5A5o6y4L6RPRiNZwY/gaDfWni+NQCadRujZ95QK56Dmk9vUFX0j2vpD68tH1v6KwMFU9s+tUkHosa6xI+LR9uJVI7+rfSNKsBKR58z1Lo1/T9P1dqVESC60GW8KfeIyhpPifv4Dvr7B0B0U6P6O7YhFmgJdmhOHJzyQXVeOP6j7D81f0qv8a34hx4HH8zDfFXWmA/BU+P938fzLLO+fc1/CsDbO+K1wAnmJMxcXhWc15Z0ndCmkdfxHJPwrduueOxLYQE8R/8iRsrz50JlY631lTDen9Rt6fkC6s/ce49p8ea76O1aM1Y9aLyw62+1uCkdlxJ5KSe8GoWbb7O0SVB059KxyNcjlG5E90xshculbV4I3E8k2ZsEhpY9c0imz7SnI6t0DzppSLWlthRR5nnMe1yePZY96orPbpCHwFKUE95znFaC6wETO4mPYHEtrPa1Oqwq5I27iKr7fpy5LSFMOpcH2NZfKvVT+YTaVVlF8mUVu0vdysZiqPqarbJpO5qntMJhrU44oJSAOZJ2FZrJya28Az6y+usHZn1h0awrvofVcjo/e0fINvTFYnMX2OB1DzikkPNPEnIcQpI4eEEFKxyKTn0ouJSgqUoBIGST3V6BhqmPQiKfQfr6/vPzxnXNlZD3NySTMY86JNidfClsyGlcltOBaT6jav448kbFY96Z94HMVt7QdTu2QRw+OaDkOZB3AGPGvi4lWp5N06aFZ1r0RyFxmkG525Cn4i/qVgZU35ED3Ar4VusacHlANLx5VmMtUXI7veet/CWT8zEapv9J/Y/+7k5Ntt0fBCI6hUzNsV0XIW3kFaACpIOSkHlkeh9qKotrWanIrd+I8t+kYiSklSjj71TW7TMAAAn80PkZTR1j4aVSqtenoCAkoQk48TVpZrZCZKVBtIP2NZLNyXIMLt/KviWltSwnhCUg+u9XGjktHXtvzwpAfSrJ7sb/wCqzBsHeAZj+oEitj9J72xc1qaCjJBUR4UPcurudmkW2eWpEWW0pl5lwcSHEKGFJI7wQSK3GNksACGnkViDfEV6Qs1o0HoY6dsLaI9vRKkSWI6cJQwHXFOFtAHJCVKISO4YHdTCTcwoZC0+9Ozn952x8wM1EmStjsFl0tru836zvyG3L6sPy2FylrYL2SVOoQSQhagUhXDgEITt3lq9ett3Acfeu2dQ7z3EzvySYluN3K2VAqGCN96+VrxGtV3v16ttnlITLgvOMuDqu0yrJ4ThScEeBGQcHwNZ7PyC4D+g5mt6ATQ7KDomT0TT85On0JvvwqpySUuLjJKW3ADgLAO4Khvjuzip256Os69RoupbcTLaR1YcQsjiRnPCRyIzvuOe9DUZunPy+Pr7T0erdyL38jX6yPgOnYFZ9qfQHM4wT7VobxNZWPEpLetIx2iKqLe6nAwv81l8sEgyNo8alRbX2yoArNWOm5rbOoYri3SkJcTk55VjcglXBmTz6yVIntEUrDYIcTjzrRbygcKd9jT6hz28zy6xRviY9aetPNSSOed6ycVxowEkd2c0yVhqCkQCQ2odnJ8cZqejSNTt68lxrpEgrsy8qhyo76uvRsOw62QBjOcKSTywQOdWgpo9x+0kBO7lIitsKUVEBIycmvE7tJbdvDksJTxniSF/UElWeHPhnfFJ8uzegJpuj1EsSZN3GbhCsLGTUrcpuQR1gzR2FVxN5SoCzyeFqyGnHHIbB86bxtX27IJlp9DW8txGPpG9WdSw5jiNre1pQB15UR3CnkPXUEY6tLivSk9/T3bmFC2u3/Exuz0kIYV/w4yz60UOmCQyOJmNwnwO9Jn6Etp/MYLbjpYfMNR+pHVrCkthaeAYGxOacMfqR1I9b+FEQl08lcRx7VU3w5XWAUsIiZ+j4dh8CLrh+oXXzDQXBYXIVxZW0ghJCQMkjPM+AHM03tfSz0kG5vtS74xIiLeU6yoMEOtNqwQhRyAcHONs4wDnGalZ0ummv/I7P1/vMDbo2M1ugmgP3+3tqbWjXEbTF5uFxtanEybooOTeBzAdcBUesIP1dognvASO4VhM6dJS7gpD8l5CUjYJVkk+eKCGBk5Tlmfz/RKx0THU71JLUXTRfJaVRY0iQgK24yscvaoO6dIV5bUsOSXFnGw4tq0GH0epAO/yYfXj04q/lWT7/SHd1sK4piUk8uI5xSCXrO4pWVPXXJV4DlWjpwKk4WD2ZWh4Op5/CwpQys+9UEFLKkgFZ38af271F+MAT5j2I20lQw4PankRwtpwlxOKUW+ZqcYBeI0aW443/KlJ7966+CW6R/zSR65oAsF9IewJgt0tUuLbFSIC2JMhKhwsuOcAcGRkA5ABxk5NaS7dKlWBcR2eqOFjbqXihaD3YUn/AO2qQtUgEjzFz1uSyb8Ec+swDt2aS2ly5Fa0JCSoJO5A3Przoxq8X1tI7RIByCds11q6nHkSpTcvje5xK1HenWlcTBO2+OdT0y9XV1RKi5v9I2q2mipeILfdbwBFMi4PHdanEEeZNBSLgop4nJDqvNJpiqD0ih7D6mKX7ukKx1Kz9yKWy7mFIwmOT60alf1i6y3fpNoUDJHa3p5CipTzV+aja0Z49Wo6iMt4GSR601jtNAZ6w+hpZYTNJQi6haJEJmW3GemNoddzwNqcAUvHPA5n0oy1xJaJkgzJiH2VOcUYJRwrQnA7KjyO+cbd+9Aue0HY+0KUFnHafA5/SGyNMWu5z48p9s/ERVhbLwPbRg5wD4HcEd4p/FsUNWCvBz4mlt2S4XXtOGtK2Zl9eY3j2C1pwcoTRf7RbMEEJUP8aTPl2kyosYM9boAVksIPpSyVAtqUbRW80RTdY3rI7iCdDilJHUNj0qelwYnESqOg+lPqHbXMgyq3IiWZFgb8TSPakctu3ozhpA9KcVFzFmRXUPOojhyCADnbzptHkDAPENvvRziA0tGMeU2rk6CAcHB5HwreBMmpv0ht8IVEISth3jAUDjtII7wDgg/fHdQTKPO43qc7Uj/n7RqfgJL7Tj6G1rYWlxtRA4kKHIg8xTdickAYc/NAWKTzG9ZQbK+sPj3PhH8hpgzdlAfOaXWVbkGO4Y3enAMJKq3/AH55KOWfWgmxlJ8yrUydv7xHIA+dLpF4cUT/AO6urx1EiRE8u6LVkf7pJMuSt+yD602prErZtCIZt1OTlP5qem3NJz2R703qr1EmTduQFr1ZKdQpD8VTbjSynIyErGdlDPiPaj4Opbsb+40/Hb+FKeJpxBPEMYHCocs7k5HdTpqF8zMpk26Ugff+/wB8RjHujjN0dlMpUlyRguAuHhJAwDjlnHfTFF+ndxTQr1D1h1eQ6+FhDN9ufECFDNMI1+u4PzJ50I1SGHV5d44jJjUF6JASpA8xTBm93pQ/mQPSg3pqjOvIyHhiLveiN5SeXhXZud3POfj7YoY1Vj0hga0jyZmZ11V8078UO9PuAH/WH2qYRPacY2AcxdIuc7BPxKjSqVeJY2Lx8aMrrWLrbrB6xTKu72+VA0ol3Y75CaOrr3xFFtpPMhWLggJG+fWjmbqEkEGm7IYqVwYU3d9h2hmjWbsCOY96HZNy4NDWbruOFX5o9m7Odzn5oZqpetpEOYvK0pyXM+tGN6gKfr/NCtUDDa8ntm6NSuf32rs6mIHz1UaRChnGYuaoXyDv5oR7UhOxdPlmprQJS+cT6xdJ1GnH8nP70rlX8HiHETRaU+8AfILRZIvZI7IFK5F0Wv6vOi0T2grPJBmQsnY58qYtSXFHIUkjzozczlF5hbbp4AeIAnlvRDclYwlK8+tQYAxqtqmEtzFBY4jijGbgrIHWAUOw1L+6GNXE7dvPrW6bl/3VUQJ93TQXEE/Odq6+NBx2/Peudok+6cmUjG6+fKslzG8bnkK+1PtiBvzWuHOaAenNnISc+VWAb4nCwEBelAg5UOfjQb01kJOFgk0So1BLclFHM//Z"
   }
}
