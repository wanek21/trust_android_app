package ru.example.di

import android.content.Context
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.example.Api
import ru.example.ApiService
import ru.example.AuthFragmentViewModel
import ru.example.CodeConfirmViewModel
import ru.example.data.AuthInterceptor
import ru.example.data.MockProfileRepository
import ru.example.data.RefreshTokenApi
import ru.example.data.TokenAuthenticator
import ru.example.data.UsersRepository
import ru.example.domain.HttpClientFactory
import ru.example.domain.KeyStoreManager
import ru.example.preferences.Preferences
import ru.example.ui.fragments.ExternalProfileDetailesViewModel
import ru.example.ui.fragments.addUserFragment.AddUserViewModel
import ru.example.ui.fragments.contactsFragment.ContactsViewmodel
import ru.example.ui.fragments.evaluatorsUsersFragment.EvaluatorsUsersViewmodel
import ru.example.ui.fragments.externalSearch.ExternalSearchViewModel
import ru.example.ui.fragments.mainFragment.MainFragmentViewModel
import ru.example.ui.fragments.registrationFragment.RegistrationViewModel
import ru.example.ui.fragments.userProfileFragment.UserProfileViewModel

val module = module {


    single {
        AuthFragmentViewModel(get())
    }

    single {
        KeyStoreManager(androidContext())
    }

    single {
        Preferences(androidContext().getSharedPreferences("app_prefs", Context.MODE_PRIVATE))
    }
    single { MockProfileRepository() }
    single { UsersRepository(get(), get()) }


    viewModel {
        MainFragmentViewModel(get(), get(), get(), get())
    }

    viewModel {
        ExternalProfileDetailesViewModel(get(), get())
    }

    viewModel{
        EvaluatorsUsersViewmodel(get())
    }

    viewModel{
        UserProfileViewModel(get())
    }

    viewModel {
        CodeConfirmViewModel(get())
    }

    viewModel {
        ContactsViewmodel(get())
    }

    viewModel {
        RegistrationViewModel(get())
    }

    viewModel {
        AddUserViewModel(get())
    }

    viewModel{
        ExternalSearchViewModel()
    }

    single {
        get<Retrofit>().create(RefreshTokenApi::class.java)
    }
    single {
        TokenAuthenticator(get())
    }
    single {
        AuthInterceptor(get())
    }

    single {
        Retrofit.Builder()
            .client(UnsafeOkHttpClient.getUnsafeOkHttpClient(get(), get()))
            .baseUrl("https://185.178.46.6")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    single {
        get<Retrofit>().create(Api::class.java)
    }

    single {
        androidContext().getSharedPreferences("app_prefs", Context.MODE_PRIVATE)
    }
    single {
        ApiService(get(), get(), androidContext())
    }

}