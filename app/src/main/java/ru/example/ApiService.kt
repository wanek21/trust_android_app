package ru.example

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.provider.OpenableColumns
import android.util.Log
import androidx.core.content.edit
import androidx.core.net.toUri
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import retrofit2.Response
import ru.example.data.CancelComplaintRequest
import ru.example.domain.AddUserRequest
import ru.example.domain.Answer
import ru.example.domain.ComplaintUserRequest
import ru.example.domain.Contacts
import ru.example.domain.ContactsPagingSource
import ru.example.domain.EvaluatorsProfileEntity
import ru.example.domain.EvaluatorsUsersPagingSource
import ru.example.domain.ExtraSearchParams
import ru.example.domain.GetUserContactsRequest
import ru.example.domain.PhoneNumber
import ru.example.domain.ProfileEntity
import ru.example.domain.ProfilesPagingSource
import ru.example.domain.RateUserRequest
import ru.example.domain.Selection
import ru.example.domain.TokenManager
import ru.example.preferences.Preferences
import ru.example.ui.fragments.addUserFragment.AddUserProfileForm
import ru.example.ui.fragments.registrationFragment.RegistrationProfileForm
import ru.example.ui.util.InputStreamRequestBody
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream

class ApiService(
    private val api: Api,
    private val sharedPreferences: SharedPreferences,
    private val context: Context
) {

    suspend fun getVoteDetailes(id: String) = api.getVoteDetailes(id)

    suspend fun sendNumber(number: PhoneNumber) = api.sendSms(number)

    suspend fun sendCode(code: CodeCell) = api.sendCodeNew(code).also {
        log("Code verify response: ${it.body().toString()}")
        if (it.isSuccessful) {
            TokenManager.access_token = "Bearer ${it.body()?.token}"
            TokenManager.refreshToken = it.body()?.refreshToken
            sharedPreferences.edit {
                putString(TOKEN_KEY, "Bearer ${it.body()?.token}")
                putString(REFRESH_TOKEN, it.body()?.refreshToken)
                putString(Preferences.PHONE, code.phone)
                putBoolean(Preferences.IS_ADMIN, it.body()?.isAdmin ?: false)
            }
            Log.d("@@@", "sendCode TokenManager body token ${it.body()?.token}")
        }
    }

    suspend fun blockUser(id: Int) = api.blockUser(id)
    suspend fun cancelComplaint(userId: Int, complainingUserId: Int) = api.cancelComplaint(
        CancelComplaintRequest(userId, complainingUserId)
    )
    suspend fun getUsersForBlock() = api.getComplaintsList(Selection(0, 100))
    suspend fun getUserById(id: Int) = api.getProfileById(id)

    suspend fun addAvatar(imageUri: Uri): Response<Answer> {
        return withContext(Dispatchers.IO) {
            val file = File(getRealPathFromURI(imageUri, context))
            val shrinkedImage = file.toUri()
            val requestBody = InputStreamRequestBody(
                "multipart/form-data".toMediaTypeOrNull(),
                context.contentResolver,
                shrinkedImage
            )
            val body = MultipartBody.Part.createFormData(
                "file",
                shrinkedImage.path,
                requestBody
            )

            api.addAvatar(body)
        }
    }

    // получить путь до файла по его Uri
    private fun getRealPathFromURI(uri: Uri, context: Context): String? {
        val returnCursor = context.contentResolver.query(uri, null, null, null, null)
        val nameIndex =  returnCursor?.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor?.moveToFirst()
        if(nameIndex != null) {
            val name = returnCursor.getString(nameIndex)
            val file = File(context.filesDir, name)
            try {
                val inputStream: InputStream? = context.contentResolver.openInputStream(uri)
                val outputStream = FileOutputStream(file)
                var read = 0
                val maxBufferSize = 1 * 1024 * 1024
                val bytesAvailable: Int = inputStream?.available() ?: 0
                val bufferSize = Math.min(bytesAvailable, maxBufferSize)
                val buffers = ByteArray(bufferSize)
                while (inputStream?.read(buffers).also {
                        if (it != null) {
                            read = it
                        }
                    } != -1) {
                    outputStream.write(buffers, 0, read)
                }
                inputStream?.close()
                outputStream.close()

            } catch (e: java.lang.Exception) {
                log("${e.stackTraceToString()}")
            }
            return file.path
        } else return null
    }

    suspend fun rateUser(id_user: Int, feedback: Boolean, score: Int) =
        api.rateUser(RateUserRequest(
            id_user, feedback, score
        )).also {
            if (it.isSuccessful) {
                TokenManager.access_token = it.body()?.token
                Log.d("@@@", "rateUser TokenManager body token ${it.body()?.token}")
            }
        }

    suspend fun sendComplaint(complaintUserRequest: ComplaintUserRequest) = flow {
        val response = api.sendComplaint(complaintUserRequest)

        if(response.body() != null) {
            sharedPreferences.getStringSet(Preferences.COMPLAINTS_IDS, setOf())?.let {
                sharedPreferences.edit {
                    putStringSet(Preferences.COMPLAINTS_IDS, it.plus(complaintUserRequest.userId.toString()))
                }
            }
            emit(Result.success(response.body()!!))
        } else {
            if(response.code() == 409)
                emit(Result.failure(Exception("Жалоба уже была отправлена")))
            else
                emit(Result.failure(Exception("Неизвестная ошибка")))
        }
    }

    suspend fun addUserProfile(phoneNumber: PhoneNumber, profile: AddUserProfileForm) =
        api.addUser(AddUserRequest(profile, phoneNumber)).also {
            if (it.isSuccessful) {
                TokenManager.access_token = it.body()?.token
                Log.d("@@@", "addUserProfile TokenManager body token ${it.body()?.token}")
            }
        }

    suspend fun createProfile(profile: RegistrationProfileForm) = api.createProfile(profile, "Bearer ${TokenManager.access_token}").also {
        log("token: ${TokenManager.access_token}")
        if (it.isSuccessful) {
            TokenManager.access_token = "Bearer ${it.body()?.token}"
            sharedPreferences.edit {
                putString(TOKEN_KEY, "Bearer ${it.body()?.token}")
                putString(REFRESH_TOKEN, TokenManager.refreshToken)
            }
            Log.d("@@@", "createProfile TokenManager body token ${it.body()?.token}")
        }
    }

    suspend fun getSearchRating(user_id: Int) = api.getSearchRating(user_id)
    suspend fun getUserProfileByPhone(userPhone: String) =
        api.getUserContactList(
            getUserContactsRequest = GetUserContactsRequest(
                selection = Selection(
                    offset = 0,
                    limit = 1
                ),
                contacts = Contacts(listOf(userPhone))
            )
        ).also {
            if (it.isSuccessful) {
                Log.d("@@@", "getUserProfileByPhone TokenManager body token ${it.body()?.toString()}")
            }
        }

    fun getEvaluatorsUsers(
        id_user: Int
    ): Flow<PagingData<EvaluatorsProfileEntity>> {
        return Pager(
            PagingConfig(
                pageSize = 10,
                prefetchDistance = 10,
                enablePlaceholders = true,
                initialLoadSize = 25
            ),
            pagingSourceFactory = {
                EvaluatorsUsersPagingSource(
                    api,
                    id_user
                )
            }
        ).flow
    }

    fun getProfiles(
        query: String,
        region: String,
        extraParams: ExtraSearchParams?
    ): Flow<PagingData<ProfileEntity>> {
        return Pager(
            PagingConfig(
                pageSize = 10,
                enablePlaceholders = false,
                initialLoadSize = 10
            ),
            pagingSourceFactory = {
                ProfilesPagingSource(
                    api,
                    query,
                    region,
                    extraParams
                )
            }
        ).flow
    }

    fun getContacts(
        contacts: List<String>
    ): Flow<PagingData<ProfileEntity>> {
        return Pager(
            PagingConfig(
                pageSize = 10,
                prefetchDistance = 10,
                enablePlaceholders = true,
                initialLoadSize = 25
            ),
            pagingSourceFactory = {
                ContactsPagingSource(
                    api,
                    contacts
                )
            }
        ).flow
    }

    companion object {
        const val TOKEN_KEY = "access_token"
        const val REFRESH_TOKEN = "refresh_token"
        const val AUTH_HEADER = "Authorization"
    }
}