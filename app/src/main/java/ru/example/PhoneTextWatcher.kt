package ru.example

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.navigation.findNavController
import ru.example.databinding.AuthLayoutBinding

class PhoneTextWatcher(
    private val binding: AuthLayoutBinding
) : TextWatcher {

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        s?.let {
            if (it.length == 10) {
                binding.sendButton.isEnabled = true
            } else if (it.length < 10) {
                binding.sendButton.isEnabled = false
            }
        }
    }

    override fun afterTextChanged(s: Editable?) {

    }
}