package ru.example

import com.google.gson.annotations.SerializedName

data class ProfileCreationAnswer(
    val STATUS: String,
    @SerializedName("access_token")
    val token: String
)
