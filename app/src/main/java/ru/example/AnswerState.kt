package ru.example

enum class AnswerState {

    SUCCESS, FAIL
}