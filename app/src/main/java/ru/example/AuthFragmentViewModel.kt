package ru.example

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import ru.example.domain.PhoneNumber

class AuthFragmentViewModel(
    private val remote: ApiService
): ViewModel() {


    private val _answerFlow = MutableSharedFlow<AnswerState>()
    val answerFlow: SharedFlow<AnswerState> = _answerFlow

    val codeState = MutableStateFlow("")


    fun senCode(number: PhoneNumber) {
        viewModelScope.launch {
            try {
                val result = remote.sendNumber(number)
                /*log("code from result: ${result.body()?.message}")
                val code = result.body()?.message
                if(!code.isNullOrEmpty()) {
                    codeState.update {
                        code
                    }
                }*/
                delay(500L)

                _answerFlow.emit(AnswerState.SUCCESS)
                Log.d("@@@", "sendCode ${result.toString()}")
                if(result.isSuccessful){
                    _answerFlow.emit(AnswerState.SUCCESS)
                } else {
                    _answerFlow.emit(AnswerState.FAIL)
                }
            } catch (e: Exception){
                e.printStackTrace()
                _answerFlow.emit(AnswerState.FAIL)
            }
        }
    }


}