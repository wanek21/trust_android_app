package ru.example.preferences

import android.content.SharedPreferences
import androidx.core.content.edit


class Preferences(private val sharedPreferences: SharedPreferences) {

    companion object{
        const val USER_REGION = "region"
        const val PHONE = "user_phone"
        const val IS_ADMIN = "is_admin"
        const val COMPLAINTS_IDS = "blocked_users"
    }


    var region by sharedPreferences.string(USER_REGION, "")

    fun savePhone(phone: String) {
        sharedPreferences.edit {
            putString(PHONE, phone)
        }
    }
    fun getPhone(): String? {
        return sharedPreferences.getString(PHONE, null)
    }

}